/*
# $Header: /home/ralph/progs/concordance/RCS/INSTALL,v 0.5 1996/12/01 14:06:38 ralph Exp ralph $
# $Date: 1996/12/01 14:06:38 $
# $Author: ralph $
# $Log: INSTALL,v $
# Revision 0.5  1996/12/01 14:06:38  ralph
# Changed the 1st line of the copyright to indicate program's purpose.
#
# Revision 0.4  1996/12/01 14:01:56  ralph
# .Changed INSTALL from the old DOS README.TXT for UNIX application.
#
*/
Concordance - a program to make concordances for text files. 
Copyright (C) 1996 Ralph L. Meyer, Spotswood, NJ 08884

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Send bug reports or suggestions to:
meyer@princeton.edu OR
R. L. Meyer, 39 Nelson Ave., Spotswood, NJ 08884 


PURPOSE:

This program, concordance, was created to make a concordance of all the words
in a given text file, along with their locations in the file (page, line, or
verse - depending on the switches you set) and to output a file with this
information in convenient columnar form along with another file providing
information on alphabet usage in the file concordanced.  It is supposed to be a
non-destructive program -- i.e.  it does nothing at all to the file
concordanced but read it (but note the disclaimer above!  If it eats your file
and spits out garbage and trashes your computer's  motherboard or something,
don't say you weren't warned!!!  I take no responsibility for any of that!)

INSTALLING:

If you want to see what is in the tar'ed, gzip'ed file concordance.tar.gz, I
suggest you list the files using the command:

tar -tzvf /pathWhereItIs/concordance.tar.gz

The tar'ed/gzipped file concordance.tar.gz comes with a binary executable
compiled on a Linux Pentium system by g++ V. 2.7.0.  The source .cc and .h
files are included in the .tar.gz file. There is also a Makefile.  To install
concordance I suggest you untar the file in /usr/local or wherever you wish to
put it if you  are not root.

tar -xzvf /pathWhereItIs/concordance.tar.gz

Run concordance on a test text file to make sure the compiled binary in the
distribution works on your machine.  If it does, you can then just run:

make install

and the Makefile will install concordance to the directory /usr/local/bin.  Of
course, you have to do this as root.  If you aren't root, then you'll have to
manually copy concordance to whatever directory you keep binaries in under your
$HOME directory, or change the INSTALLDIR macro in the Makefile to equal
whatever directory is the one in which you want to install the binary.

If, on testing the binary included, it doesn't work, you'll have to make the
binary yourself by changing whatever needs to be changed in the Makefile to
suit your system. You must use your system's C++ compiler, whatever it is. (the
Makefile assumes the GNU c++ compiler, g++ [Note the line CC = g++  -- change
the g++ in this line to whatever the name of your compiler is]) If you're not
root, and you want to use the 'make install' command, you'll have to change the
INSTALLDIR = /usr/local/bin to whatever the path is to the directory you want
to put concordance in as mentioned above.

There are 3 sets of CFLAGS in the Makefile, only one of which should be
uncommented. The one including the -g switch puts debugging information into
the binary.  You need this information if you want to muck about with the
source code and run a debugger to see what happens, or to fix something if it
goes wrong. (NOTE THE INFORMATION IN THE GNU PUBLIC LICENSE ABOUT CHANGING THE
SOURCE CODE AND WHAT YOUR RESPONSIBILITIES ARE IF YOU DO SO.) A second set of
CFLAGS, containing the -O flag will compile the binary with optimizations on. 
A third set will cause the binary to be compiled without optimizations on. 
Neither the second nor the third set of CFLAGS will create a binary with
debugging code. If you compile the program without the debugging code, it will
be appreciably smaller.  After fixing the Makefile (if you need to) to your
liking, compile the program by  entering:

make

If everything goes well (you may get some warnings, but shouldn't have any
errors), you'll have an up-to-date executable, 'concordance', sitting in your
~/concordance directory.  You can manually put it wherever you keep your
executables but it is easier to use 'make install'.  I usually put new
non-system apps in /usr/local/bin  - but then I'm root on the box here.

If you are root, to install the binary to /usr/local/bin and the man page to
/usr/local/man/man1, automatically, as mentioned above, enter:

make install

If you are not root, you can edit the Makefile and change the INSTALLDIR and
LOCALMANPAGEDIR make macros to point to the locations in which you install your
own binaries and man pages and then run 'make install.'


UNINSTALLING:

To remove the binary and the man pages, run:

make uninstall

(trusting, of course, that any changes needed to INSTALLDIR and LOCALMANPAGEDIR
have been made).  'make uninstall' will remove (using rm -f) the binary and the
man pages from their locations.

USING CONCORDANCE:
The following instructions tell how to use concordance:

USAGE: 
concordance [-p[q][n:num] | -l[q][n:num] | -s[q]] filename [outfile]

filename is the usual file[path]name to the file for which you wish to make a
concordance.  -p, -s, and -l are main switches by which you can tell the
program whether you wish to concordance words in the file by the page on which
they are found, by the verse in which they are found, or by the line on which
they are found.  The program will consider a page to be delimited by ASCII char
# 12 (decimal) - the formfeed character.  Every formfeed character passed will
increment the page counter by 1 page.  To indicate verses, verse numbers in the
file must have the form 'number>': e.g.: 1>  for verse 1, 2> for verse 2, and
so forth.  Verse numbers must precede the verse they delimit.  Line numbers are
incremented by the carriage return character, ASCII character 13 (decimal) 0x0D
(hex).  Default operation (no switches indicated on the command line) is to
indicate the page number in the file on which a word will be found. Note that
only one page/line/verse switch at a time is allowed!

The auxiliary parameter, n:number, can be appended to any of the type of count
switches except the -s (stanza location) switches and the beginning number of
the line, or page will be changed to the number following the n:. Since stanzas
are expected to be manually numbered, the stanza switch will not accept the
'n:num' parameter (you can add it but it will do nothing). Example:

concordance MY.TXT -pn:40 MYCORD [Enter]

...would cause the text file MY.TXT to be concordanced by pages with the first
page considered to be page 40.  Output would be put in the files MYCORD.wrd and
MYCORD.abc.

concordance -s MY.TXT [Enter]

...would cause the text file MYPOEM.TXT to be concordanced by verses delineated
as 1> 2> 3> etc. and the output concordance file to be MYPOEM.wrd with the
alphabet use file being MYPOEM.abc.  Note that when using the stanza switch,
whatever the number is in the stanza number indicator in the file, 'num>', that
is the number the words in that stanza will be listed as included in.  The
stanza numbering in the file does not have to be in numerical order.  But note
that any words after any stanza indicator, including prose, commentary, etc.,
will be listed as located in that stanza until the next stanza number indicator
is run across.

concordance will create 2 files, one named filename.wds which contains a
concordance of all the words in the file 'filename.ext'.  The words are listed
alphabetically.  The number preceding the word is the word's length. the number
immediately following the word indicates the number of times that word was used
in the document filename.ext.  The succeeding numbers indicate the line numbers
in which the word was found.  Each linefeed in the document is considered
indicative of a new line.

In addition to producing the file 'filename.wds', concordance also outputs a
document named 'filename.abc' that contains a listing of the letters of the
English alphabet, and the numerals 0-9, whose usage in the document
'filename.ext' has been counted and graphed.

After running concordance, look for these two files named for the file that was
concordanced.  They may be examined using the command 'cat', 'less', etc.  or
by using your favorite editor.

Please Note that documents that are too long will not be able to be
concordanced if the memory in the machine and its virtual memory is
insufficient to allow concordancing.  To concordance this sort of Larger
document, break it into smaller files, concordance each piece (starting the
concordance with the n: auxiliary parameter with the beginning line or page
number the file would have in the entire document, and when all the pieces have
been concordanced, concatenate and sort the concordances from them into 1
concordance for the entire set.  The purpose of the 'n:startingnumber' addition
to the switch is to allow the page or line numbers on successive files to be
set properly.  If the first part of a too-long file starts on page 1, and the
second part on page 25, you can concordance both parts and concatenate their
results manually by concordancing the first part with the command line:

concordance MYFILE1.EXT [Enter]

and the second part with the line

concordance MYFILE2.EXT -n:25 [Enter]

The second file will begin being concordanced with its first page considered
by the concordancing program to be page 25.

Concordancing long files by line can be done in the same manner.  The
'n:number' switch does not work for stanzas, however.  If a file is
concordanced by stanzas, the stanza numbers are set to the actual stanza
numbers indicated by the stanza indicators: 'number>'.  5> will be identified
as stanza 5, 14> as stanza 14.  Stanzas, as mentioned above, are set by the
delineated stanza numbers, not by incrementation as are pages and lines. 
However, since stanzas are numbered by stanza indicators 1> 2> 3>....n>,
smaller files from a file broken down will still have the proper stanza
numbering.

Files created by word processing programs can be saved as DOS or ASCII text
with hard returns on each line.  It may be necessary to page the document by
adding ASCII Formfeed character FF, decimal 12, hex 0x0C, at the exact point
where each page break comes in order to count pages accurately.  Without ASCII
formfeed characters, a document will be considered to be one long page 1!

If you have suggestions or bug reports regarding this program, the author 
would be happy to hear them.  You may contact me at:

Ralph L. Meyer
R & R Computing Services
39 Nelson Ave.
Spotswood, NJ 08884

or send e-mail to:
meyer@aragon.princeton.edu

PS: This thing was originally written a couple of years ago in MS-DOG (before
Linux was on the scene!) and it may contain some stuff/comments in the source,
etc. that has applicability to MS-DOG, as, in porting it to Linux I may have
inadvertantly missed some stuff/comments that related originally to MS-DOG or
the old Borland compiler(s). My apologies.

I think the thing is working all right now.  I've tested it on a number of
different files and in a number of different circumstances, so it should be OK,
BUT....  SEE the GNU Public License that accompanies this program for a
complete statement of the lack of warranty of any sort and about hacking the
program for yourself and what is required of you to do so.  If the program eats
your dog or makes untoward advances to your floppy disks, that's YOUR problem!
