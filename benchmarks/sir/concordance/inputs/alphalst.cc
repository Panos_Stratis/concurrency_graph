/*
# $Header: /home/ralph/progs/concordance/RCS/alphalst.cc,v 0.3 1996/11/26 23:52:11 ralph Exp ralph $
# $Date: 1996/11/26 23:52:11 $
# $Author: ralph $
# $Log: alphalst.cc,v $
# Revision 0.3  1996/11/26 23:52:11  ralph
# Changed loop ints to unsigned ints to get rid of warnings that there were comparisons between signed and unsigned ints.
# Change.
# [AChanged alphagbte beet type from unsigned to sighned char.
#
# Revision 0.2  1996/11/23 00:51:19  ralph
# Added GNU public license.
#
# Revision 0.1  1996/11/10 22:08:51  ralph
# Alpha test version.
#
# Revision 1.1  1996/11/10 20:41:50  ralph
# Initial revision
#
# Revision 0.1  1996/05/21 22:59:26  ralph
# Initial revision
#
*/
/* Edited: 12-30-93 13:18 */
/************************************************************************
* File: alphalst.cc - used by Concordancing program
* Creates a count of the usage of letters in a document;
* Keeps track of percentage of each character's usage of total
* eof functions in concutils.cc file.
* Keeps track of total alpha/numeric characters
* Copyright (C) 12-16-93 Ralph L. Meyer - R & R Consulting
* 39 Nelson Avenue, Spotswood, NJ 08884
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* Bug reports and suggestions can be sent to:
* meyer@princeton.edu
* Ralph L. Meyer, 39 Nelson Ave. Spotswood, NJ 08884

DEBUGGING:
*************************************************************************/
#include "alphalst.h"

//note: this alphabet could be expanded to include ascii diacriticals!

char *alphabet={"abcdefghijklmnopqrstuvwxyz0123456789"};

//EXPERIMENTAL: UNIMPLEMENTED!: to use this expanded diacritical alphabet list...
//must change incChar function & define constant ALPHABETLEN
//char *alphabet="a������b�c�d�e����fg�hi����jklm�n�o����p��qrs�t�u�����vwxy�z�0123456789";

enum SORTORDER {descending,ascending};

//*****  class CharCt functions* *********************************************************

CharCt &CharCt::operator = (CharCt &cc)
{
   character=cc.getCharacter();
   useAmt=cc.getUse();
   percentUse=cc.getPercentUse();
   return *this;
}

CharCt &CharCt::operator = (char &ch)
{
   character=ch;
   useAmt=0;
   percentUse=0.0;
   return *this;
}

int CharCt::operator == (CharCt &cc)
{
   return character==cc.getCharacter() ? 1 : 0;
}

int CharCt::operator < (CharCt &cc)
{
   return character < cc.getCharacter() ? 1 : 0;
}

int CharCt::operator > (CharCt &cc)
{
   return character > cc.getCharacter() ? 1 : 0;
}

void CharCt::saveChar(FILE *fp)
{
   fwrite(&character,sizeof(char),1,fp);
   fwrite(&useAmt,sizeof(long),1,fp);
   fwrite(&percentUse,sizeof(float),1,fp);
}

void CharCt::loadChar(FILE *fp)
{
   fread(&character,sizeof(char),1,fp);
   fread(&useAmt,sizeof(long),1,fp);
   fread(&percentUse,sizeof(float),1,fp);
}

istream &operator>>(istream &is,CharCt &cc)
{
   is >> cc.character;
   cc.useAmt=1;
   return is;
}

ostream &operator<<(ostream &os,CharCt &cc)
{
   os.width(2);
   os.fill(' ');
   os << cc.character << ' ';
   os.width(5);
   os.fill('.');
   os << cc.useAmt;
   os.width(6);
   os.fill(' ');
   os.precision(2);
   os << cc.percentUse*100 << "% ";
   for (int n=0;n<(cc.percentUse*100);n++)
      os << cc.character;  //put the char in file as graph
//    os << '*';
   return os;
}

//***** End of CharCt *********************************************************


//*****  CharCtVector functions *********************************************************

CharCtVector::CharCtVector(void)
   :totAlphaNums(0)
{
   for (int n=0;n<ALPHABETLEN;n++){
      charVector[n]=*(alphabet+n);  //use in relation to overloaded = that sets usage to 0
   }
}

CharCtVector::CharCtVector(CharCtVector &ccv)
   :totAlphaNums(0)
{
   for (int n=0;n<ALPHABETLEN;n++){
      charVector[n].setCharacter(ccv[n].getCharacter(),ccv[n].getUse());
   }
}

void CharCtVector::saveVector(FILE *fp)
{
   if (totAlphaNums>0)doPercentages();
   fwrite(&totAlphaNums,sizeof(long),1,fp);
   for (int n=0;n<ALPHABETLEN;n++){
      charVector[n].saveChar(fp);
   }
}

void CharCtVector::loadVector(FILE *fp)
{
   fread(&totAlphaNums,sizeof(long),1,fp);
   for (int n=0;n<ALPHABETLEN;n++){
      charVector[n].loadChar(fp);
   }
}

void CharCtVector::incChar(int ch)   //increment character ch
{
   int index;
   if(isalpha(ch)){
      index=tolower(ch)-'a';               //index into the vector to the char
      charVector[index].incChar();
      totAlphaNums++;
   }
   else if (isdigit(ch)){
      index='z'-'a'+ch-'/';       //index to the numeral
      charVector[index].incChar();
      totAlphaNums++;
   }
   else return;   //character wasn't an alphanumeric. don't count.
}

void CharCtVector::doPercentages(void)  //set percentages of total alphanumeric characters for all characters
{
   if (totAlphaNums>0)
      for (int n=0;n<ALPHABETLEN;n++){
         charVector[n].setPercent(charVector[n].getUse()/float(totAlphaNums));
      }
}

CharCt &CharCtVector::operator[](int elem)
{
   assert (elem>=0 && elem < ALPHABETLEN);
   return charVector[elem];
}

ostream &operator<<(ostream &os,CharCtVector &ccv)
{
   if (ccv.totAlphaNums>0)ccv.doPercentages();
   for (int n=0;n<ALPHABETLEN;n++){
      os << ccv[n] << endl;
   }
   os << endl << "Total Characters: "<< ccv.totAlphaNums << endl;
   return os;
}

//***** end of CharCtVector class  *********************************************************
