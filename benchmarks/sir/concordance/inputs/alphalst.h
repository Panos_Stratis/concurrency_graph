/*
# $Header: /home/ralph/progs/concordance/RCS/alphalst.h,v 0.2 1996/11/23 00:53:01 ralph Exp ralph $
# $Date: 1996/11/23 00:53:01 $
# $Author: ralph $
# $Log: alphalst.h,v $
# Revision 0.2  1996/11/23 00:53:01  ralph
# Added GNU public license.
#
# Revision 0.1  1996/11/10 20:50:21  ralph
# Alpha version
#
# Revision 0.1  1996/11/10 20:42:13  ralph
# Initial revision
#
# Revision 0.1  1996/05/21 23:00:38  ralph
# Initial revision
#
*/
/* Edited: 12-23-93 09:10 */
/*************************************************************
* File: ALPHALST.H -header file for alphabet counting class for
* concordancing program
* Copyright (C) 12-16-93 Ralph L. Meyer - R & R Consulting
* 39 Nelson Avenue, Spotswood, NJ 08884
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
* 
* Bug reports and suggestions can be sent to:
* meyer@princeton.edu
* Ralph L. Meyer, 39 Nelson Ave. Spotswood, NJ 08884 
* 
NOTES:
**************************************************************/

#ifndef _ALPHALIST_
#define _ALPHALIST_

#define ALPHABETLEN 36	//length of alphabet + numerals 0-9

#include "standard.h"

class CharCtVector;	//advance declaration

//*****	class CharCt ************************************************

/*this class keeps track of an individual character & the amount of its usage
* and is used in conjunction with CharCtVector
*/
class CharCt{
	unsigned char character; //an individual character entry
	long useAmt;  //the number of times the character was used in a file
	float percentUse; //percentage of total characters used

	CharCt(void):character('\0'),useAmt(0),percentUse(0.0){}
	CharCt(unsigned char ch,long use=0) :character(ch),useAmt(use),percentUse(0.0){}
	CharCt(const CharCt &cc):character(cc.getCharacter()),useAmt(cc.getUse()),percentUse(cc.getPercentUse()){}
	unsigned char getCharacter(void) const { return character; }
	void setCharacter(char ch, long use=0) { character = (unsigned char)ch; useAmt = use; }
	void setCharacter(unsigned char ch, long use=0) { character = ch; useAmt = use; }
	void incChar(void){useAmt++;}
	long getUse(void) const { return useAmt; }
	float getPercentUse(void) const { return percentUse; }
	void setPercent(float pct){percentUse=pct;}
	CharCt &operator = (CharCt &cc);
	CharCt &operator = (char &ch);
	int operator == (CharCt &cc);
	int operator < (CharCt &cc);
	int operator > (CharCt &cc);
	void saveChar(FILE *fp);
	void loadChar(FILE *fp);

public:
	friend class CharCtVector;
	friend istream &operator>>(istream &is,CharCt &cc);
	friend ostream &operator<<(ostream &os,CharCt &cc);
};

//*****	class CharCtVector *********************************************************
/* This class is a vector of the character counts of characters in the alphabet.
*  the individual members of the vector are instantiations of the CharCt class.
*/
class CharCtVector{
	CharCt charVector[ALPHABETLEN];	 //vector of characters a-z,0-9
	long totAlphaNums;
public:
	CharCtVector(void);
	CharCtVector(CharCtVector &ccv);
	void saveVector(FILE *fp);
	void loadVector(FILE *fp);
	void incChar(int ch);	 //increment character ch
	void doPercentages(void);	//set percentages of total alphanumeric characters for all characters
	CharCt &operator[](int elem);
	friend ostream &operator<<(ostream &os,CharCtVector &ccv);
};

#endif
