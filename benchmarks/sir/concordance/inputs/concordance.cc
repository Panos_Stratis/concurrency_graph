/*
# $Header: /home/ralph/progs/concordance/RCS/concordance.cc,v 0.5 1996/11/26 23:49:48 ralph Exp ralph $
# $Date: 1996/11/26 23:49:48 $
# $Author: ralph $
# $Log: concordance.cc,v $
# Revision 0.5  1996/11/26 23:49:48  ralph
# Fixed SIGSEG seg fault occurring with 4 letter filenames.
# Checked and changed instructions so they all conform to UNIX standards
# set up in file.
# Made cosmetic changes to copyright function.
#
# Revision 0.4  1996/11/23 12:33:45  ralph
# main() rewritten to handle UNIX-like type parameter switches.
# function copyright() to put copyright(left) on screen.
# parameters to parse() - which must be modified - changed/added.
#
# Revision 0.3  1996/11/23 00:50:20  ralph
# Added function copyright(), GNU public license.
#
# Revision 0.2  1996/11/21 22:13:25  ralph
# Minor cosmetic changes. Fixed bug with .wds file.
#
# Revision 0.1  1996/11/16 18:47:12  ralph
# Program runs as of this date, and compiles well, but there is a problem:
# It only writes to the .wds file 2 words and then then quits.  It writes the .abc
# file all right, and apparently properly, though I haven't checked the
# counts yet.
#
*
* File: concordance.cc - main file for program 'concordancer'
* Copyright (C) 12-16-93 Ralph L. Meyer - R & R Consulting
* 39 Nelson Avenue, Spotswood, NJ 08884
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* Bug reports and suggestions can be sent to:
* meyer@princeton.edu
* Ralph L. Meyer, 39 Nelson Ave. Spotswood, NJ 08884
*
* NOTES & SUGGESTIONS TO MYSELF:
*
* DONE 1. Could add command line statement of beginning line number
* as an argv.  Words on first line of the document will be considered as
* beginning on line ...whatever the beginning line number is....
* DONE 2. Add word counter - number of words in document
* DONE 3. # of lines in document
* DONE 4. Add facilities to do stanzas as 57>
* DONE 5. Add facilities to do pages as on formfeed char ASCII 12
* 6. Alter line count saving not to save a line already present
*    in Word.cpp locus
* 7. DOS - Count Word/WordPerfect page breaks? - necessitates seeing what
*    codes these programs use to break/count pages.
* DONE 8. UNIX - set argvs to take flags before filename
* 9. Average the word lengths at end of .abc file...  keep a running count of the
*    number of words in the file, and add the size of the words to create a word
*    length average.
*/


#define MAXBFRSZ  80 //max size of word holding buffer
#define FILESEGMENT  512 //length of input file to read at one time
#define EXTENSIONLEN 5   //additional length to add to filename buffer for .wds, .abc file extensions.


#include <new.h>
#include "standard.h"
#include "word.h"
#include "alphalst.h"


//global - for use by outOfMem();
unsigned int locNum=1;   //line/page/stanza number of document determined by CR/FF/n>.
unsigned int beginningLocNum=locNum; //beginning count of lines
long fileLoc=1;      //read location in file
long wordCount=0;    //number of words in file

//external functions (in concutls.cpp | concutils.cc):
extern int ateof(istream &instream);
extern int ateof(FILE *fp);
extern void copyright(void);//put copyright on screen at program start.
extern char *lowerCaseString(char *stringToLc);
extern void getFilenameOnly(char *fname);
extern BOOLEAN isAlphaDiacritic(int c);
extern BOOLEAN isFlagError(BOOLEAN stanzaflag,BOOLEAN pageflag,BOOLEAN lineflag);
#ifdef OS_DOS
   extern void propeller(int xl=0,int yl=0);
#endif

//forward declarations (functions in this file):
void parse(char *fname,char countType,char *ofname,BOOLEAN quietflag);   //parse file into words
void instructions(void);   //puts instructions for use on screen
void copyright(void);   //puts copyright on screen
void saveConFile(char *origFn, char *conFn, char ctType,WordList &wl,BOOLEAN quiet);   //save concordance file
void saveAlphabetFile(char *origFn, char *alphaFn,CharCtVector &ccv,BOOLEAN quiet); //save alphabet count file
void outOfMem(void);  //new() error handler

int main(int argc, char **argv)
{
    char *outfile;
    int n, ndx;
    BOOLEAN QuietFlag=OFF; //indicate whether to put stuff on screen while operating

    copyright();

    if (argc<2){   //if only appname is called, provide instructions
       instructions();
       return 0;
    }

    //if help is asked for, provide instructions
    else if(strcmp(argv[1],"-?")==0||strcmp(argv[1],"-h")==0||strcmp(argv[1],"--help")==0){
       instructions();
       return 0;
    }

    if (argc<4){   //no outfile name given on command line: set as flag to  use defaults
       outfile=0;
    }
    else{ //otherwise use the name of the outfile given by operator:
       outfile=new char[strlen(argv[3])];
       assert(outfile!=0);
       strcpy(outfile,argv[3]);
   }

   set_new_handler(outOfMem);    //Borland c++ V. 3.1 new() error handler changer

   char numBfr[12];  //buffer to hold command line page/stanza/line number to start


#ifdef OS_UNIX //this block is set to handle command line switches as UNIX does: argv[1] is switch
   enum CtType{UNINDICATED,PAGE,LINE,STANZA}; //command line switch indicators
      CtType countType=UNINDICATED;
   if (argc > 2){    //operator has included switches (segfaulted here without argc > 2 !)
      if (strstr(argv[1],"n:")){ //determine starting p/line/v number if operator has indicated
         for (n=0;argv[1][n]!=':';n++);//move past the colon
         n++;
         for (ndx=0;ndx<12&&argv[1][n];ndx++,n++){
            numBfr[ndx]=argv[1][n];
         }

      numBfr[ndx]='\0';
      for (n=0;n<12&&numBfr[n];n++) //check to be surethere is a number in the buffer
         if (!isdigit(numBfr[n])){
            cerr << "\n\nSYNTAX ERROR on command line: switch -[pvl]n: did not contain a proper integer.\n";
            exit(1);
         }
      locNum=beginningLocNum=atoi(numBfr);
   }

   //find out what kind of numbering is to be used in determining word locations:

   for(n=1; n < argc; n++){
      if(strstr(argv[n],"-")){    //if the arg is a switch...
         if(strstr(argv[n],"p"))
            countType=PAGE;
         else if(strstr(argv[n],"l"))
            countType=LINE;
         else if(strstr(argv[n],"s"))
            countType=STANZA;
         else
            countType=PAGE;
         if(strstr(argv[n],"q"))
            QuietFlag=ON;
      }
   }
   switch(countType){
      case STANZA:
         parse(argv[2],'s',outfile,QuietFlag);
         break;
      case LINE:
         parse(argv[2],'l',outfile,QuietFlag);
         break;
      case PAGE:
         parse(argv[2],'p',outfile,QuietFlag);
         break;
      case UNINDICATED:
         parse(argv[2],'p',outfile,QuietFlag);
         break;
      }
   }

   else parse(argv[1],'p',outfile,QuietFlag);  //default choice if none is given: concordance by pages

#endif //end of ifdef UNIX


#ifdef OS_DOS //This block is set to handle command line switches as DOS does.
   if (argc > 2){
      if (strstr(argv[2],"n:")){ //program segfaults here without 2nd parameter (argc>2)

         for (n=0;argv[2][n]!=':';n++);//move past the colon
         n++;
         for (ndx=0;ndx<12&&argv[2][n];ndx++,n++){
            numBfr[ndx]=argv[2][n];
         }

         numBfr[ndx]='\0';
         for (n=0;n<12&&numBfr[n];n++) //check to be surethere is a number in the buffer
            if (!isdigit(numBfr[n])){
               cerr << "\n\nSYNTAX ERROR on command line: switch -n: did not contain a proper integer.\n";
               exit(1);
            }
            locNum=beginningLocNum=atoi(numBfr);
      }

   //find out what kind of numbering is to be used in determining word locations:
      if (strstr(argv[2],"-l"))
         parse(argv[1],'l',outfile,QuietFlag);
      else if (strstr(argv[2],"-L"))
         parse(argv[1],'l',outfile,QuietFlag);
      else if (strstr(argv[2],"-s"))
         parse(argv[1],'s',outfile,QuietFlag);
      else if (strstr(argv[2],"-S"))
         parse(argv[1],'s',outfile,QuietFlag);
      else{
         parse(argv[1],'p',outfile,QuietFlag);  //default choice: concordance by pages
      }
   }

   else parse(argv[1],'p',outfile,QuietFlag);  //default choice if none is given: concordance by pages

#endif   //end of ifdef OS_DOS

    set_new_handler(0);         //use with Borland C++ V. 3.1 and compact model

    delete [] outfile;
    return 0;
}


//**copyright(void)**************************************************************
// put copyright on screen
void copyright(void)
{
    cout << "\nThis is Concordance, a program to make concordances for text files.\n"
   << "$Id: concordance.cc,v 0.5 1996/11/26 23:49:48 ralph Exp ralph $\n"
   << "Copyright (C) 1996 Ralph L. Meyer, Spotswood, NJ 08884.\n"
   << "Concordance is licensed to users only under the conditions stated in the\n"
   << "GNU public license that comes with the distribution of this program.\n"
   << "Concordance comes with ABSOLUTELY NO WARRANTY; for details see the GNU public\n"
   << "license that accompanies this program.  You are welcome to redistribute this\n"
   << "software under certain conditions; for details see the GNU public license.\n"
   << "Send BUG reports to: meyer@princeton.edu.\n\n";
}



// print usage instructions on screen:
#ifdef OS_UNIX
void instructions(void)
{
    cout << "\nUSAGE:\n"
      << "concordance [-p[q][n:num] or -l[q][n:num]] or -s[q][num]] filename [outfile]"
      << "\nfilename is the usual /filepath/fname to the file for which you wish to make"
      << "\na concordance.  -p, -l, and -s are switches by which you can tell the program"
      << "\nwhether you wish to concordance words in the file by the page on which they"
      << "\nare found, by the line on which they are found, or by the stanza in which"
      << "\nthey are found.  The program considers a page to be delimited by ASCII char"
      << "\n12 (decimal) - FF, the formfeed character.  Every formfeed character passed will"
      << "\nincrement the page counter by 1 page.  To indicate stanzas, stanza numbers in"
      << "\nthe file must have the form: number>. i.e.: 1>  for stanza 1, 2> for stanza 2,"
      << "\nand so forth.  Stanza numbers must precede the stanza they delimit.  Line"
      << "\nnumbers are incremented by the Line feed  character, ASCII character 10"
      << "\n(decimal).  Default operation (no switches indicated on the command line)"
      << "\nis to indicate the page number in the file on which a word will be found."
      << "\nNote that only one page/line/stanza switch at a time is allowed!"
      << "\nThe addition n:number can be appended to any of the type-of-count switches"
      << "\nand the beginning number of the page or line will be changed to the number"
      << "\nfollowing the n: (n:num has no effect on stanza counting).  If an outfilename"
      << "\nis used, there MUST be a switch indicating the count type on the command line."
      << "\nThe auxiliary switch \'q\' causes only the copyright to be output to stdout."
      << "\nWithout \'q\' concordance outputs information on the filenames in which it has"
      << "\nstored results."
      << "\nPress enter to continue reading.";
    getchar();
    cout << "\nExample:"
      << "\n\nconcordance -pqn:40 MY.TXT MYCORD [Enter]"
      << "\n\n...would cause the text file MY.TXT to be concordanced by pages with the first"
      << "\npage considered to be page 40. No messages would be output to screen but the"
      << "\nopening copyright notice.  Output would be put in the files MYCORD.wrd and"
      << "\nMYCORD.abc .  Concordance creates 2 files, one named \'filename.wds\', which"
      << "\ncontains a concordance of all the words in the file \'filename\'.  The words are"
      << "\nlisted alphabetically.  The number preceding the word is the word's length."
      << "\nthe number immediately following the word indicates the number of times"
      << "\nthat word was used in the document filename.  The succeeding numbers"
      << "\nindicate the page/line/stanza numbers in which the word was found."
      << "\n\n"
      << "\nIn addition to producing the file \'filename.wds\', concordance also outputs"
      << "\na document named \'filename.abc\' that contains a listing of the letters"
      << "\nof the English alphabet, and the numerals 0-9, whose usage in the document"
      << "\n\'filename.ext\' has been counted and graphed.\n"
      << "\nAfter running concordance, look for these two files named for the file that was"
      << "\nconcordanced or the name you gave the outfile.  They may be examined using"
      << "\nthe command \'cat\', \'less\', or by using your favorite text editor."
      << "\n\nPlease Note that very long documents may need to be separated into pieces"
      << "\nto be concordanced.  Larger documents should be broken into smaller pieces,"
      << "\nPress enter to continue reading.";
    getchar();
    cout  << "\neach piece concordanced, and the concordances from each piece concatenated."
      << "\n\nTo get this help screen again, type \'concordance -? [Enter]\' at the shell"
      << "\nprompt, see the file README, or check the man pages.\n\n";

    return;
}
#endif
#ifdef OS_DOS
void instructions(void)
{
   cout  << "\nUSAGE: concrdnc filepath.ext [/p or v or l [& n:num]] [outfile] [Enter]"
         << "\nfilepath.exe is the usual DOS filepath to the file for which you wish to make"
         << "\na concordance.  /p, /v, and /l are switches by which you can tell the program"
         << "\nwhether you wish to concordance words in the file by the page on which they"
         << "\nare found, by the verse in which they are found, or by the line on which they"
         << "\nare found.  The program will consider a page to be delimited by ASCII char #"
         << "\n12 (decimal) - the formfeed character.  Every formfeed character passed will"
         << "\nincrement the page counter by 1 page.  To indicate verses, verse numbers in"
         << "\nthe file must have the form: number>. i.e.: 12>  for verse 12, 5> for verse 5,"
         << "\nand so forth.  Verse numbers must precede the verse they delimit.  Line"
         << "\nnumbers are incremented by the carriage return character, ASCII character 13"
         << "\n(decimal).  Default operation (no switches indicated on the command line)"
         << "\nis to indicate the page number in the file on which a word will be found."
         << "\nNote that only one page/line/verse switch at a time is allowed!"
         << "\nThe addition n:number can be appended to any of the type of count switches"
         << "\nand the beginning number of the line, or page will be changed to the number"
         << "\nfollowing the n:.  If an outfile is used, it MUST be preceded by a switch"
         << "\nindicating the count type."
         << "\nExample:"
         << "\n\nCONCRDNC MY.TXT /pn:40 MYCORD [Enter]"
         << "\n\n...would cause the text file MY.TXT to be concordanced by pages with the first"
         << "\npage considered to be page 40.  Output would be put in the files MYCORD.WRD and"
         << "\nMYCORD.ABC.  Press a key to continue reading.";
   getchar();
   cout  << "\nCONCRDNC will create 2 files, one named filename.wds which contains a"
         << "\nconcordance of all the words in the file \'filename.ext\'.  The words are"
         << "\nlisted alphabetically.  The number preceding the word is the word's length."
         << "\nthe number immediately following the word indicates the number of times"
         << "\nthat word was used in the document filename.ext.  The succeeding numbers"
         << "\nindicate the page/verse/line numbers in which the word was found.  (Each"
         << "\ncarriage return in the document is considered indicative of a new line.)\n"
         << "\nIn addition to producing the file \'filename.wds\', concrdnc also outputs"
         << "\na document named \'filename.abc\' that contains a listing of the letters"
         << "\nof the English alphabet, and the numerals 0-9, whose usage in the document"
         << "\n\'filename.ext\' has been counted and graphed.\n"
         << "\nAfter running CONCRDNC, look for these two files named for the file that was"
         << "\nconcordanced or the name you gave the outfile.  They may be examined using"
         << "\nthe DOS command \'TYPE\' or by using your favorite text editor."
         << "\n\nPlease Note that very long documents may need to be separated into pieces"
         << "\nto be concordanced.  Larger documents should be broken into smaller pieces,"
         << "\neach piece concordanced, and the concordances from each piece concatenated."
         << "\n\nTo get this help screen again, type \'concrdnc ? [Enter]\' at the DOS"
         << "\nprompt, or see the file READ.ME\n\n\n";

   return;
}
#endif

//parser of words on line numbers, pages, stanzas.
void parse(char *fname,char countType,char *ofname,BOOLEAN quiet=OFF)
{
   FILE *fp; //read-in file, char file, concordance file
   char *filenameBfr;
   char *conFn;      //filename of the concordance words file
   char *alphaFn; //filename of the concordance alphabet characters used file
   char *cext=".wds";   //extension of the word list file
   char *aext=".abc";   //extension of the alphabet file
   int fnlen;
   CharCtVector ccv; //character counter
   WordList wl;   //the concordance: the linked list of words and information on their locations

   if((fp=fopen(fname,"rb"))==0){   //open file to be concordanced for read
      cerr << "Couldn't open file" << fname << endl;
      perror("File opening error");
      assert (fp);   //make sure it opened OK
   }

    //Set up filenames for output .wds & .abc results files:
   if (ofname==0){   //if operator hasn't provided an output file name
      filenameBfr=new char[(fnlen=strlen(fname)+EXTENSIONLEN)];
      assert(filenameBfr!=0);
      strcpy(filenameBfr,fname);  //use the filename of file to be concordanced
   }
   else {   //if operator has indicated the name of the output files:
      filenameBfr=new char[(fnlen=strlen(ofname)+EXTENSIONLEN)];
      assert(filenameBfr!=0);
      strcpy(filenameBfr,ofname); //otherwise use provided output file name
   }

   getFilenameOnly(filenameBfr); //get a filename only for later storage

   conFn=new char[fnlen];  //prepare the concordance .wds filename
   assert(conFn!=0);
   strcpy(conFn,filenameBfr);
   strcat(conFn,cext);

   alphaFn=new char[fnlen];   //prepare the alphabet character use filename
   assert(alphaFn!=0);
   strcpy(alphaFn,filenameBfr);
   strcat(alphaFn,aext);

   delete [] filenameBfr;
   //End of setting up output filenames.

   //data parser--parse the data from the input file:
   char wordBfr[MAXBFRSZ];    //buffer to hold word
   int wordFlag=0;   //indicator of word in wordBfr
   int wordNdx=0;    //index into wordBfr
   int isNum=0;   //indicator that information in buffer is a number
   int isWord=0;  //indicator that information in buffer is a word
   size_t charsRead; //number of characters actually read from the file.
    //*
#ifdef OS_DOS
   cout << "\n\nMaking Concordance. Please Wait!...  (1 dot = 125 words processed)"
      << "\n                                     (1 line of dots = 10,000 words processed)" << endl;
#endif
#ifdef OS_UNIX
   if(!quiet)cout << "\n\nMaking Condordance. Depending on file length, this may take awhile.\n";
#endif
   unsigned char inbuffer[FILESEGMENT];  //holding buffer for reads

   while (!ateof(fp)) {
      charsRead=fread(inbuffer,sizeof(char),sizeof(inbuffer),fp);
      assert(charsRead>0);
      for (unsigned int n=0;n<FILESEGMENT&&n<charsRead&&char(inbuffer[n])!=EOF;n++){
         ccv.incChar(inbuffer[n]);
         if (isAlphaDiacritic(inbuffer[n])||isdigit(inbuffer[n])){ //put inbuffer[n] characters and digits into initial buffer
            if (!wordFlag)wordFlag=1;   //as a word
            wordBfr[wordNdx++]=inbuffer[n];
            if (isdigit(inbuffer[n])&&!isWord)isNum=YES; //if inbuffer[n] character is a digit and there is no indication that there is a word
            else if (!isWord){
               isWord=YES;  //if a digit has a letter included, inbuffer[n] change to word, otherwise it is a word
               isNum=NO;    //added along with &&(inbuffer[n]!='>'&&!isWord) below
            }
         }
         else if (wordFlag&&!(inbuffer[n]=='>'&&isNum)){ //inbuffer[n]character is whitespace and there's a word in bfr
            wordBfr[wordNdx]='\0';  //NULL end the word in wordBfr
            wl.addWord(wordBfr,locNum);  //add the word to the concordance with its location or add the location to a word already there
            fileLoc=ftell(fp); //note location in file
            wordCount++;
#ifdef OS_DOS
            if(wordCount%5==0)propeller();
            if(wordCount%125==0) cout << '.';   //One line of dots on screen = 10,000 words processed.
#endif
            wordNdx=isWord=wordFlag=isNum=0;
         }

         //increment the location in the file according to operator indication of what type of location to indicate:
#ifdef OS_DOS
         if (inbuffer[n] == CR && countType == 'l') //if whitespace is carriage return & we're counting lines...
#endif
#ifdef OS_UNIX
         if (inbuffer[n] == LF && countType == 'l') //if whitespace is Linefeed & we're counting lines...
#endif
            locNum++; //increment locnum as line number on carriage return
         else if (inbuffer[n] == FF && countType == 'p') //if whitespace if a formfeed & we're counting pages...
            locNum++; //increment locnum as page number on formfeed
         else if (inbuffer[n] == '>' && countType == 's' && isNum){   //set stanza to stanza number
            wordBfr[wordNdx]='\0';  //end the string
            locNum=atoi(wordBfr);   //put the number into location number
            wordNdx=isWord=wordFlag=isNum=0; //start with new word
         }
      }
   }
   fclose(fp); //close the input file to be concordanced.

   saveConFile(fname,conFn,countType,wl,quiet); //save the list of words, usage amounts, and locations.
   saveAlphabetFile(fname,alphaFn,ccv,quiet);   //save the alphabet character usage listing.

   if(!quiet)
      cout << "\n\nConcordancing completed.  For concordance file, see " << conFn << "; for alphabet\n"
      << "usage file, see " << alphaFn << "\n\n";

    delete [] conFn;
    delete [] alphaFn;
}



void saveConFile(char *origFn, char *conFn, char ctType,WordList &wl,BOOLEAN
quiet=OFF)
{
   ofstream confile(conFn);
   assert(confile!=0);
   if(!quiet)cout << "\n\nSaving concordance file " << conFn << ", - concordance to file " << origFn;
   confile << "Concordance for file: " << origFn << "\n\n";
   confile << "Note: Concordancer considers a word a set of alphanumeric characters\n";
   confile << "      bounded by whitespace at each end of the set.\n\n";
   confile << "Total number of words in file: " << wordCount << "\n";

   if(ctType=='s')      confile << "Initial stanza of file was stanza " << beginningLocNum << "\n";
   else if(ctType=='p') confile << "Initial page of file was page " << beginningLocNum << "\n";
   else if(ctType=='l') confile << "Initial line of file was line " << beginningLocNum << "\n";

   if(ctType=='s')        confile << "Last stanza of file was stanza " << locNum << "\n\n";
   else if(ctType=='p')   confile << "Last page of file was page " << locNum << "\n\n";
   else if(ctType=='l')   confile << "Last line of file was line " << locNum << "\n\n";

   confile << "Word           Number of";
   if(ctType=='s') confile << "   Stanza number\n";
   else if(ctType=='p') confile << "   Page number\n";
   else if(ctType=='l') confile << "   Line number\n";
   confile << "Len  Word:         uses:   Locations:\n";
   confile << wl;
}



void saveAlphabetFile(char *origFn, char *alphaFn,CharCtVector &ccv,BOOLEAN quiet=OFF)
{
   ofstream alphafile(alphaFn);
   assert(alphafile!=0);

   if(!quiet)cout << "\n\nSaving alphabet count file " << alphaFn << ", - alphabet count for file " << origFn;
   alphafile << ccv;
}



//out of memory message to display on screen in parser:
void outOfMem(void)
{
   cerr << "\n\nERROR: You have insufficient memory to continue making a concorcance for this\n"
        << "document.  It will be necessary for you to shorten the document.  Break the document\n"
        << "into several pieces, and concatenate and sort the .wrd and .abc files that result from\n"
        << "making concordances for the several pieces.  Concordancing got to\n"
        << "pg/line number " << locNum << " in this document and " << fileLoc << " characters\n"
        << "had been read when the program had to exit due to lack of memory.\n";
   exit(1);
}

