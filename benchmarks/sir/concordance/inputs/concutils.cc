/*
# $Header: /home/ralph/progs/concordance/RCS/concutils.cc,v 0.4 1996/11/26 23:54:11 ralph Exp ralph $
# $Date: 1996/11/26 23:54:11 $
# $Author: ralph $
# $Log: concutils.cc,v $
# Revision 0.4  1996/11/26 23:54:11  ralph
# Removed copyright message function from concutils and put it in concordance.cc.
#
# Revision 0.3  1996/11/23 12:36:25  ralph
# Minor cosmetic changes to file.
#
# Revision 0.2  1996/11/23 00:52:25  ralph
# Added copyright() function., GNU public license.
#
# Revision 0.1  1996/11/10 20:53:41  ralph
# Alpha version
#
*/

/*************************************************************
* File: concutils.cc - utilities used by Concordancing program
* Copyright (C) 12-16-93 Ralph L. Meyer - R & R Consulting
* 39 Nelson Avenue, Spotswood, NJ 08884
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* Bug reports and suggestions can be sent to:
* meyer@princeton.edu
* Ralph L. Meyer, 39 Nelson Ave. Spotswood, NJ 08884
*
* NOTES: utility function file for use with concordancing program
* FUNCTIONS IN THIS FILE:
* BOOLEAN isAlphaDiacritic(int c)      //returns true if char is alphadiacritical
* BOOLEAN isAlDiaNum(int c)        //returns true if char is alphadiacritical or numeric
* int ateof(istream &instream)    //check for eof
* int ateof(FILE *fp)   //true check for eof
* void copyright()   //put copyright message on screen
* char *lowerCaseString(char *stringToLc) //lower case a string
* void getFilenameOnly(char *fname)  //return filename only in fname
* void outOfMem(void)   //message for on-screen at memory out
* int diactolower(int ch) //lowercases alphabetic chars and ascii diacriticals returns lowercased char
* int strcomp(char *str1,char *str2) //determine if strings are equal by subtracting str2 from str1 chars
*
* FOR USE WITH UNIX: (MS-DOG--LIKE FUNCTIONS):
* int wherex(void);  //finds x location of cursor on screen
* int wherex(WINDOW *); //finds y location of cursor in window
* int wherey(void);  //finds x location of cursor on screen
* int wherey(WINDOW *); //finds y location of cursor in window
* void gotoxy(int, int);   //moves cursor to x,y position on screen
* void gotoxy(WINDOW *, int, int);  //moves cursor to x,y position on screen
**************************************************************/

#include "oshdr.h"   //set the operating system

#ifdef DOS_FUNCS
#include <curses.h>

int wherex(void);
int wherex(WINDOW *);
int wherey(void);
int wherey(WINDOW *);
void gotoxy(int, int);
void gotoxy(WINDOW *, int, int);
int strcmpi(const char *, const char *);
int stricmp(const char *, const char *);

#endif


/* Edited: 12-12-93 08:30 */
/*
* Functions in this file:
* BOOLEAN isAlphaDiacritic(int c)
* BOOLEAN isAlDiaNum(int c)
* int ateof(istream &instream) //overloaded
* int ateof(FILE *fp)          //true check for eof
* char *lowerCaseString(char *stringToLc)
* char *upperCaseString(char *stringToLc)
* void getFilenameOnly(char *fname)
* int diactolower(int ch)
* int strcomp(char *str1,char *str2)
*
* Function operative if OS_DOS is defined:
* Unused in Unix.
* void propeller(int xl=0,int yl=0)
*
* Functions to mimic MS-DOG functions:
* Operative if DOS_FUNCS is defined
* if used, curses.h must be #included
* int wherex(void)
* int wherex(WINDOW *win)
* int wherey(void)
* int wherey(WINDOW *win)
* void gotoxy(int x, int y)
* void gotoxy(WINDOW *win, int x, int y)
* int strcmpi(const char str1*, const char str2*)
* int stricmp(const char str1*, const char str2*)
*/

#include "standard.h"

//isAlphaDiacritic(int char):**************************************************************

/* This function returns 0 if the character entered is not in any of the
* following characters: (ASCII codes)
* ',
* A-Z,   65 -  90
* a-z,   97 - 122
* �-�, 128 - 154
* �-�, 160 - 165
* �-�, 224 - 235
*/

//OK returns non-0 if c is an alphabetic or diacritical alphabetic
BOOLEAN isAlphaDiacritic(int c)
{
   return ((c=='\'')||
   (c>='A'&&c<='Z')||
   (c>='a'&&c<='z')||
   (c>=128&&c<=154)||
   (c>=160&&c<=165)||
   (c>=224&&c<=235))
   ? YES : NO;
}


//OK returns non-0 if c is an alphanumeric or diacritical alphanumeric
BOOLEAN isAlDiaNum(int c)
{
   return ((c=='\'')||
   (c>='0'&&c<='9')||
   (c>='A'&&c<='Z')||
   (c>='a'&&c<='z')||
   (c>=128&&c<=154)||
   (c>=160&&c<=165)||
   (c>=224&&c<=235))
   ? YES : NO;
}


//ateof(istream/FILE):**************************************************************

//OK tell if at end of fstream file on input:
//overloaded
int ateof(istream &instream)
{
   long floc = instream.tellg();
   instream.seekg(floc+2);
   if(instream.peek() == EOF){
      return EOF;
   }
   else{
      instream.seekg(floc);
      return 0;
   }
}


//OK tell is at eof with file pointers
//overloaded
int ateof(FILE *fp)  //true check for eof
{
   char chkchar;
   int check;

   fread(&chkchar,1,1,fp); //read a char to see if move past eof
   check=feof(fp);   //test for eof
   if(check) return check;
   else{
      fseek(fp,-1,SEEK_CUR);
      return check;
   }
}


//**************************************************************

//lower cases a null ended c string:
char *lowerCaseString(char *stringToLc)
{
   int n;

   for (n=0;*(stringToLc+n)!='\0';n++){
      *(stringToLc+n)=tolower(*(stringToLc+n));
   }
   *(stringToLc+n)='\0';   // added because of segfaulting SIGSEG 11/24/96 RM
   return stringToLc;
}

//upper cases a null ended c string:
char *upperCaseString(char *stringToLc)
{
   for (int n=0;*(stringToLc+n)!='\0';n++){
      *(stringToLc+n)=toupper(*(stringToLc+n));
   }
   return stringToLc;
}

//**************************************************************

//parse out the filename portion of a filename.ext string:
//stores filename portion in fname
void getFilenameOnly(char *fname)
{
   int n;
   for (n=0;*(fname+n)!='\0'&&*(fname+n)!='.';n++);
   *(fname+n)='\0';  //add the null ending to the string
}

//**************************************************************

//this function returns a lowercased alphadiacritical or the original
//character entered if not an alphadiacritical.
int diactolower(int ch)
{
   if (!(ch>='A'||ch<='Z'||ch>='a'||ch<='z'||
       ch>=128||ch<=154||ch>=160||ch<=165))return ch;
   else if (ch>='A'&&ch<='Z')return ch+'a'-'A';
   else if (ch>='a'&&ch<='z')return ch;
   else{
      switch(ch){
         case '�':
            return '�';
         case '�':
            return '�';
         case '�':
            return '�';
         case '�':
            return '�';
         case '�':
            return '�';
         case '�':
            return '�';
         case '�':
            return '�';
         case '�':
            return '�';
         default :
            return ch;
      }
   }
}

//**************************************************************
//determine if strings are equal by subtracting string 2 from string 1:
int strcomp(char *str1,char *str2)
{
   int n;

   for (n = 0; *(str1+n)!='\0'&&*(str2+n)!='\0' ; n++){
      int val;
      if ((val=(*(str1+n))-(*(str2+n)))==0);
      else if (val<0)return val;
      else if (val>0)return val;
   }
   //determine if one string is longer than the other:
   if (str1[n]==str2[n])return 0;
   else if (str1[n]>str2[n])return str1[n];
   else return -str2[n];
}


#ifdef OS_DOS
#define PROP
#elif DOS_FUNCS
#define PROP
#endif
#ifdef PROP
/*
* App was originally written for MS-DOG. These functions are to mimic the MSDOG functions
* in the original as one means of porting the program to LINUX.
* These functions use curses to perform screen activities called for above.
* Warning to programmer: note that after initscr() for curses, endscr() must be
* called before any exit()s from the program!
*/



//**************************************************************
//put a propeller on screen at the cursor location
void propeller(int xl=0,int yl=0)
{
   static int xloc, yloc, lastpresy, prop=1;
   static int started = 0; //flag-has function been called previously?
   int presx,presy;

   if(!started){
      started=1;
      if(!xl||xl<0)presx=xloc=wherex();
      else presx=xloc=xl;
      if(!yl||yl<0)lastpresy=presy=yloc=wherey();
      else lastpresy=presy=yloc=yl;
      gotoxy(xloc,yloc);
      putchar('-');
      gotoxy(presx,presy);
   }
   else if(prop%2==0){
      prop++;
      presx=wherex();
      presy=wherey();
      gotoxy(xloc,yloc);
      if(presy>lastpresy){
         lastpresy=presy;
         if(yloc>1)yloc--;
      }
      putchar('\\');
      gotoxy(presx,presy);
   }
   else if(prop%3==0){
      prop=1;
      presx=wherex();
      presy=wherey();
      gotoxy(xloc,yloc);
      if(presy>lastpresy){
         lastpresy=presy;
         if(yloc>1)yloc--;
      }
      putchar('/');
      gotoxy(presx,presy);
   }
   else{
      prop++;
      presx=wherex();
      presy=wherey();
      gotoxy(xloc,yloc);
      if(presy>lastpresy){
         lastpresy=presy;
         if(yloc>1)yloc--;
      }
      putchar('-');
      gotoxy(presx,presy);
   }
   return;
}
#endif

#ifdef DOS_FUNCS
/*Mimicing MS DOG like functions in Turbo-C:
* NOTE: that in using these functions, since they are overloaded, they MUST
* be used only in C++ programs, and g++ must be run with the switch -lcurses.
* Also: these functions must be forward declared where used as they are not found in UNIX.:
* int wherex(void);
* int wherex(WINDOW *);
* int wherey(void);
* int wherey(WINDOW *);
* void gotoxy(int, int);
* void gotoxy(WINDOW *, int, int);
*/
int wherex(void)
{
   int x, y;
   getyx(stdscr, y, x);
   return x;
}

int wherex(WINDOW *win)
{
   int x, y;
   getyx(win, y, x);
   return x;
}

int wherey(void)
{
   int x, y;
   getyx(stdscr, y, x);
   return y;
}

int wherey(WINDOW *win)
{
   int x, y;
   getyx(win, y, x);
   return y;
}

void gotoxy(int x, int y)
{
   move(y, x);
   refresh();
}

void gotoxy(WINDOW *win, int x, int y)
{
   wmove(win,y,x);
   wrefresh(win);
}

int strcmpi(const char str1*, const char str2*)
{
   return strcasecmp(str1, str2);
}

int stricmp(const char str1*, const char str2*)
{
   return strcasecmp(str1, str2);
}
#endif
