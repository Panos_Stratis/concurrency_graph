#! /bin/csh -f
echo 
echo 
echo ------------------------------------------------
echo               Running EP test cases
echo ------------------------------------------------
echo 
echo 

echo ------------------------------------------------
echo Running EP01
echo Rationale: Test concordance with one input parameter
echo Expected output: Correct concordance by page

concordance inputfiles/multiPageText

cat inputfiles/multiPageText.wds
rm inputfiles/multiPageText.wds
rm inputfiles/multiPageText.abc

echo ------------------------------------------------
echo Running EP02
echo Rationale: Test concordance with invalid number of input parameters 3
echo Expected output: An error message like "Too many parameters specified",
echo no output files are generated

concordance inputfiles/multiPageText outputFile errorFile

cat outputFile.wds
rm outputFile.wds
rm outputFile.abc

echo ------------------------------------------------ 
echo Running EP03
echo Rationale: Test input item "Existance of input file", invalid case 
echo Expected output: An error message like "nonExistingFile: The file you specified doesn't exist"

concordance nonExistingFile

echo ------------------------------------------------
echo Running EP04
echo Rationale: Test input item "Integer value for the -n parameter", invalid case - not a number
echo Expected output: An error message like "Syntax error. a is not an integer value"

concordance -pn:a inputfiles/multiPageText

cat inputfiles/multiPageText.wds
rm inputfiles/multiPageText.wds
rm inputfiles/multiPageText.abc

echo ------------------------------------------------
echo Running EP05
echo Rationale: Test -n for negative numbers
echo Expected output: An error message indicating that a number >= 0 should be provided

concordance -pn:-1 inputfiles/multiPageText

cat inputfiles/multiPageText.wds
rm inputfiles/multiPageText.wds
rm inputfiles/multiPageText.abc

echo ------------------------------------------------
echo Running EP06
echo Rationale: Test functionality of -n in combination with -p mode
echo Expected output: Standard screen output, concordance begins with page 2

concordance -pn:2 inputfiles/multiPageText > /dev/null

cat inputfiles/multiPageText.wds
rm inputfiles/multiPageText.wds
rm inputfiles/multiPageText.abc

echo ------------------------------------------------
echo Running EP08
echo Rationale: Test functionality of -n parameter in combination with -l mode
echo Expected output: Standard screen output, concordance by line number starting with line 2

concordance -ln:2 inputfiles/multiLineText > /dev/null

cat inputfiles/multiLineText.wds
rm inputfiles/multiLineText.wds
rm inputfiles/multiLineText.abc

echo ------------------------------------------------
echo Running EP09
echo Rationale: Test functionality of -n parameter in combination with -s mode
echo Expected output: Standard screen ouput, -n parameter is ignored

concordance -sn:2 inputfiles/stanzasText > /dev/null

cat inputfiles/stanzasText.wds
rm inputfiles/stanzasText.wds
rm inputfiles/stanzasText.abc

echo ------------------------------------------------
echo Running EP10
echo Rationale: Test invalid stanzas, stanzas is not a number
echo Expected Output: Standard screen output, the "a>" stanza is ignored in the outputfile - or rather treated like a normal word

concordance -s inputfiles/invalidStanzasText > /dev/null

cat inputfiles/invalidStanzasText.wds
rm inputfiles/invalidStanzasText.wds
rm inputfiles/invalidStanzasText.abc


echo ------------------------------------------------
echo Running EP11
echo Rationale: Test invalid stanza, stanza is empty
echo Expected Output: Standard screen output, the empty stanza is ignored in the outputfile - or rather treated like a normal word

concordance -s inputfiles/emptyStanzasText > /dev/null

cat inputfiles/emptyStanzasText.wds
rm inputfiles/emptyStanzasText.wds
rm inputfiles/emptyStanzasText.abc

echo ------------------------------------------------
echo Running EP12
echo Rationale: Test invalid stanza, stanza is real number
echo Expected Output: Standard screen output, the stanza with the real number is ignored in the outputfile - or rather treated like a normal word

concordance -s inputfiles/realStanzasText

cat inputfiles/realStanzasText.wds
rm inputfiles/realStanzasText.wds
rm inputfiles/realStanzasText.abc

echo ------------------------------------------------
echo Running EP13
echo Rationale: Test invalid stanza, stanza is real number
echo Expected Output: Standard screen output, the stanza with the real number is ignored in the outputfile - or rather treated like a normal word

concordance -s inputfiles/realStanzasText2

cat inputfiles/realStanzasText2.wds
rm inputfiles/realStanzasText2.wds
rm inputfiles/realStanzasText2.abc

echo ------------------------------------------------
echo Running EP14
echo Rationale: Test invalid stanza, stanza is real number
echo Expected Output: Standard screen output, the stanza with the real number is ignored in the outputfile - or rather treated like a normal word

concordance -s inputfiles/realStanzasText3

cat inputfiles/realStanzasText3.wds
rm inputfiles/realStanzasText3.wds
rm inputfiles/realStanzasText3.abc

echo ------------------------------------------------

echo 
echo 
echo ------------------------------------------------
echo               Running BV test cases
echo ------------------------------------------------
echo 
echo 
echo Running BV01
echo Rationale: Test how empty files are handled by concordance
echo Expected Output: An error message indicating that the specified input file was empty, no output files are created 

concordance inputfiles/emptyFile

cat inputfiles/emptyFile.wds
rm inputfiles/emptyFile.wds
rm inputfiles/emptyFile.abc

echo ------------------------------------------------

echo Running BV02
echo Rationale: Test how very large files are handled by concordance
echo Expected output: Correct concordance by page

echo concordance veryLargeFile - TODO

echo ------------------------------------------------

echo Running BV03
echo Rationale: Test handling of the 0 number for n:num option
echo Expected Output: Correct concordance by page number, starting by number 0

concordance -pn:0 inputfiles/multiPageText > /dev/null

cat inputfiles/multiPageText.wds
rm inputfiles/multiPageText.wds
rm inputfiles/multiPageText.abc

echo ------------------------------------------------

echo Running BV04
echo Rationale: Test -n option of the upper bounds - 2147483647 - of the input item num
echo Expected output: Correct concordance by page, no error message

concordance -pn:2147483646 inputfiles/multiPageText > /dev/null

cat inputfiles/multiPageText.wds
rm inputfiles/multiPageText.wds
rm inputfiles/multiPageText.abc

echo ------------------------------------------------

echo Running BV05
echo Rationale: Test -n option of the upper bounds - 2147483647 - of the input item num
echo Expected output: Correct concordance by page, no error message

concordance -pn:2147483647 inputfiles/multiPageText > /dev/null

cat inputfiles/multiPageText.wds
rm inputfiles/multiPageText.wds
rm inputfiles/multiPageText.abc

echo ------------------------------------------------

echo Running BV06
echo Rationale: Test invalid stanzas, stanza is 0
echo Expected output: First stanza should be ignored since it is implicitly stated that the stanza must be an integer number >= 1

concordance -s inputfiles/invalidStanzasText2 > /dev/null

cat inputfiles/invalidStanzasText2.wds
rm inputfiles/invalidStanzasText2.wds
rm inputfiles/invalidStanzasText2.abc

echo ------------------------------------------------

echo Running BV07
echo Rationale: Test upper bound for stanzas, stanza numbers below upper boundary
echo Expected output: Correct concordance by stanza, no error message

concordance -s inputfiles/largeStanzasText > /dev/null

cat inputfiles/largeStanzasText.wds
rm inputfiles/largeStanzasText.wds
rm inputfiles/largeStanzasText.abc

echo ------------------------------------------------

echo Running BV08
echo Rationale: Test upper bound for stanzas, stanza number larger than upper boundary
echo Expected output: An error message indicating that the second stanza is too large, no output files are created

concordance -s inputfiles/tooLargeStanzasText

cat inputfiles/tooLargeStanzasText.wds
rm inputfiles/tooLargeStanzasText.wds
rm inputfiles/tooLargeStanzasText.abc

echo ------------------------------------------------

echo Running BV09
echo Rationale: Test a way too large stanza to cover the case that the chosen integer was larger than 32 bit
echo Expected output: An error message indicating that the second stanza is too large, no output files are created

concordance -s inputfiles/muchTooLargeStanzasText

cat inputfiles/muchTooLargeStanzasText.wds
rm inputfiles/muchTooLargeStanzasText.wds
rm inputfiles/muchTooLargeStanzasText.abc

echo ------------------------------------------------

echo 
echo 
echo ------------------------------------------------
echo               Running EG test cases
echo ------------------------------------------------
echo 
echo 

echo Running EG01
echo Rationale: Test handling of non-ascii characters
echo Expected output: Correct concordance by line, no error messages. German characters are included in the .abc file

concordance -l inputfiles/germanText > /dev/null

cat inputfiles/germanText.wds
cat inputfiles/germanText.abc
rm inputfiles/germanText.wds
rm inputfiles/germanText.abc

echo ------------------------------------------------

echo Running EG02
echo Rationale: Test how special character are handled by concordance
echo Expected output: Correct concordance by line, special characters are ignored - i.e. neither shown as words in the .wds file nore as characters in the .abc file

concordance -l inputfiles/specialText > /dev/null

cat inputfiles/specialText.wds
cat inputfiles/specialText.abc
rm inputfiles/specialText.wds
rm inputfiles/specialText.abc

echo ------------------------------------------------

echo Running EG03
echo Rationale: Test how concordance works if the outputfile already exists
echo Expected Output: Existing file gets overwritten, correct concordance by page TODO

cat outputFile.wds < blabla  
cat outputFile.abc < jaja

concordance inputfiles/multiPageText outputFile

cat outputFile.wds
cat outputFile.abc
rm outputFile.wds
rm outputFile.abc

echo ------------------------------------------------

echo Running EG04
echo Rationale: Test how concordance deals with mixed up stanzas
echo Expected Output: Correct concordance by stanza, standard screen output

concordance -s inputfiles/mixedUpStanzasText

cat inputfiles/mixedUpStanzasText.wds
rm inputfiles/mixedUpStanzasText.wds
rm inputfiles/mixedUpStanzasText.abc

echo ------------------------------------------------

echo Running EG05
echo Rationale: Test how real values are handled for the -n parameter
echo Expected Output: An error message like "Invalid value. Please enter an integer value"

concordance -pn:2.5323 inputfiles/multiPageText

cat inputfiles/multiPageText.wds
rm inputfiles/multiPageText.wds
rm inputfiles/multiPageText.abc

echo ------------------------------------------------

echo 
echo 
echo ------------------------------------------------
echo        Running Syntax testing test cases
echo ------------------------------------------------
echo 
echo 

echo Running S01
echo Rationale: Run with help parameter
echo Expected Output: Help screen for concordance

echo concordance help

echo ------------------------------------------------

echo Running S02
echo Rationale: Test with outputfile
echo Expected output: Correct output by page number

concordance inputfiles/multiPageText outputFile > /dev/null

cat outputFile.wds
rm outputFile.wds
rm outputFile.abc

echo ------------------------------------------------

echo Running S03
echo Rationale: Test -p mode
echo Expected output: Correct concordance by page

concordance -p inputfiles/multiPageText > /dev/null

cat inputfiles/multiPageText
rm inputfiles/multiPageText.wds
rm inputfiles/multipageText.abc

echo ------------------------------------------------

echo Running S04
echo Rationale: Test -s -q mode
echo Expected output: Reduced screen output, concordance correctly by stanza

concordance -sq inputfiles/stanzasText > /dev/null

cat inputfiles/stanzasText.wds
rm inputfiles/stanzasText.wds
rm inputfiles/stanzasText.abc

echo ------------------------------------------------

echo Running S05
echo Rationale: Test -ln mode
echo Expected output: Normal screen output, concordance by line starting with #2

concordance -ln:2 inputfiles/multiLineText

cat inputfiles/multiLineText.wds
rm inputfiles/multiLineText.wds
rm inputfiles/multiLineText.abc

echo ------------------------------------------------

echo Running S06
echo Rationale: test -lqn mode
echo Expected output: Reduced screen output, concordance by line starting with #2

concordance -lqn:2 inputfiles/multiLineText

cat inputfiles/multiLineText.wds
rm inputfiles/multiLineText.wds
rm inputfiles/multiLineText.abc

echo ----------------------------------------------

echo Running S08
echo Rationale: Test -p option without providing a filename
echo Expected output: An error message like "Please specify an input file"

concordance -p

echo ----------------------------------------------

echo Running S09
echo Rationale: Test -sq option without providing a filename
echo Expected output: An error message like "Please specify an input file"

concordance -sq

echo ----------------------------------------------

echo Running S10
echo Rationale: Test -lqn:2 option without providing a filename
echo Expected output: An error message like "Please specify an input file"

concordance -lqn:2

echo ----------------------------------------------

echo Running S12
echo Rationale: Invalid token for command line parameters
echo Expected output: An error message like "-x: Invalid parameter", no output files are generated

concordance -x inputfiles/multiLineText

cat inputfiles/multiLineText.wds
rm inputfiles/multiLineText.wds
rm inputfiles/multiLineText.abc

echo ----------------------------------------------

echo Running S13
echo Rationale: Invalid token after help
echo Expected output: Although an invalid token is provided, the output for the help function should still be the same

echo concordance help foo

echo ----------------------------------------------


echo Running S14
echo Rationale: Invalid token after a command line parameter
echo Expected output: Error message like "-s : invalid input filename", no output files are created

concordance -p -s

echo ----------------------------------------------

echo Running S15
echo Rationale: Invalid token after -sq
echo Expected output:  Error message like "-s : invalid input filename", no output files are created

concordance -sq -s

echo ----------------------------------------------

echo Running S 16
echo Rationale: Invalid token after -lqn
echo Expected output:  Error message like "-s : invalid input filename", no output files are created

concordance -lqn:2 -s

echo ----------------------------------------------

echo Running S17
echo Rationale: Invalid token after inputfilename
echo Expected output: Error message like "-s: invalid output filename", no outputfiles are created

concordance inputfiles/multiLineText -s

cat inputfiles/multiLineText.wds
rm inputfiles/multiLineText.wds
rm inputfiles/multiLineText.abc

echo ----------------------------------------------

echo Running S18
echo Rationale: Provide too many parameter
echo Expected output: Error "Too many parameters"

concordance inputfiles/multiLineText test -s

cat test.wds
rm test.wds
rm test.abc


