/*
# $Header: /home/ralph/progs/concordance/RCS/oshdr.h,v 0.2 1996/11/26 23:57:04 ralph Exp ralph $
# $Date: 1996/11/26 23:57:04 $
# $Author: ralph $
# $Log: oshdr.h,v $
# Revision 0.2  1996/11/26 23:57:04  ralph
# Cosmetic changes.
#
# Revision 0.1  1996/11/21 22:21:55  ralph
# Alpha version.  Initial writing.
#
*/

/*
* This file is a:
* Header to define the operating system for compilation:
* Comment out the operating system(s) NOT being compiled for.  Only 1 operating system
* should be #define-d!:
*/

#ifndef OS_HDR
#define OS_HDR

#define OS_UNIX

//DOS_FUNCS can be defined if OS_UNIX is defined if the program being ported wants to use
//DOS-type functions in the file concutils.cc.  If these functions are not needed, leave
//DOS_FUNCS commented out.
#ifdef OS_UNIX
//#define DOS_FUNCS
#endif


// #define OS_DOS

#endif

