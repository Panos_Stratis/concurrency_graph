/*
# $Header: /home/ralph/progs/concordance/RCS/standard.h,v 0.3 1996/11/26 23:57:24 ralph Exp ralph $
# $Date: 1996/11/26 23:57:24 $
# $Author: ralph $
# $Log: standard.h,v $
# Revision 0.3  1996/11/26 23:57:24  ralph
# Cosmetic changes.
#
# Revision 0.2  1996/11/23 00:50:57  ralph
# Added GNU public lkiicense.
#
# Revision 0.1  1996/11/10 22:08:02  ralph
# Alpha test version
#
# Revision 0.1  1996/05/21 23:07:39  ralph
# Initial revision
#
*/
/*************************************************************
*File: STANDARD.H - standard program header containing various general conventions
*
* Copyright (C) 11-13-93 Ralph L. Meyer - R & R Consulting
* 39 Nelson Avenue, Spotswood, NJ 08884
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* Bug reports and suggestions can be sent to:
* meyer@princeton.edu
* Ralph L. Meyer, 39 Nelson Ave. Spotswood, NJ 08884
*
Project #: .
Standard header file
**************************************************************/
#include "oshdr.h"

#ifndef _STANDARDHDR_
#define _STANDARDHDR_

#include <stdio.h>
#include <fstream.h>
#include <iostream.h>
#include <iomanip.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <ctype.h>

#ifdef DOS_FUNCS
#include <curses.h>
#endif

//colors for Ioblock windows, messages, etc:
#define PROMPTCOLOR WHITE        //Prompt messages at bottom of screen
#define ERRORCOLOR LIGHTRED      //error messages at bottom of screen
#define LABELCOLOR YELLOW        //entry window label colors
#define LABELBACKGROUND BLACK
#define WINDOWCOLOR LIGHTCYAN    //entry window colors
#define WINDOWBACKGROUND BLUE

#define REVCOLOR BLUE            //reversed menu window colors
#define REVBACKGROUND WHITE
#define BORDERCOLOR LIGHTRED     //menu window border colors
#define BORDERBACKGROUND BLUE
#define NORMALCOLOR  LIGHTGRAY
#define NORMALBACKGROUND BLACK

//key scancodes:
#define DNARROW 80   //keytype: 0
#define UPARROW 72   //    0
#define RTARROW 77   //    0
#define LTARROW 75   //    0
#define ENTER  13 //    1
#define CR  13 //    1
#define LF  10 //    1
#define DEL 83 //    0
#define BKSP    8 //    1
#define HOME   71 //    0
#define INSERT 82 //    0
#define END 79 //    0
#define ESC 27 //    1
#define F1KEY  59 //    0

//ASCII code(s)
#define FF  12 //formfeed

/* boolean flags */
typedef enum Boolean {FALSE=0,TRUE=1,FAIL=0,SUCCEED=1,OK=1,NO=0,YES=1,NOMSG=0,MSG=1,OFF=0,ON=1} BOOLEAN;

//typedef enum Boolean {FAIL=0,SUCCEED=1,OK=1,NO=0,YES=1,NOMSG=0,MSG=1,OFF=0,ON=1} BOOLEAN;

//calendar enumerations:
typedef enum {JAN,FEB,MAR,APR,MAY,JUN,JUL,AUG,SEP,OCT,NOV,DEC} MONTH;
typedef enum {SUN, MON, TUE, WED, THU, FRI, SAT, WEEK} DAY;

const int errlocx=5;   //x screen location of error message beginning
const int errlocy=25;  //y screen location of error message beginning

//enumeration of days of the year to the end of each month in the year
enum {
   JANBEG=  0,
   JANEND= 31,     //31
   FEBEND= 59,     //28 Note: leap years have 29!
   MAREND= 90,     //31
   APREND=120,     //30
   MAYEND=151,     //31
   JUNEND=181,     //30
   JULEND=212,     //31
   AUGEND=243,     //31
   SEPEND=273,     //30
   OCTEND=304,     //31
   NOVEND=334,     //30
   DECEND=365      //31
};                 //365

#endif
