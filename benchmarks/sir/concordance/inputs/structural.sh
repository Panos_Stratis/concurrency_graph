covclear
#  ===========================================================
#  Strutural test suite for Concordance
#  Version 1.21, 30/Oct/05
#  Author Florian Niedermann, fniederm@uwo.ca
#  Revision history:
#  1.0 Initial setup, provided valid test cases and invalid
#      test cases as "guessed" from concordance.cc
#      Coverage for concordance.cc at 87%
#  1.1 Included "-?" and -h parameter. 
#      Coverage for concordance.cc at 89%
#  1.2 Included --help parameter
#      Coverage for concordance.cc at 89%
#  1.3 Modified input files so that some would contain both
#      ff and lf characters
#      Coverage for concordance.cc at 90%
#  1.4 Added a testcase were the command line just included the 
#      parameters "inputfile" and "outputfile". Although this
#      should not work according to the specification it still was
#      possible to achieve some additional coverage by making sure
#      that "outputfile" was the name of a file which already existed
#      Coverage for concordance.cc at 91%
#  1.5 Added a larger inputfile for one test case (with more than
#      512 characters) to provide additional coverage in concordance.cc
#      Coverage for concordance.cc at 92%
#  1.6 Modified one input file to include an invalid stanza. Added stanza to 
#      multiLinePageText for some additional coverage in the parser
#      Coverage for concordance.cc at 96%
#  1.7 Stopped testing concordance.cc, since the remaining
#      conditions weren't feasible to cover.
#      Final coverage for cocordance.cc at 96%
#  1.8 Started testing of alphalst.cc
#      All non-tested conditions of alphalst.cc were found to be not feasible. 
#      Final coverage for alphalst.cc at 46%
#  1.9 Started testing of alphalst.h
#      According to covfn no functions in this header file were "called".
#      Therefore, providing coverage is not feasible
# 1.10 Started testing of concutils.cc
#      Coverage for concutils.cc is at 19%
# 1.11 Added some "weird" ASCII characters to one of the input files which
#      provided some substantial coverage in one of the switch statements
#      Coverage for concutils.cc is at 27%
# 1.12 Included an inputfile with an actual file-ending (.txt). This provided
#      some additional coverage.
#      Coverage for concutils.cc is at 28%
# 1.13 Stopped testing concutils.cc, since the remaining
#      conditions weren't feasible to cover.
#      Final coverage for concutils.cc at 28%
# 1.14 Started testing word.cc
#      All remaining conditions are found to be not feasible to cover
#      Final coverage for word.cc at 24%
# 1.15 Started testing word.h
#      According to covfn no functions in this header file were "called".
#      Therefore, providing coverage is not feasible
# 1.16 Ended testing for more coverage
#      Final coverage of the whole testsuite is at 48% or 215/446
# 1.17 Started minimizing test suite.
#      Initial number of test cases is 15
# 1.18 Removed ST09 since it is covered by ST08 and ST10
#      Coverage remains at 215/446
#      Number of test cases reduced to 14
# 1.19 Removed ST12 since it doesn't seem to provide additional coverage
#      Coverage remains at 215/446
#      Number of test cases reduced to 13
# 1.20 Removed ST07 since it is covered by ST08
#      Coverage remains at 215/446
#      Number of test cases reduced to 12
# 1.21 Ended minimizing the test suite
#      Final coverage is at 48% or 215/446
#      Final number of test cases is at 12
#  ===========================================================

echo ===========================================================
echo 1. Test "valid" cases
echo The following test cases all test "normal" flow in Concordance.
echo This means that they only use input files which conform to the
echo specification given by Concordance and do not try to force errors
echo by using invalid command line parameters etc.
echo Since no special output is expected from these test cases all 
echo screen output is piped to /dev/null
echo ===========================================================
echo ST01
echo Only one command line argument
concordance < /dev/null > /dev/null
echo ===========================================================
echo ST02
echo "-?" argument
concordance "-?" < /dev/null > /dev/null
echo ===========================================================
echo ST03
echo undocumented -h argument
concordance -h < /dev/null > /dev/null
echo ===========================================================
echo ST04
echo undocumented --help argument
concordance --help  < /dev/null > /dev/null
echo ===========================================================
echo ST05
echo Two command lines arguments. Default -p mode.Contains both 
echo formfeeds and linefeeds to provide some additional coverage.
concordance inputfiles/multiLinePageText < /dev/null > /dev/null
# cat inputfiles/multiLinePageText.wds
rm inputfiles/multiLinePageText.wds
rm inputfiles/multiLinePageText.abc
echo ===========================================================
echo ST06
echo Three command line arguments, -s mode. Use mcgee.txt to test a 
echo larger text than usual and to provide some additional coverage
echo by using a file ending delimited with .
concordance -s inputfiles/mcgee.txt < /dev/null > /dev/null
# cat inputfiles/mcgee.wds
rm inputfiles/mcgee.wds
rm inputfiles/mcgee.abc
echo  ===========================================================
# echo ST07
# echo Three command line arguments, -qn mode
# concordance -qn:5 inputfiles/multiLinePageText < /dev/null > /dev/null
# cat inputfiles/multiLinePageText.wds
# rm inputfiles/multiLinePageText.wds
# rm inputfiles/multiLinePageText.abc
# Note: Covered by ST08
# echo  ===========================================================
echo ST08
echo Four command line arguments, -pqn mode
concordance -pqn:2 inputfiles/multiLinePageText outputfile < /dev/null > /dev/null
# cat outputfile.wds
rm outputfile.wds
rm outputfile.abc
echo  ===========================================================
# echo ST09
# echo Four command line arguments, -lqn mode
# concordance -lqn:2 inputfiles/multiLinePageText outputfile < /dev/null > /dev/null
# cat outputfile.wds
# rm outputfile.wds
# rm outputfile.abc
# Note: The coverage created by this test case is redundant.
# ST08 and ST10 provide together provide together a superset of the coverage
# of ST09

echo ===========================================================
echo 2. Test invalid test cases
echo These test cases all contain one or several invalid elements.
echo This may refer to command line parameters, content of input
echo files or other environmental attributes.
echo If the screen output is of interest it is displayed.
echo The same is true for the resulting .wds file.
echo ===========================================================
echo ST10
echo Invalid param for -n
concordance -lqn:a inputfiles/multiLinePageText outputfile < /dev/null > /dev/null
cat outputfile.wds
rm outputfile.wds
rm outputfile.abc
echo  ===========================================================
echo ST11
echo Large number for - n. Should not lead to an error -
echo it is just reduced to the largest possible number
concordance -lqn:12345678912345678 inputfiles/multiLinePageText outputfile < /dev/null > /dev/null
cat outputfile.wds
rm outputfile.wds
rm outputfile.abc
# echo  ===========================================================
# echo ST12
# echo Non existing file for concordance
# concordance nonExistingFile < /dev/null > /dev/null
# Note: This test case doesn't seem to provide additional coverage
# and was removed
echo  ===========================================================
echo ST13
echo Invalid command line parameter
concordance -x inputfiles/multiLinePageText
cat inputfiles/multiLinePageText.wds
rm inputfiles/multiLinePageText.wds
rm inputfiles/multiLinePageText.abc
echo  =========================================================== 
echo ST14
echo Two parameters, no switches. For this to work, the file 
echo existingOutput  must already exist since Concordance will 
echo else crash at another location in the code
echo Test > existingOutput
concordance inputfiles/multiLinePageText existingOutput
cat existingOutput.wds
rm existingOutput.wds
rm existingOutput.abc
rm existingOutput
echo  ===========================================================
echo ST15
echo Test invalid stanza, where stanza is not a number. Additionally includes
echo weird ASCII characters to provide some additional coverage in concutils.cc
concordance -s inputfiles/invalidStanzasText
cat inputfiles/invalidStanzasText.wds
rm inputfiles/invalidStanzasText.wds
rm inputfiles/invalidStanzasText.abc
echo  ===========================================================
covfn 
covbr -u
