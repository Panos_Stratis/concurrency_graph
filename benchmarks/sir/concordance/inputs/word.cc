/*
# $Header: /home/ralph/progs/concordance/RCS/word.cc,v 0.4 1996/11/26 23:55:02 ralph Exp ralph $
# $Date: 1996/11/26 23:55:02 $
# $Author: ralph $
# $Log: word.cc,v $
# Revision 0.4  1996/11/26 23:55:02  ralph
# Changed loop ints to unsigned to get rid of warning messages that indicated
# a comparison was being made between unsigned and signed ints.
#
# Revision 0.3  1996/11/23 00:51:53  ralph
#  Added GNU public license.
#
# Revision 0.2  1996/11/21 22:22:52  ralph
# Error in WordList friend operator <<() that crashed at very start corrected.
#
# Revision 0.1  1996/11/10 22:09:23  ralph
# Alpha version test version
#
# Revision 0.1  1996/05/21 23:09:56  ralph
# Initial revision
#
* File: word.cc - used by Concordancing program
* contains wordlist classes' functions & friends
* Copyright (C) 12-16-93 Ralph L. Meyer - R & R Consulting
* 39 Nelson Avenue, Spotswood, NJ 08884
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* Bug reports and suggestions can be sent to:
* meyer@princeton.edu
* Ralph L. Meyer, 39 Nelson Ave. Spotswood, NJ 08884
**********************************************************************/

/*
NOTES:
Set class definitions up in headers after debugging.  Include forward
declarations as externs.  Put eof functions in utils file
check it out to see which it is.
*/

#include "word.h"

//forward declarations:

extern char *lowerCaseString(char *stringToLc);  //lower cases a null ended string
   //in concutls.cpp

//****   class Locus functions *********************************************************

Locus::Locus(const Locus &lc)       //X(const X&) constructor
{
   loc = lc.getLoc();
   nxt=this;   //temporarily point it at itself
}

Locus::Locus(unsigned int lc)
{
   loc = lc;
   nxt=this;   //temporarily point it at itself
}

void Locus::saveLoc(FILE *fp) //save locus to file
{
   fwrite(&loc,sizeof(unsigned int),1,fp);
}

void Locus::loadLoc(FILE *fp) //read a locus from file
{
   fread(&loc,sizeof(unsigned int),1,fp);
}

Locus &Locus::operator=(Locus &lc)
{
   loc = lc.getLoc();
   //nxt=this;     for the moment, leave nxt as it is
   return *this;
}

Locus &Locus::operator=(unsigned int lc)
{
   loc = lc;
   //nxt=this;     for the moment, leave nxt as it is
   return *this;
}

ostream &operator<<(ostream &os,Locus &lc)
{
   os << lc.loc;
   return os;
}

istream &operator>>(istream &is,Locus &lc)
{
   is >> lc.loc; //in;
   return is;
}

//***** end of class Locus  *********************************************************


//*****  class Word functions *********************************************************

Word::Word(char *wrd,unsigned int locus,unsigned int numUses)
   :useAmt(numUses), prv(0), nxt(0)
{
   word=new char[wordLen=(strlen(wrd)+1)];
   assert(word!=0);
   strcpy(word,lowerCaseString(wrd));  //enter a lower cased word
   tail = loci = new Locus(locus);
   assert(loci!=0);
}

Word::Word(const Word &wrd)
   :wordLen(wrd.getLen()),useAmt(wrd.getUse()),loci(0),tail(0),prv(0),nxt(0)
{
   word=new char[wordLen];
   assert(word!=0);
   strcpy(word,wrd.getWord());

   Locus *newLoc,*walker=wrd.getLoci(); //create a new locus list
   while (walker!=walker->nxt) {
      newLoc=new Locus;   //copy the list of locations
      assert(newLoc!=0);
      *newLoc=*walker;
      addLocus(newLoc);
      walker=walker->nxt;
   }
   newLoc=new Locus(*walker); //copy the last location
   assert(newLoc!=0);
   addLocus(newLoc);
}

Word::~Word(void)
{
   if (word) delete [] word;
   delLoci();
}

//add a locus to the locus list:
void Word::addLocus(Locus *lcp) //add a locus to the word
{
   if (!loci){ //if no list yet (NULL loci = no list)
      loci=tail=lcp;   //locus is head of list
      return;
   }

   Locus *walker=loci; //pointer to walk through list - was at top
   while (walker!=walker->nxt) { //go to the end of the list
      if (walker->getLoc()==lcp->getLoc()){
         delete lcp; //unneeded - location already present
         return;  //if locus is already recorded, don't record;
      }
      walker=walker->nxt;
   }
   //add new locus at the end of the list
   if (walker->getLoc()==lcp->getLoc()){ //check last member of list if same loc...
      delete lcp; //unneeded - location already present
      return;  //if locus is already recorded, don't record;
   }
   walker->nxt=lcp;

/*formerly:
//adds loci even if repeating a locus:
   //add new locus at the end of the list
   else tail->nxt=lcp;
   tail=lcp;
*/
}

//removes locus list
void Word::delLoci(void)
{
   if (loci){
      Locus *delLocus,*walker=loci;

      while (walker!=walker->nxt) {   //delete the list of loci
         delLocus=walker;
         walker=walker->nxt;
         delete delLocus;
      }
      delete walker; //remove the last member of the list
      tail=loci=0;  //indicate empty list
   }
}

//copies a char string into Word and sets its use to 1
//does NOT set locus!
Word &Word::operator=(char *wrd)
{
   if (word)delete [] word;
   word=new char[wordLen=(strlen(wrd)+1)];
   assert(word!=0);
   strcpy(word,lowerCaseString(wrd));
   useAmt=1;
   if (loci)delLoci();
   tail=loci=0;  //no loci!!
   return *this;
}

//note that the use of this function requires
//adding the word to a list in order to set prv and nxt!
Word &Word::operator=(Word &wrd)  //set a word to another word
{
   wordLen=wrd.getLen();
   if (word)delete [] word;
   word=new char[wordLen];
   assert(word!=0);
   strcpy(word,wrd.getWord());
   useAmt=wrd.getUse();
   if (loci)delLoci();
   Locus *newloc,*prv=0,*walker=wrd.getLoci(); //write the location list
   if (walker){
      for (unsigned int n=0;n<useAmt&&prv!=walker;n++){
         prv=walker;
         newloc=new Locus(walker->getLoc());
         addLocus(newloc);
         walker=walker->getNxt();
      }
   }

   prv=0;
   nxt=0;
   return *this;
}

void Word::incWord(unsigned int locus)  //increment the number of times a word is used
{                               //and add a new location to the list
   if (word){
      useAmt++;
      Locus *newLoc=new Locus(locus);
      assert(newLoc!=0);
      addLocus(newLoc);
   }
   else{
      puts("ERROR: Cannot increment usage of word not in concordance.");
   }
}

void Word::saveWord(FILE *fp)   //save a word to disk
{
   if(!wordLen||!word)return; //on an empty word, don't write
   fwrite(&wordLen,sizeof(wordLen),1,fp); //write the length
   fwrite(word,wordLen,1,fp); //write the word to the stream
   fwrite(&useAmt,sizeof(useAmt),1,fp);   //write the usage (also # of locs)

   if(!loci){  //if there is no locus list, write a unsigned int 0 for the list & quit
      unsigned int substitute=0;
      fwrite(&substitute,sizeof(unsigned int),1,fp);
      return;
   }
   Locus *prv=0,*walker=loci; //write the location list
   if (walker){
      for (unsigned int n=0;n<useAmt&&prv!=walker;n++){
         prv=walker;
         fwrite(walker->getLocAdd(),sizeof(unsigned int),1,fp);
         walker=walker->getNxt();
      }
   }
}

Word *Word::loadWord(FILE *fp)   //load a word from disk
{
   fread(&wordLen,sizeof(wordLen),1,fp); //read the length
   word = new char[wordLen];
   assert (word!=0);
   fread(word,wordLen,1,fp); //read the word to the stream
   fread(&useAmt,sizeof(useAmt),1,fp);   //read the usage (also # of locs)

   Locus *inloc;  //read in the location list
   unsigned int lloc;
   for (unsigned int n=0;n<useAmt;n++){
      fread(&lloc,sizeof(lloc),1,fp);
      inloc=new Locus(lloc);
      addLocus(inloc);
   }
   return this;
}

//note that this function is non-functional for reading
//information in from a file as saved by operator << !!!
istream &operator>>(istream &is,Word &wrd)
{
   if (wrd.word)delete [] wrd.word;  //delete the word in the former data
   if(wrd.loci)wrd.delLoci();        //delete the loci in the former data

   is >> wrd.wordLen;
   wrd.word = new char[wrd.wordLen];
   assert(wrd.word!=0);

   is >> wrd.word >> wrd.useAmt;

   Locus *inloc;
   unsigned int lloc;
   for(unsigned int n=0;n<wrd.useAmt;n++){
      is >> lloc;
      inloc=new Locus(lloc);
      assert(inloc!=0);
      wrd.addLocus(inloc);
   }
   return is;
}

//added spaces and endls to set up for read in - check if it works DEBUG
ostream &operator<<(ostream &os,Word &wrd)
{
   if(wrd.word){    //output nothing if the word is empty
      os.width(2);
      os << wrd.wordLen-1;
      os << ' ';
      os.width(15);
      os.setf(ios::left);
      os.fill('.');
      os << wrd.word;
      os.width(6);
      os.setf(ios::right);
      os.fill('.');
      os << wrd.useAmt;
      os.fill(' ');
      os << " - ";

      Locus *walker=wrd.loci;
      if(!wrd.loci){ //if there is no locus list, write a unsigned int 0 for the list & quit
         unsigned int substitute=0;
         os << substitute << endl;
         return os;
      }
      else{
         while (walker!=walker->getNxt()){
            os << walker->getLoc() << " ";
            walker=walker->getNxt();
         }
         os << walker->getLoc() << endl;  //put tail on stream
      }
   }
   return os;
}

void Word::printWord(void)
{
   cout << word << ", # of uses: "<< useAmt << ", and locations";
   Locus *walker=loci;
   if(loci){
      while (walker!=walker->getNxt()){
         cout << ", " << walker->getLoc();
         walker=walker->getNxt();
      }
      cout << ", " << walker->getLoc();  //put tail on screen
   }
   cout << endl;
}

//***** end of class Word *********************************************************

//*****  class WordList functions *********************************************************

SORTORDER WordList::sortOrder=ascending;

WordList::WordList(Word *wrd) //wordlist receiving a word as pointer
   :numWords(1)
{
   current = head = tail = new Word;
   assert(head!=0);
   *head = *wrd; //put word into head of list
   head->nxt=head->prv=head;  //point word at itself indicating end of list
}

WordList::WordList(Word &wrd) //wordlist receiving a word as ref
   :numWords(1)
{
   current = head = tail = new Word;
   assert(head!=0);
   *head = wrd;
   head->nxt=head->prv=head;
}

WordList::WordList(char *wrd,unsigned int loc) //wordlist receiving a word as char string
   :numWords(1)
{
   current = head = tail = new Word(wrd,loc);
   assert(head!=0);
   head->nxt=head->prv=head;
}

WordList::WordList(const WordList &wl) //copy constructor
   :numWords(wl.getNumWords())
{
   Word *newWord,*walker = wl.head;
   if (walker){
      while (walker!=walker->nxt) {
         newWord = new Word(*walker);
         assert (newWord!=0);
         addWord(newWord);
         walker=walker->nxt;
      }
      newWord = new Word(*walker); //get last member of wl
      assert (newWord!=0);
      addWord(newWord);
   }
}

WordList::~WordList(void)
{
   Word *walker=head,*delWord;

   while (walker!=walker->nxt) {
      delWord=walker;
      walker=walker->nxt;
      delete delWord;
   }
   delete walker; //delete the last one of the list
}

void WordList::delList(void)
{
   if (!head)return;
   current=head;
   Word *delWord;
   while ((delWord=iterf())!=delWord->nxt) {
      delete delWord;
   }
   delete delWord;
   head=tail=current=0;
   numWords=0;
}

//iterates wordlist.current forward.  Always return present current!
Word *WordList::iterf(void) //iterators
{
   Word *holder=current;
   current=current->nxt;
   return holder;
}

//iterates wordlist.current backward.  Always returns present current!
Word *WordList::iterb(void)
{
   Word *holder=current;
   current=current->prv;
   return holder;
}

//adds a word to the list in ascending alphabetic order (at the moment)
int WordList::addWord(char *wrd,unsigned int locus)  //add a word to the list
{
   Word *newWord = new Word(wrd,locus);
   assert (newWord!=0);
   if (!head){
      current = tail = head = new Word(wrd,locus);
      assert (head!=0);
      head->nxt=head->prv=head;
      numWords++;
      return OK;
   }
   else{
      Word *walker=head;
      while (walker!=walker->nxt) {
         if (strcasecmp(wrd,walker->getWord())==0){//if same word
            walker->incWord(locus); //add the new location to the word in the list
            delete newWord;   //delete the new word - it is unneeded
            return OK;
         }
         else if(strcasecmp(wrd,walker->getWord())<0){//if new word < word in list
            if(walker==head){   //make newWord the new head
               walker->prv=newWord;
               newWord->prv=newWord;
               newWord->nxt=walker;
               head=newWord;
               numWords++;
               return OK;
            }
            else{
               newWord->prv=walker->prv;
               newWord->nxt=walker;
               walker->prv->nxt=newWord;
               walker->prv=newWord;
               numWords++;
               return OK;
            }
         }
         //else if(strcasecmp(wrd,walker->getWord())>0&&strcasecmp(wrd,walker->nxt->getWord())<0){   //wrd > walker & < walker->nxt 15.004 sec 21% of time!!**
         else if(strcasecmp(wrd,walker->getWord())>0&&strcasecmp(wrd,walker->nxt->getWord())<0){   //wrd > walker & < walker->nxt
            newWord->nxt=walker->nxt;
            walker->nxt->prv=newWord;
            newWord->prv=walker;
            walker->nxt=newWord;
            numWords++;
            return OK;
         }
         else walker = walker->nxt;
      }
      //if the word is the same as the word at tail, increment the word at tail
      if (strcasecmp(wrd,walker->getWord())==0){//if same word
         walker->incWord(locus); //add the new location to the word in the list
         return OK;
      }
      else if(strcasecmp(wrd,walker->getWord())<0){//if new word < word in list
         if(walker==head){   //make newWord the new head
            walker->prv=newWord;
            newWord->prv=newWord;
            newWord->nxt=walker;
            head=newWord;
            numWords++;
            return OK;
         }
         else{
            newWord->prv=walker->prv;
            newWord->nxt=walker;
            walker->prv->nxt=newWord;
            walker->prv=newWord;
            numWords++;
            return OK;
         }
      }
      //then at end of list: add new word to end
      else{
         tail->nxt=newWord;
         newWord->prv=tail;
         newWord->nxt=newWord;
         tail=newWord;
         numWords++;
         return OK;
      }
   }
}

int WordList::addWord(Word *wrd)
{
   Word *newWord=new Word(*wrd);
   assert(newWord!=0);
   if (!head){
      current = tail = head = new Word(*wrd);
      assert(head!=0);
      head->nxt=head->prv=head;
      numWords++;
      return OK;
   }
   else{
      Word *walker=head;
      while (walker!=walker->nxt) {
         if (strcasecmp(wrd->getWord(),walker->getWord())==0){//if same word
            walker->incWord(); //add the new location to the word in the list
            return OK;
         }
         else if(strcasecmp(wrd->getWord(),walker->getWord())<0){  //wrd < walker
            if(walker==head){   //make newWord the new head
               walker->prv=newWord;
               newWord->prv=newWord;
               newWord->nxt=walker;
               head=newWord;
               numWords++;
               return OK;
            }
            else{
               newWord->prv=walker->prv;
               newWord->nxt=walker;
               walker->prv->nxt=newWord;
               walker->prv=newWord;
               numWords++;
               return OK;
            }
         }
         else if(strcasecmp(wrd->getWord(),walker->getWord())>0&&strcasecmp(wrd->getWord(),walker->nxt->getWord())<0){   //wrd > walker & < walker->nxt
            newWord->nxt=walker->nxt;
            walker->nxt->prv=newWord;
            newWord->prv=walker;
            walker->nxt=newWord;
            numWords++;
            return OK;
         }
         else walker = walker->nxt;
      }
      if (strcasecmp(wrd->getWord(),walker->getWord())==0){//if same word
         walker->incWord(); //add the new location to the word in the list
         return OK;
      }
      else if(strcasecmp(wrd->getWord(),walker->getWord())<0){//if new word < word in list
         if(walker==head){   //make newWord the new head
            walker->prv=newWord;
            newWord->prv=newWord;
            newWord->nxt=walker;
            head=newWord;
            numWords++;
            return OK;
         }
         else{
            newWord->prv=walker->prv;
            newWord->nxt=walker;
            walker->prv->nxt=newWord;
            walker->prv=newWord;
            numWords++;
            return OK;
         }
      }
      //then at end of list without match: so, add new word to end:
      else{
         tail->nxt=newWord;
         newWord->prv=tail;
         newWord->nxt=newWord;
         tail=newWord;
         numWords++;
         return OK;
     }
   }
}


void WordList::delWord(char *wrd)   //find a word and remove it from the list
{
   Word *wordToDel; //word to be deleted

   if ((wordToDel=this->findWord(wrd))!=0){
      if (wordToDel==head&&head==head->nxt){ //at head & no list
         head=tail=current=0; //NULL all pointers
         numWords=0;
         delete wordToDel;
      }
      else if(wordToDel==head){
         head=head->nxt;          //set next record as head
         head->nxt->prv=head->nxt;  //and point it at itself
         numWords--;
         delete wordToDel;
      }
      else if (wordToDel==tail){
         wordToDel->prv->nxt=wordToDel->prv; //point previous Word at itself as tail
         tail=wordToDel->prv; //make previous Word tail
         current = tail;
         numWords--;
         delete wordToDel;
      }
      else{
         wordToDel->prv->nxt=wordToDel->nxt;
         wordToDel->nxt->prv=wordToDel->prv;
         numWords--;
         delete wordToDel;
      }
   }
}

Word *WordList::findWord(char *wrd) //find a word, given a char string
{
   current = head;
   Word *walker;
   while ((walker=iterf())!=walker->nxt) {
      if (strcasecmp(walker->getWord(),wrd)==0)
         return walker;
   }
   if (strcasecmp(walker->getWord(),wrd)==0)  //check last record
      return walker;
   else return 0; //NULL returned on not found
}

Word *WordList::findWord(Word *wrd) //find a word, given a Word.
{
   current = head;
   Word *walker;
   while ((walker=iterf())!=walker->nxt) {
      if (strcasecmp(walker->getWord(),wrd->getWord())==0)
         return walker;
   }
   if (strcasecmp(walker->getWord(),wrd->getWord())==0)
      return walker;
   else return 0; //NULL returned on not found
}

void WordList::saveWordList(FILE *fp)  //save a list to disk
{
   Word *svWord;
   if (!head) return;
   else{
      fwrite(&numWords,sizeof(long),1,fp);
      current=head;
      while ((svWord=iterf())!=svWord->nxt) {
         svWord->saveWord(fp);
      }
   }  svWord->saveWord(fp);
}

void WordList::loadWordList(FILE *fp)  //load a list from disk
{
   long numWordsToLoad;
   fread(&numWordsToLoad,sizeof(long),1,fp);
   if (numWordsToLoad>0){
      for (int ct=0;ct<numWordsToLoad;ct++){
         Word *inWord  = new Word;
         assert(inWord!=0);
         addWord(inWord->loadWord(fp));
      }
   }
}

WordList &WordList::operator=(WordList &wl)  //copy a list into another list
{
   delList();
   Word *newWord,*walker = wl.head;
   if (walker){
      while (walker!=walker->nxt) {
         newWord = new Word(*walker);
         assert (newWord!=0);
         addWord(newWord);
         walker=walker->nxt;
      }
      newWord = new Word(*walker); //get last member of wl
      assert (newWord!=0);
      addWord(newWord);
      numWords=wl.getNumWords();
   }
   return *this;
}

/*istream &operator>>(istream &is,WordList &wl) //input a WordList (???)
{
} */

ostream &operator<<(ostream &os,WordList &wl) //output a WordList stream
{
   if (wl.head){
      wl.setCurrentToHead();
      Word *walker=wl.iterf();
      while (walker!=walker->getNxt()) {
         os << *walker;
         walker=wl.iterf();
      }
      os << *walker;
   }
   return os;
}

//***** end of class WordList *********************************************************

