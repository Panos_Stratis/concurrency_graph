/*
# $Header: /home/ralph/progs/concordance/RCS/word.h,v 0.3 1996/11/23 00:51:37 ralph Exp ralph $
# $Date: 1996/11/23 00:51:37 $
# $Author: ralph $
# $Log: word.h,v $
# Revision 0.3  1996/11/23 00:51:37  ralph
# Added GNU public license.
#
# Revision 0.2  1996/11/21 22:31:13  ralph
# Small cosmetic changes made to file.
#
# Revision 0.1  1996/11/10 22:10:07  ralph
# Alpha test version.
#
# Revision 0.1  1996/05/21 23:10:37  ralph
# Initial revision
#
* File: word.h - header file containing definitions of word and word list
*  classes for concordancer program.
* Copyright (C) 12-16-93 Ralph L. Meyer - R & R Consulting
* 39 Nelson Avenue, Spotswood, NJ 08884
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* Bug reports and suggestions can be sent to:
* meyer@princeton.edu
* Ralph L. Meyer, 39 Nelson Ave. Spotswood, NJ 08884
*
NOTES:
Use the save and load functions except for final output, as these functions
   save the information in the amount and location lists as numerical values.
   The iostream class means of saving files, using the operators >> and << save
   the information as ascii numbers rather than the numerical values.  These
   are good for outprints, but not value saving.
*/

#ifndef _WORDLIST_
#define _WORDLIST_

#include "standard.h"

//forward declarations:
class WordList;
class Word;

enum SORTORDER {descending,ascending};

//-----  class Locus --------------------------------------------------------

//this class is used in the Word class and keeps track of line
//locations of words in a document:

class Locus{
   unsigned int loc;   //unsigned int containing a line location
   Locus *nxt; //pointer to the next Locus-singly linked list

   Locus(void):loc(0),nxt(this){}   //default constructor - point obj at itself
   Locus(const Locus &lc);       //X(const X&) constructor
   Locus(unsigned int lc);               //constructor receiving a line number
   Locus &operator=(Locus &lc);
   Locus &operator=(unsigned int lc);
   unsigned int getLoc(void) const {return loc;} //return a line number
   void setLoc(unsigned int lc){loc = lc;} //set the location
   unsigned int *getLocAdd(void) {return &loc;} //return line number's address
   void setNxt(Locus *lp){nxt=lp;}  //set the nxt pointer
   Locus *getNxt(void){return nxt;} //return the pointer to the next locus
   Locus *getThis(void){return this;} //return the pointer to this locus
   void saveLoc(FILE *fp); //save locus to file
   void loadLoc(FILE *fp); //read a locus from file
   friend ostream &operator<<(ostream &os,Locus &lc);
   friend istream &operator>>(istream &is,Locus &lc);
   friend istream &operator>>(istream &is,Word &wrd);
   friend ostream &operator<<(ostream &os,Word &wrd);
   friend class Word;   //Word class manipulates the list of loci
};

//-----  class Word ------------------------------------------------------

//this class keeps track of a word, the amount of its usage
//and its locations in a file It is used only by WordList class
class Word{
   char *word; //an individual concordance word entry
   int wordLen; //length of the word
   unsigned int useAmt;  //the number of times the word was used in a file
   Locus *loci;   //pointer to head of a singly linked list of loci of words
   Locus *tail;   //end of locus list.  Add loci here
      //could make this more efficient by tracking the tail as well as head of list and
      //coding just to add new loci to tail of list rather than walking to tail
   Word *prv;  //address of previous word in a list
   Word *nxt;  //address of next word in a list

   void addLocus(Locus *lcp); //add a locus to the word
   void delLoci(void); //delete the locus list
   Word(void):word(0),wordLen(0),useAmt(0),loci(0),tail(0),prv(0),nxt(0){} //default constructor
   Word(char *wrd,unsigned int lc,unsigned int numUses=1);  //args: the word, the line number, and the number of times used
   Word(const Word &wrd);
   ~Word(void);
   char *getWord(void) const {return word;}
   int  getLen(void) const {return wordLen;}
   unsigned int getUse(void) const {return useAmt;}
   Word *getPrv(void) const {return prv;}
   Word *getNxt(void) const {return nxt;}
   Locus *getLoci(void) const { return loci; }
   Locus *getTail(void) const { return tail; }
   void printWord(void);  //put word on screen in formatted manner
   Word &operator=(Word &wrd);  //set a word to another word
   Word &operator=(char *wrd);  //set a word to an instance of 1 with no locus (for use with word counting only!)
   void incWord(unsigned int locus);  //increment the number of times a word is used and add the locus of the word
   void incWord(void){incWord(0);} //increment a word without adding a locus by making the locus 0.(wordcounting only!)
   void saveWord(FILE *fp);   //save a word to disk
   Word *loadWord(FILE *fp);   //load a word from disk
   friend istream &operator>>(istream &is,Word &wrd);
   friend ostream &operator<<(ostream &os,Word &wrd);
   friend ostream &operator<<(ostream &os,WordList &wl); //output a WordList stream
   friend class WordList;
};

//-----  class WordList  -------------------------------------------------------

//This is an alphabetizing doubly linked list that adds words
//in their proper alphabetic locations using standard ascii sorting
//NOTE: this linked list is set up to indicate its ends by having:
//      head->prv point to head, and tail->nxt point to tail. (No Null Pointers indicating ends!)
class WordList{
   static SORTORDER sortOrder; /* 12-21-93 08:13:04.73 presently only a place-holder */
   Word *head; //head of doubly linked list
   Word *tail; //tail of doubly linked list
   Word *current; //current iterator location
   long numWords; //number of words in list

public:
   WordList(void):head(0),tail(0),current(0),numWords(0){} //default constructor
   WordList(Word *wrd); //wordlist receiving a word as pointer
   WordList(Word &wrd); //wordlist receiving a word as ref
   WordList(char *wrd,unsigned int loc=0); //wordlist receiving a word as char string
   WordList(const WordList &wl); //copy constructor
   ~WordList(void);
   void delList(void);
   long getNumWords(void) const {return numWords;}
   Word *getHead(void){return head;}
   void setCurrentToHead(void){current=head;}
   Word *iterf(void); //iterators
   Word *iterb(void);
   int addWord(char *wrd,unsigned int locus=0);
   int addWord(Word *wrd);   //add a word to the list
   void delWord(char *wrd);   //find a word and remove it from the list
   Word *findWord(char *wrd); //find a word, given a char string
   Word *findWord(Word *wrd); //find a word, given a Word.
   void saveWordList(FILE *fp);  //save a list to disk
   void loadWordList(FILE *fp);  //load a list from disk
   WordList &operator=(WordList &wl);  //copy a list into another list
// friend istream &operator>>(istream &is,WordList &wl); //input a WordList (???)
   friend ostream &operator<<(ostream &os,WordList &wl); //output a WordList stream
};

#endif
