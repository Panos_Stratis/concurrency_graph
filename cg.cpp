#include <iostream>
#include <unordered_map>
#include <vector>
#include <algorithm>
#include <set>
#include <fstream>
#include <stack>
#include <string>
#include <sstream>
#include <stdlib.h>

#include <clang/AST/StmtVisitor.h>
#include "clang/AST/ASTContext.h"
#include "clang/Basic/LangOptions.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/Stmt.h"
#include "clang/AST/Decl.h"
#include "clang/AST/Expr.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "clang/Tooling/Tooling.h"
#include "clang/Analysis/CFG.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Driver/Options.h"
#include "clang/Analysis/AnalysisContext.h"
#include "clang/Analysis/Analyses/CFGReachabilityAnalysis.h"
#include "clang/Basic/SourceLocation.h"
#include "clang/AST/DeclCXX.h"
#include "clang/AST/Type.h"
#include "clang/AST/DeclFriend.h"
#include "clang/AST/DeclBase.h"

#include "llvm/Support/raw_os_ostream.h"
#include "llvm/Support/Allocator.h"
#include "llvm/Support/Format.h"
#include "llvm/Support/GraphWriter.h"
#include "llvm/Support/SaveAndRestore.h"

using namespace std;
using namespace clang;
using namespace clang::driver;
using namespace clang::tooling;
using namespace llvm;

class classNode;
class methodNode;
class functionNode;
class classMutex;
class procedureAnalyzer;

typedef unordered_map<string, classNode*> classHash;
typedef unordered_map<string, methodNode*> methodHash;
typedef unordered_map<string, functionNode*> functionHash;
typedef unordered_map<string, classMutex*> mutexHash;
typedef unordered_map<CFGBlock *, string> CFGBlockID;
typedef unordered_map<classNode*,  CXXBaseSpecifier*> inheritanceInfo;
typedef unordered_map<CFGBlock*, int> CFGBlock2Position;
typedef unordered_map<CFGBlock*, CFGBlock*> CFGBlockIndex;
typedef unordered_map<CFGBlock*,vector<CFGBlock*> > CFGBlockVector;
typedef unordered_map<CFGBlock*,vector<string> > CFGBlockVectorLabel;
typedef unordered_map<string, vector<CFGBlock*> > CFGBlockHash;

#define M_PUBLIC 0
#define M_PROTECTED 1
#define M_PRIVATE 2

#define FUNCTION_THREAD 1
#define METHOD_THREAD 2
#define LAMBDA_THREAD 3

class functionNode;
class procedureAnalyser;

class MemberExprVisitor : public RecursiveASTVisitor<MemberExprVisitor> {
public:
  explicit MemberExprVisitor() {}

  bool VisitMemberExpr(MemberExpr *mrex){
    vec.insert(vec.end(),mrex);
    return true;
  }

  vector<MemberExpr *> *getMemberExprs(){
    return &vec;
  }

  void emptyVector(){
    vec.clear();
  }

protected:
  vector<MemberExpr *> vec;
};


class DeclRefVisitor : public RecursiveASTVisitor<DeclRefVisitor> {
public:
  explicit DeclRefVisitor() {}

  bool VisitDeclRefExpr(DeclRefExpr *drex){
    vec.insert(vec.end(),drex);
    return true;
  }

  vector<DeclRefExpr *> *getDeclRefs(){
    return &vec;
  }

  void emptyVector(){
    vec.clear();
  }

protected:
  vector<DeclRefExpr *> vec;
};

class LambdaExprVisitor : public RecursiveASTVisitor<LambdaExprVisitor> {
public:
  explicit LambdaExprVisitor() {}

  bool VisitLambdaExpr(LambdaExpr * lamEx){
    vec.insert(vec.end(),lamEx);
    return true;
  }

  vector<LambdaExpr *> *getLambdaExprs(){
    return &vec;
  }

  void emptyVector(){
    vec.clear();
  }

protected:
  vector<LambdaExpr *> vec;
};

class toolBox{
public:
  
  template<class T, class T2>
  static void insertPair(T *key, T2 *element, unordered_map<T*,T2*> *hashTable){
    pair<T *, T2 *> hash (key, element);
    hashTable->insert(hash);
  }


  template<class T, class T2>
  static void expandVectorMap(T *key, T2 element, unordered_map<T*,vector<T2> > *hashTable){
    if(hashTable->count(key) > 0){
      hashTable->at(key).insert(hashTable->at(key).end(), element);
    }
    else{
      vector<T2> vec;
      vec.insert(vec.end(), element);
      pair<T *,vector<T2> > hash (key ,vec);
      hashTable->insert(hash);
    }
  }


  template<class T, class T2>
  static void expandVectorMap(T *key, T2 *element, unordered_map<T*,vector<T2*> > *hashTable){
      if(hashTable->count(key) > 0){
	hashTable->at(key).insert(hashTable->at(key).end(), element);
      }
      else{
	vector<T2*> vec;
	vec.insert(vec.end(), element);
	pair<T *,vector<T2*> > hash (key ,vec);
	hashTable->insert(hash);
      }
    }


  template<class T, class T2>
  static void expandVectorMap(T key, T2 *element, unordered_map<T,vector<T2*> > *hashTable){
    if(hashTable->count(key) > 0){
      hashTable->at(key).insert(hashTable->at(key).end(), element);
    }
    else{
      vector<T2*> vec;
      vec.insert(vec.end(), element);
      pair<T ,vector<T2*> > hash (key ,vec);
      hashTable->insert(hash);
    }
  }

  static vector<DeclRefExpr *> *findDeclRefExpr(Expr *ex){
    if(!DeclRefVisitorInit){
      drv = new DeclRefVisitor();
      DeclRefVisitorInit = true;
    }
    else
      drv->emptyVector();
      
    drv->TraverseStmt(ex);
    return drv->getDeclRefs();
  }


  static vector<LambdaExpr *> *findLambdaExpr(Expr *ex){
    if(!LambdaExprVisitorInit){
      lev = new LambdaExprVisitor();
      LambdaExprVisitorInit = true;
    }
    else
      lev->emptyVector();
      
    lev->TraverseStmt(ex);
    return lev->getLambdaExprs();
  }

  static vector<MemberExpr *> *findMemberExpr(Expr *ex){
    if(!MemberExprVisitorInit){
      mev = new MemberExprVisitor();
      MemberExprVisitorInit = true;
    }
    else
      mev->emptyVector();
      
    mev->TraverseStmt(ex);
    return mev->getMemberExprs();
  }

  static bool isThreadConstructor(string constructorName){
    if(constructorName == "std::thread::thread")
      return true;
    return false;
  }

  static bool isJoinOp(string methodName){
    if(methodName == "std::thread::join")
      return true;
    return false;
  }


  static bool isDetachOp(string methodName){
    if(methodName == "std::thread::detach")
      return true;
    return false;
  }
  

  static bool isMutex(string type){
    std::size_t found1 = type.find("std::mutex");
    std::size_t found2 = type.find("std::recursive_mutex");
    std::size_t found3 = type.find("std::timed_mutex");
    std::size_t found4 = type.find("std::recursive_timed_mutex");

    if((found1 != std::string::npos) || (found2 != std::string::npos) || (found3 != std::string::npos) || (found4 != std::string::npos))
      return true;
    return false;
  }

  static bool isMutexLockOp(string methodName){
    if(methodName == "std::mutex::lock")
      return true;
    if(methodName == "std::mutex::try_lock")
      return true;
    if(methodName == "std::recursive_mutex::lock")
      return true;
    if(methodName == "std::recursive_mutex::try_lock")
      return true;
    if(methodName == "std::timed_mutex::lock")
      return true;
    if(methodName == "std::timed_mutex::try_lock")
      return true;
    if(methodName == "std::timed_mutex::try_lock_for")
      return true;
    if(methodName == "std::timed_mutex::try_lock_until")
      return true;
    if(methodName == "std::recursive_timed_mutex::lock")
      return true;
    if(methodName == "std::recursive_timed_mutex::try_lock")
      return true;
    if(methodName == "std::recursive_timed_mutex::try_lock_for")
      return true;
    if(methodName == "std::recursive_timed_mutex::try_lock_until")
      return true;
    return false;
  }

  static bool isMutexUnlockOp(string methodName){
    if(methodName == "std::mutex::unlock")
      return true;
    if(methodName == "std::recursive_mutex::unlock")
      return true;
    if(methodName == "std::timed_mutex::unlock")
      return true;
    if(methodName == "std::recursive_timed_mutex::unlock")
      return true;
    return false;
  }

  static bool isAtomic(string type){
    std::size_t found = type.find("atomic");

    if(found != std::string::npos)
      return true;
    return false;
  }

  static bool isAtomicOp(string methodName){
    std::size_t found1 = methodName.find("std::atomic");
    std::size_t found2 = methodName.find("std::__atomic");

    if((found1 != std::string::npos) || (found2 != std::string::npos))
      return true;
    return false;
  }

  static bool isReferenceWrapper(string type){
    std::size_t found = type.find("std::reference_wrapper");

    if(found != std::string::npos)
      return true;
    return false;
  }

  static Expr *getRefArgument(Expr *ex){
    MaterializeTemporaryExpr *mte;
    CallExpr *callEx;

    mte = (MaterializeTemporaryExpr *) ex;
    ex = mte->GetTemporaryExpr();
    callEx = (CallExpr *) ex;
    return callEx->getArg(0);
  }

  static bool isBindWrapper(string type){
    std::size_t found = type.find("std::_Bind");

    if(found != std::string::npos)
      return true;
    return false;
  }


protected:
  static bool DeclRefVisitorInit;
  static DeclRefVisitor *drv;
  static bool LambdaExprVisitorInit;
  static LambdaExprVisitor *lev;
  static bool MemberExprVisitorInit;
  static MemberExprVisitor *mev;
};
bool toolBox::DeclRefVisitorInit = false;
bool toolBox::LambdaExprVisitorInit = false;
bool toolBox::MemberExprVisitorInit = false;
DeclRefVisitor* toolBox::drv = NULL;
LambdaExprVisitor* toolBox::lev = NULL;
MemberExprVisitor* toolBox::mev = NULL;

class objectVar{
public:

  objectVar(FieldDecl *decl, DeclRefExpr *obj, CFGBlock *block) : decl(decl), obj(obj), block(block) {}

  FieldDecl *getFieldDecl(){
    return decl;
  }

  DeclRefExpr *getObjectDecl(){
    return obj;
  }

  CFGBlock *getCFGBlock(){
    return block;
  }

protected:
  FieldDecl *decl;
  DeclRefExpr *obj;
  CFGBlock *block;
};

class aloneVar{
public:
  aloneVar(VarDecl *decl, CFGBlock *block) : decl(decl), block(block) {}

  VarDecl *getVarDecl(){
    return decl;
  }

  CFGBlock *getCFGBlock(){
    return block;
  }

protected:
  VarDecl *decl;
  CFGBlock *block;
};

class threadLaunch{
public:
  void setThreadVariable(VarDecl *vd){
    threadVariable = vd;
  }

  VarDecl *getThreadVariable(){
    return threadVariable;
  }

  CFGBlock *getThreadLauncher(){
    return launcher;
  }

  void insertThreadDetach(CFGBlock *block){
    detaches.insert(detaches.end(), block);
  }

  vector<CFGBlock *> *getThreadDetaches(){
    return &detaches;
  }

  void insertThreadJoin(CFGBlock *block){
    joins.insert(joins.end(), block);
  }

  vector<CFGBlock *> *getThreadJoins(){
    return &joins;
  }
  
  string getThreadID(){
    return std::to_string(id);
  }

  int getKind(){
    return kind;
  }

  void insertAloneArgument(VarDecl *vd, int order){
    pair <int, VarDecl*> hash (order, vd);
    aloneArgs.insert(hash);
  }

  void insertObjectArgument(FieldDecl *fld, DeclRefExpr *drex, int order){
    pair <int, FieldDecl*> hash (order, fld);
    pair <int, DeclRefExpr*> hash2 (order, drex);
    objectArgs.insert(hash);
    objectArgsOb.insert(hash2);
  }

  VarDecl *getAloneArgument(int key){
    if(aloneArgs.count(key) > 0)
      return aloneArgs.at(key);
    else
      return NULL;
  }

  FieldDecl *getObjectArgument(int key){
    if(objectArgs.count(key) > 0)
      return objectArgs.at(key);
    else
      return NULL;
  }

  DeclRefExpr *getObjectArgumentOb(int key){
    if(objectArgsOb.count(key) > 0)
      return objectArgsOb.at(key);
    else
      return NULL;
  }

  int isAloneArgument(VarDecl *vd){
    VarDecl *tmp;
    for(auto it = aloneArgs.begin(); it != aloneArgs.end(); it++){
      tmp = it->second;
      if(vd == tmp)
	return it->first;
    }
    return -1;
  }

  int isObjectArgument(DeclRefExpr *drex){
    DeclRefExpr *tmp;
    for(auto it = objectArgsOb.begin(); it != objectArgsOb.end(); it++){
      tmp = it->second;
      if(drex == tmp)
	return it->first;
    }
    return -1;
  }

 protected:
  VarDecl *threadVariable;
  CFGBlock *launcher;
  vector <CFGBlock *> detaches;
  vector <CFGBlock *> joins;
  int id;
  int kind;
  unordered_map <int, VarDecl*> aloneArgs;
  unordered_map <int, FieldDecl*> objectArgs;
  unordered_map <int, DeclRefExpr*> objectArgsOb;
};

class lambdaThreadLaunch : public threadLaunch{
public:
  lambdaThreadLaunch(LambdaExpr *lex, CFGBlock *block, int nmbr){
    lamEx = lex;
    launcher = block;
    id = nmbr;
    kind = LAMBDA_THREAD;
  }
  
  LambdaExpr *getLambdaExpr(){
    return lamEx;
  }
  
  void setFunctionNode(functionNode *fn){
    fNode = fn;
  }
  
  functionNode *getFunctionNode(){
    return fNode;
  }

protected:
  LambdaExpr *lamEx;
  functionNode *fNode;
};

class functionThreadLaunch : public threadLaunch{
public:

  functionThreadLaunch(FunctionDecl *fd, CFGBlock *block, int nmbr){
    threadFunction = fd;
    launcher = block;
    id = nmbr;
    kind = FUNCTION_THREAD;
  }

  FunctionDecl *getThreadFunction(){
    return threadFunction;
  }

protected:
  FunctionDecl *threadFunction;
};

class methodThreadLaunch : public functionThreadLaunch{
public:
  methodThreadLaunch(FunctionDecl *fd, VarDecl *ob,CFGBlock *block, int nmbr) : functionThreadLaunch(fd, block, nmbr){
    object = ob;
    kind = METHOD_THREAD;
  }

  VarDecl *getObject(){
    return object;
  }
protected:
  VarDecl *object;
};


class functionCall{
public:
  functionCall(FunctionDecl *fd) : decl(fd) {}

  FunctionDecl *getFunctionDecl(){
    return decl;
  }

  

protected:
  FunctionDecl *decl;
};

class PDTree{
public:
  PDTree(CFG *cfg){
    CFGBlock *root;
    root = &cfg->getExit();
    //Mark nodes in reverse post-order - Use depth first search.
    DFS(root);

    for(unsigned int i=0; i<reversePostorder.size(); i++){
      pair<CFGBlock*, int> hash (reversePostorder[i], i);
      b2p.insert(hash);
    }
    dom = new int[reversePostorder.size()];
    //Calculate dominators.
    calculate();
    //Create dominator index.
    index();
  }

  void DFS(CFGBlock *node){
    processedBlocks.insert(node);
    for(CFGBlock::pred_iterator it = node->pred_begin(); it != node->pred_end(); ++it){
      CFGBlock *pre = NULL;
      pre = it->getReachableBlock();
      
      if(pre != NULL)
	if(!isDiscovered(pre))
	  DFS(pre);//Recursion.
    }
    reversePostorder.insert(reversePostorder.begin(), node);
  }

  bool isDiscovered(CFGBlock *node){
    if(processedBlocks.count(node) != 0)
      return true;
    else
      return false;
  }

  void calculate(){
    bool changed = true;
    int newIdom = 0;
    CFGBlock *first;

    fill_n(dom, reversePostorder.size(), -1);
    dom[0] = 0; //root.
    
    while(changed){
      changed = false;

      for(unsigned int i=1; i<reversePostorder.size(); i++){
	for(CFGBlock::succ_iterator it = reversePostorder[i]->succ_begin(); it != reversePostorder[i]->succ_end(); ++it){
	  CFGBlock *suc = NULL;
	  suc = it->getReachableBlock();
	  if(suc != NULL)
	    if(dom[b2p.at(suc)] != -1){
	      newIdom = b2p.at(suc);
	      first = suc;
	      break;
	    }
	}
	
	for(CFGBlock::succ_iterator it = reversePostorder[i]->succ_begin(); it != reversePostorder[i]->succ_end(); ++it){	  
	  CFGBlock *suc = NULL;
	  suc = it->getReachableBlock();
	  if(suc != NULL)
	    if(suc != first)
	      if(dom[b2p.at(suc)] != -1)
		newIdom = intersect(b2p.at(suc), newIdom);
	}
	
	if(dom[b2p.at(reversePostorder[i])] != newIdom){
	  dom[b2p.at(reversePostorder[i])] = newIdom;
	  changed = true;
	}
      }
    }
  }

  int intersect(int b1,int b2){
    int finger1 = b1;
    int finger2 = b2;

    while(finger1 != finger2){
      while(finger1 > finger2){
	finger1 = dom[finger1];
      }
      while(finger2 > finger1){
	finger2 = dom[finger2];
      }
    }
    return finger1;
  }

  
  bool properlyDominates(CFGBlock *A, CFGBlock *B){
    if(A == B)
      return false;
    
    while(true){
      B = iDom.at(B);
      if(A == B)
	return true;
      if(B == iDom.at(B))
	return false;
    }
  }

  CFGBlock *getIDom(CFGBlock *A){
    return iDom.at(A);
  }

  void index(){
    for(unsigned int i=0; i<reversePostorder.size(); i++){
      pair<CFGBlock*, CFGBlock*> hash (reversePostorder[i],reversePostorder[dom[b2p.at(reversePostorder[i])]]);
      iDom.insert(hash);
    }
  }

  void print(){
    for(unsigned int i=0; i<reversePostorder.size(); i++){
      cout<<reversePostorder[i]->getBlockID()<<"---"<<reversePostorder[dom[b2p.at(reversePostorder[i])]]->getBlockID()<<endl;
    }
  }

protected:
  CFGBlockIndex iDom;
  int *dom;
  CFGBlock2Position b2p;
  vector<CFGBlock *> reversePostorder;
  set<CFGBlock *> processedBlocks;
};

class CDGraph{
public:
  CDGraph(CFG *cfg, PDTree *pdt){
    CFGBlock *currentBlock;
    CFGBlock *currentSuccessor;
    CFGBlock *parentOfCurrentBlock;
    
    for(CFG::graph_iterator I = cfg->nodes_begin(), E = cfg->nodes_end(); I != E; ++I){
      currentBlock = I;
      if(currentBlock == &cfg->getEntry()){//If on entry node, identify whichs nodes are present on every possible path.
	for(CFG::graph_iterator I2 = cfg->nodes_begin(), E2 = cfg->nodes_end(); I2 != E2; ++I2){
	  currentSuccessor = I2;
	  if(pdt->properlyDominates(currentSuccessor, currentBlock)){
	    toolBox::expandVectorMap(currentBlock, currentSuccessor, &cdm);
	  }
	}
      }
      else{
	for(CFGBlock::succ_iterator A = currentBlock->succ_begin(), T = currentBlock->succ_end(); A != T; ++A){
	  currentSuccessor = NULL;
	  currentSuccessor = A->getReachableBlock();
	  
	  if(currentSuccessor != NULL){
	    if(!pdt->properlyDominates(currentSuccessor, currentBlock)){//Examine edges (A, B) where B does not post-dominate A.
	      
	      parentOfCurrentBlock = pdt->getIDom(currentBlock);
	   
	      while(currentSuccessor != parentOfCurrentBlock){
		toolBox::expandVectorMap(currentBlock, currentSuccessor, &cdm);
		currentSuccessor = pdt->getIDom(currentSuccessor);
	      }
	    }
	  }
	}
      }
    }
  }

  vector<CFGBlock*> getControlDependentNodes(CFGBlock *A){
    return cdm.at(A);
  }

  CFGBlockVector *getControlDependencies(){
    return &cdm;
  }

protected:
  CFGBlockVector cdm;
};

class procedureAnalyser{
public:
  procedureAnalyser(const FunctionDecl *FD){
    functionDecl = FD;
    manager = new AnalysisDeclContextManager();
    analysis = new AnalysisDeclContext(&manager, FD);
    threadCounter = 0;

    //Create the CFG - Clang built-in.
    cfg = analysis->getCFG();
    //Create the PDT - Own implementation.
    pdt = new PDTree(cfg);
    //Create the CDG - Own implementation.
    cdg = new CDGraph(cfg, pdt);
    
    scanMethod();
  }

  void scanMethod(){
    CFGBlock *block;
    Stmt *ist;
    Expr *ex;
    Expr *task;
    Expr *var;
    const ParmVarDecl *pvd;
    vector<DeclRefExpr*> *dreV;
    vector<Expr *> *exV;
    vector<bool> *RefExV;
    CXXConstructExpr *conExp;
    CXXConstructorDecl *conDecl;
    DeclStmt *ds;
    NamedDecl *nd;
    FunctionDecl *fd;
    DeclRefExpr *drex;
    Decl *d;
    VarDecl *vd;
    ValueDecl *vld;
    FieldDecl *fld;
    MemberExpr *mex;
    ImplicitCastExpr *imCaEx;
    MaterializeTemporaryExpr *mte;
    LambdaExpr *lex;
    const LambdaCapture *lca;
    CallExpr *callEx;
    CXXOperatorCallExpr *opCaEx;
    FunctionDecl *callee;
    BinaryOperator *binOp;
    UnaryOperator *uOp;
    int flag;
    functionThreadLaunch *ftl;
    methodThreadLaunch *mtl;
    lambdaThreadLaunch *ltl;
    functionCall *fc;
    aloneVar *sv;
    objectVar *nsv;

    flag = 0;

    //Identify function parameters.
    for(unsigned i=0; i<functionDecl->getNumParams(); i++){
      pvd = functionDecl->getParamDecl(i);
      functionParameters.insert(functionParameters.end() ,pvd);
    }
    exV = new vector<Expr*>();
    RefExV = new vector<bool>();

    for(CFG::graph_iterator I = cfg->nodes_begin(), E = cfg->nodes_end(); I != E; ++I){
      block = I;
      for(CFGBlock::iterator I2 = block->begin(), E2 = block->end(); I2 != E2; ++I2){
	if (Optional<CFGStmt> cfgs = I2->getAs<CFGStmt>()){
	  ist= (Stmt *) cfgs->getStmt();
	  callee = NULL;
	  exV->clear();
	  RefExV->clear();

	  //Statement is a variable declaration.
	  if(isa<DeclStmt>(ist)){
	    ds = (DeclStmt *) ist;
	    
	    for(DeclStmt::decl_iterator A = ds->decl_begin(), T = ds->decl_end();A != T; ++A){
	      d = *A;
	      if(isa<VarDecl>(d)){
		vd = (VarDecl *) d;
		switch(flag){
		case FUNCTION_THREAD:
		  functionThreads.back()->setThreadVariable(vd);
		  flag = 0;
		  break;
		case METHOD_THREAD:
		  methodThreads.back()->setThreadVariable(vd);
		  flag = 0;
		  break;
		case LAMBDA_THREAD:
		  lambdaThreads.back()->setThreadVariable(vd);
		  flag = 0;
		  break;
		default:
		  variableDeclarations.insert(variableDeclarations.end(), vd);
		  break;
		}
	      }
	    }
	  }

	  //Statement is a constructor.
	  if(isa<CXXConstructExpr>(ist)){
	    conExp = (CXXConstructExpr *) ist;
	    conDecl = conExp->getConstructor();
	    
	    if(toolBox::isThreadConstructor(conDecl->getQualifiedNameAsString())){
	      flag = true;
	      ex = conExp->getArg(0);//task
	      
	      //Get thread constructor arguments.
	      if(toolBox::isBindWrapper(ex->getType().getAsString())){//std::bind
		mte = (MaterializeTemporaryExpr *) ex;
		ex = mte->GetTemporaryExpr();
		callEx = (CallExpr *) ex;
		for(unsigned i=0; i<callEx->getNumArgs();i++){
		  ex = callEx->getArg(i);
		  if(toolBox::isReferenceWrapper(ex->getType().getAsString())){//std::ref
		    ex = toolBox::getRefArgument(ex);
		    RefExV->insert(RefExV->end(), true);
		  }
		  else{
		    RefExV->insert(RefExV->end(), false);
		  }
		  exV->insert(exV->end(), ex);
		}
	      }
	      else{
		for(unsigned i=0; i<conExp->getNumArgs();i++){
		  ex = conExp->getArg(i);
		  if(toolBox::isReferenceWrapper(ex->getType().getAsString())){//std::ref
		    ex = toolBox::getRefArgument(ex);
		    RefExV->insert(RefExV->end(), true);
		  }
		  else{
		    RefExV->insert(RefExV->end(), false);
		  }
		  exV->insert(exV->end(), ex);
		}
	      }
	      
	      ex = (*exV)[0];
	      task = (*exV)[0];
	      
	      if(isa<DeclRefExpr>(task)){//thread t1 {f1, 1,2,3} - tutorial 3  - tutorial 4 - tutorial 5  - tutorial 9 ;
		drex = (DeclRefExpr *) task;
		nd = drex->getFoundDecl();
		if( (nd != NULL) && (isa<FunctionDecl>(nd))){
		  fd = (FunctionDecl *) nd;
		  
		  //Create function thread.
		  ftl = new functionThreadLaunch(fd, block, threadCounter);
		  threadCounter++;
		  functionThreads.insert(functionThreads.end() ,ftl);
		  flag = FUNCTION_THREAD;
		  
		  for(unsigned i=1; i<exV->size(); ++i){
		    var = (*exV)[i];
		    handleThreadArgument(ftl, var,(*RefExV)[i] ,i-1);
		  }
		}
	      }


	      if(isa<MaterializeTemporaryExpr>(task)){
		mte = (MaterializeTemporaryExpr *) task;
		ex = mte->GetTemporaryExpr();
		task = mte->GetTemporaryExpr();
		
		if(isa<UnaryOperator>(task)){//std::thread(&C::increase_member) - tutorial 6 - tutorial 7
		  uOp = (UnaryOperator *) task;
		  ex = uOp->getSubExpr();
		  if(isa<DeclRefExpr>(ex)){
		    drex = (DeclRefExpr *) ex;
		    nd = drex->getFoundDecl();
		    if((nd != NULL) ||  (isa<FunctionDecl>(nd))){
		      fd = (FunctionDecl *) nd;
		      
		      //////////////////////////////////////////////////
		      ex = (*exV)[1];
		      mte = (MaterializeTemporaryExpr *) ex;
		      ex = mte->GetTemporaryExpr();
		      uOp = (UnaryOperator *) ex;
		      ex = uOp->getSubExpr();
		      drex = (DeclRefExpr *) ex;//Assumption that there is the form of obj.method and NOT obj.obj2.method.
		      nd = drex->getFoundDecl();
		      vd = (VarDecl *) nd;
		      /////////////////////////////////////////////////

		      mtl = new methodThreadLaunch(fd, vd, block, threadCounter);
		      threadCounter ++;
		      methodThreads.insert(methodThreads.end(), mtl);
		      flag = METHOD_THREAD;
		      
		      for(unsigned i=2; i<exV->size(); ++i){
			var = (*exV)[i];
			handleThreadArgument(mtl, var,(*RefExV)[i] ,i-1);
		      }
		    }
		  }
		}
	
		if(isa<LambdaExpr>(task)){//Deal with lamda expresions - tutorial 10
		  lex = (LambdaExpr *) task;
		  ltl = new lambdaThreadLaunch(lex,block, threadCounter);
		  threadCounter++;
		  lambdaThreads.insert(lambdaThreads.end(), ltl);
		  flag = LAMBDA_THREAD;
		  
		  int q = 0;
		  for(LambdaExpr::capture_iterator A = lex->capture_begin(), T = lex->capture_end(); A != T; ++A){
		    lca = A;
		    vd = lca->getCapturedVar();
		    ltl->insertAloneArgument(vd, q);
		    q++;
		  }
		}
	
	      }

	    }
	  }

	  //Statement is a function call.
	  if(isa<CallExpr>(ist)){
	    callEx = (CallExpr *) ist;
	    callee = callEx->getDirectCallee();
	    
	    if(callee != NULL){//function call
	      fc = new functionCall(callee);
	      functionCalls.insert(functionCalls.end(),fc);
	      
	      //arguments
	      for(unsigned i=0; i<callEx->getNumArgs(); i++){
		ex = callEx->getArg(i);
	      }
	      
	      if(toolBox::isJoinOp(callee->getQualifiedNameAsString())){
		mex = (MemberExpr *) callEx->getCallee();
		ex = mex->getBase();
		drex = (DeclRefExpr *) ex;
		nd = drex->getFoundDecl();
		vd = (VarDecl *) nd;
		
		for (vector<functionThreadLaunch*>::iterator i = functionThreads.begin(); i != functionThreads.end(); ++i)
		  if((*i)->getThreadVariable() == vd)
		    (*i)->insertThreadJoin(block);


		for (vector<methodThreadLaunch*>::iterator i = methodThreads.begin(); i != methodThreads.end(); ++i)
		  if((*i)->getThreadVariable() == vd)
		    (*i)->insertThreadJoin(block);


		for (vector<lambdaThreadLaunch*>::iterator i = lambdaThreads.begin(); i != lambdaThreads.end(); ++i)
		  if((*i)->getThreadVariable() == vd)
		    (*i)->insertThreadJoin(block);
	      }

	      if(toolBox::isDetachOp(callee->getQualifiedNameAsString())){
		mex = (MemberExpr *) callEx->getCallee();
		ex = mex->getBase();
		drex = (DeclRefExpr *) ex;
		nd = drex->getFoundDecl();
		vd = (VarDecl *) nd;
		
		for (vector<functionThreadLaunch*>::iterator i = functionThreads.begin(); i != functionThreads.end(); ++i)
		  if((*i)->getThreadVariable() == vd)
		    (*i)->insertThreadDetach(block);


		for (vector<methodThreadLaunch*>::iterator i = methodThreads.begin(); i != methodThreads.end(); ++i)
		  if((*i)->getThreadVariable() == vd)
		    (*i)->insertThreadDetach(block);


		for (vector<lambdaThreadLaunch*>::iterator i = lambdaThreads.begin(); i != lambdaThreads.end(); ++i)
		  if((*i)->getThreadVariable() == vd)
		    (*i)->insertThreadDetach(block);
	      }


	      //mutex locks / unlocks
	      if(toolBox::isMutexLockOp(callee->getQualifiedNameAsString()) || toolBox::isMutexUnlockOp(callee->getQualifiedNameAsString())){	
		mex = (MemberExpr *) callEx->getCallee();
		ex = mex->getBase();
		var = mex->getBase();
		
		//handles: mutex1.lock() , obj1.mutex1.lock()
		if(isa<MemberExpr>(var)){
		  mex = (MemberExpr *) ex;
		  vld = mex->getMemberDecl();
		  
		  if(isa<VarDecl>(vld)){//static
		    vd = (VarDecl *) vld;
		    sv = new aloneVar(vd, block);

		    if(toolBox::isMutexLockOp(callee->getQualifiedNameAsString()))
		      sMutexLocks.insert(sMutexLocks.end(),sv);
		    else
		      sMutexUnlocks.insert(sMutexUnlocks.end(),sv);
		  }
		  else{
		    if(isa<FieldDecl>(vld)){//object member
		      fld = (FieldDecl *) vld;
		      dreV = toolBox::findDeclRefExpr(mex);
		      if(dreV->size() > 0){
			nsv = new objectVar(fld, (*dreV)[0], block);
			if(toolBox::isMutexLockOp(callee->getQualifiedNameAsString()))
			  nsMutexLocks.insert(nsMutexLocks.end(),nsv);
			else
			  nsMutexUnlocks.insert(nsMutexUnlocks.end(),nsv);
		      }
		    }
		  }
		}

		//handles uses of static or locally declared  mutexes --- class_1::mutex1.lock()
		if(isa<DeclRefExpr>(var)){
		  drex = (DeclRefExpr *) ex;//class1::mutex1
		  vld = drex->getDecl();
		  
		  if(isa<VarDecl>(vld)){//static
		    vd = (VarDecl *) vld;
		    sv = new aloneVar(vd, block);

		    if(toolBox::isMutexLockOp(callee->getQualifiedNameAsString()))
		      sMutexLocks.insert(sMutexLocks.end(),sv);
		    else
		      sMutexUnlocks.insert(sMutexUnlocks.end(),sv);
		  }
		}
		
	      }


	      if(toolBox::isAtomicOp(callee->getQualifiedNameAsString())){//atomic operations
		
		if(isa<CXXOperatorCallExpr>(callEx)){//atomic<int> counter++, atomic<int> counter = 1
		  opCaEx = (CXXOperatorCallExpr *) callEx;
		  ex = opCaEx->getArg(0);
		}
		else{
		  mex = (MemberExpr *) callEx->getCallee();
		  ex = mex->getBase();
		}
		
		if(isa<ImplicitCastExpr>(ex)){
		  imCaEx = (ImplicitCastExpr *) ex;
		  ex = imCaEx->getSubExpr();
		  var = imCaEx->getSubExpr();
		  
		  if(isa<MemberExpr>(var)){
		    mex = (MemberExpr *) ex;
		    vld = mex->getMemberDecl();
		  
		    if(isa<VarDecl>(vld)){//static
		      vd = (VarDecl *) vld;
		      sv = new aloneVar(vd, block);
		      sAtomic.insert(sAtomic.end(),sv);
		    }
		    else{
		      if(isa<FieldDecl>(vld)){//object member
			fld = (FieldDecl *) vld;
			dreV = toolBox::findDeclRefExpr(mex);
			if(dreV->size() > 0){
			  nsv = new objectVar(fld, (*dreV)[0], block);
			  nsAtomic.insert(nsAtomic.end(), nsv);
			}
		      }
		    }
		  }

		  if(isa<DeclRefExpr>(var)){
		    drex = (DeclRefExpr *) ex;
		    vld = drex->getDecl();
		    
		    if(isa<VarDecl>(vld)){//static
		      vd = (VarDecl *) vld;
		      sv = new aloneVar(vd, block);
		      sAtomic.insert(sAtomic.end(),sv);
		    }
		  }
		}
	      }
	    }
	  }
	  

	  if(isa<BinaryOperator>(ist)){
	    binOp = (BinaryOperator *) ist;
	    if(binOp->isAssignmentOp()){
	      handleLHS(binOp->getLHS(), block, true);
	      handleRHS(binOp, block);
	    }
	    if(binOp->isCompoundAssignmentOp()){
	      handleLHS(binOp->getLHS(), block, true);
	      handleLHS(binOp->getLHS(), block, false);
	      handleRHS(binOp, block);
	    }
	  }


	  if(isa<UnaryOperator>(ist)){
	    uOp = (UnaryOperator *) ist;
	    if((uOp->isIncrementOp()) || (uOp->isDecrementOp())){
	      handleLHS(uOp->getSubExpr(), block, true);
	      handleLHS(uOp->getSubExpr(), block, false);
	    }
	  }

	}
      }
    }
  }

  
  void handleLHS(Expr *left, CFGBlock *block, bool def){
    DeclRefExpr *drex;
    MemberExpr *mex;
    ValueDecl *vld;
    VarDecl *vd;
    FieldDecl *fld;
    aloneVar *sv;
    objectVar *nsv;
    ImplicitCastExpr *imCaEx;
    vector<DeclRefExpr*> *dreV;
    UnaryOperator *uOp;
    MaterializeTemporaryExpr *mte;

    if(isa<MaterializeTemporaryExpr>(left)){
      mte = (MaterializeTemporaryExpr *) left;
      left = mte->GetTemporaryExpr();
    }

    if(isa<UnaryOperator>(left)){
      uOp = (UnaryOperator *) left;
      left = uOp->getSubExpr();
    }

    if(isa<ImplicitCastExpr>(left)){
      imCaEx = (ImplicitCastExpr *) left;
      left = imCaEx->getSubExpr();
    }

    if(isa<MemberExpr>(left)){
      mex = (MemberExpr *) left;
      vld = mex->getMemberDecl();
     
      if(isa<VarDecl>(vld)){//static
	vd = (VarDecl *) vld;
	sv = new aloneVar(vd, block);
	if(def)
	  sVarDef.insert(sVarDef.end(), sv);
	else
	  sVarUse.insert(sVarUse.end(), sv);
      }
      else{
	if(isa<FieldDecl>(vld)){//object member
	  fld = (FieldDecl *) vld;
	  dreV = toolBox::findDeclRefExpr(mex);
	  if(dreV->size() > 0){
	    nsv = new objectVar(fld, (*dreV)[0], block);
	    if(def)
	      nsVarDef.insert(nsVarDef.end(), nsv);
	    else
	      nsVarUse.insert(nsVarUse.end(), nsv);
	  }
	}
      }
    }
      
    if(isa<DeclRefExpr>(left)){
      drex = (DeclRefExpr *) left;
      vld = drex->getDecl();
		  
      if(isa<VarDecl>(vld)){//static
	vd = (VarDecl *) vld;
	sv = new aloneVar(vd, block);

	if(def)
	  sVarDef.insert(sVarDef.end(), sv);
	else
	  sVarUse.insert(sVarUse.end(), sv);
      }
    }
  }

  void handleRHS(BinaryOperator *binOp, CFGBlock *block){
    Expr *ex;
    bool moreRHS = true;

    while(moreRHS){
      moreRHS = false;
      ex = binOp->getRHS();
		
      if(isa<BinaryOperator>(ex)){
	moreRHS = true;
	binOp = (BinaryOperator *) ex;
	handleLHS(binOp->getLHS(), block, false);
      }
      else{
	handleLHS(ex, block, false);
      }
	
    }
  }

  void handleThreadArgument(threadLaunch *tl, Expr *ex, bool ref, int order){
    DeclRefExpr *drex;
    MemberExpr *mex;
    ValueDecl *vld;
    VarDecl *vd;
    FieldDecl *fld;
    ImplicitCastExpr *imCaEx;
    vector<DeclRefExpr*> *dreV;
    UnaryOperator *uOp;
    MaterializeTemporaryExpr *mte;
    
    if(isa<MaterializeTemporaryExpr>(ex)){
      mte = (MaterializeTemporaryExpr *) ex;
      ex = mte->GetTemporaryExpr();
    }

    if(isa<UnaryOperator>(ex)){
      uOp = (UnaryOperator *) ex;
      ex = uOp->getSubExpr();
    }

    if(isa<ImplicitCastExpr>(ex)){
      imCaEx = (ImplicitCastExpr *) ex;
      ex = imCaEx->getSubExpr();
    }

    if(isa<MemberExpr>(ex)){
      mex = (MemberExpr *) ex;
      vld = mex->getMemberDecl();
		  
      if(isa<VarDecl>(vld)){//static
	vd = (VarDecl *) vld;
	tl->insertAloneArgument(vd, order);
      }
      else{
	if(isa<FieldDecl>(vld)){//object member
	  fld = (FieldDecl *) vld;
	  dreV = toolBox::findDeclRefExpr(mex);
	  if(dreV->size() > 0){
	    tl->insertObjectArgument(fld,(*dreV)[0], order);
	  }
	}
      }
    }
      
    if(isa<DeclRefExpr>(ex)){
      drex = (DeclRefExpr *) ex;
      vld = drex->getDecl();

      if(isa<VarDecl>(vld)){//static
	vd = (VarDecl *) vld;
	tl->insertAloneArgument(vd, order);
      }
    }
  }

  CFG *getCFG(){
    return cfg;
  }

  CDGraph *getCDG(){
    return cdg;
  }

  const FunctionDecl *getFunctionDecl(){
    return functionDecl;
  }

  
  vector <const ParmVarDecl *> *getFunctionParameters(){
    return &functionParameters;
  }

  vector<VarDecl *> *getVariableDeclarations(){
    return &variableDeclarations;
  }

  vector<functionCall *> *getFunctionCalls(){
    return &functionCalls;
  }

  vector<functionThreadLaunch *> *getFunctionThreads(){
    return &functionThreads;
  }

  vector<methodThreadLaunch *> *getMethodThreads(){
    return &methodThreads;
  }

  vector<lambdaThreadLaunch *> *getLambdaThreads(){
    return &lambdaThreads;
  }

  vector<aloneVar *> *getStaticMutexLocks(){
    return &sMutexLocks;
  }

  vector<aloneVar *> *getStaticMutexUnlocks(){
    return &sMutexUnlocks;
  }

  vector<aloneVar *> *getStaticAtomics(){
    return &sAtomic;
  }

  vector<aloneVar *> *getStaticVarUses(){
    return &sVarUse;
  }

  vector<aloneVar *> *getStaticVarDefs(){
    return &sVarDef;
  }

  vector<objectVar *> *getNonStaticMutexLocks(){
    return &nsMutexLocks;
  }

  vector<objectVar *> *getNonStaticMutexUnlocks(){
    return &nsMutexUnlocks;
  }

  vector<objectVar *> *getNonStaticAtomics(){
    return &nsAtomic;
  }

  vector<objectVar *> *getNonStaticVarUses(){
    return &nsVarUse;
  }

  vector<objectVar *> *getNonStaticVarDefs(){
    return &nsVarDef;
  }

  bool isDeclaredLocally(VarDecl *vd){
    if(std::find(variableDeclarations.begin(), variableDeclarations.end(), vd) != variableDeclarations.end()) {
      return true;
    } else {
      return false;
    }
  }

  int isFunctionArgument(VarDecl *vd){
    for(unsigned i=0; i<functionParameters.size(); i++)
      if(functionParameters[i] == vd)
	return i;
    return -1;
  }

protected:
  vector <const ParmVarDecl *> functionParameters;
  vector<VarDecl *> variableDeclarations;

  vector<functionCall *> functionCalls;

  vector<functionThreadLaunch *> functionThreads;
  vector<methodThreadLaunch *> methodThreads;
  vector<lambdaThreadLaunch *> lambdaThreads;
  
  vector<aloneVar *> sMutexLocks;
  vector<objectVar *> nsMutexLocks;
  vector<aloneVar *> sMutexUnlocks;
  vector<objectVar *> nsMutexUnlocks;

  vector<aloneVar *> sAtomic;
  vector<objectVar *> nsAtomic;

  vector<aloneVar *> sVarUse;
  vector<objectVar *> nsVarUse;
  vector<aloneVar *> sVarDef;
  vector<objectVar *> nsVarDef;

  int threadCounter;
  CFG *cfg;
  CDGraph *cdg;
  PDTree *pdt;
  const FunctionDecl *functionDecl;
  AnalysisDeclContextManager manager;
  AnalysisDeclContext *analysis;
};

class node{
public:
  void setNodeID(string id){
    nodeID = id;
  }

  string getNodeID(){
    return nodeID;
  }

  void setSignature(string sign){
    signature = sign;
  }

  string getSignature(){
    return signature;
  }

  void setHasDefinition(bool def){
    definition = def;
  }

  bool hasDefinition(){
    return definition;
  }


protected:
  string nodeID;
  string signature;
  bool definition;
};

class procedureNode : public node{
public:
  procedureNode() {}

  procedureAnalyser *getAnalysis(){
    if(!hasDefinition())
      cerr<<"Attempt to ask analysis from procedure without definition. Returning NULL..."<<endl;
    return analysis;
  }

protected:
  procedureAnalyser *analysis;
  const FunctionDecl *functionDecl;
};

class methodNode : public procedureNode{
public:
  methodNode(CXXMethodDecl *md){
    const FunctionDecl *fd;
    
    methodDecl = md;
    if(methodDecl->hasBody(fd)){
      setHasDefinition(true);
      functionDecl = fd;
      analysis = new procedureAnalyser(functionDecl);
    }
    else{
      setHasDefinition(false);
      functionDecl = NULL;
      analysis = NULL;
    }
  }

  CXXMethodDecl *getMethodDecl(){
    return methodDecl;
  }

  const FunctionDecl *getFunctionDecl(){
    if(!hasDefinition())
      cerr<<"Attempt to ask function declaration from method without definition. Returning NULL..."<<endl;
    return functionDecl;
  }

  bool isVirtual(){
    return methodDecl->isVirtual();
  }

  int getMembershipType(){
    return methodDecl->getAccess();
  }

  set<methodNode*> *getOverriders(){
    return &overriders;
  }

  void insertOverrider(methodNode *mn){
    overriders.insert(mn);
  }

protected:
  CXXMethodDecl *methodDecl;
  set<methodNode*> overriders;
};

class functionNode : public procedureNode{
public:
  functionNode(FunctionDecl *fd){
    const FunctionDecl *bfd;

    if(fd->hasBody(bfd)){
      setHasDefinition(true);
      functionDecl = bfd;
      analysis = new procedureAnalyser(functionDecl);
    }
    else{
      setHasDefinition(false);
      functionDecl = fd;
      analysis = NULL;
    }
  }

  const FunctionDecl *getFunctionDecl(){
    return functionDecl;
  }
};


class classNode : public node{
public:
  classNode(CXXRecordDecl *cd) {
    if(cd->hasDefinition()){
      setHasDefinition(true);
      classDecl = cd->getDefinition();
    }
    else{
      setHasDefinition(false);
      classDecl = cd;
    }
  }
 
  void insertMemberMethod(methodNode *mn, int acc){
    switch(acc){
    case M_PUBLIC: publicMethods.insert(mn);
      break;
    case M_PROTECTED: protectedMethods.insert(mn);
      break;
    case M_PRIVATE: privateMethods.insert(mn);
      break;
    default:cerr<<"Uknown membership type."<<endl;
      break;
    }
  }

  void insertBaseClass(classNode *cn, int acc){
    switch(acc){
    case M_PUBLIC: publicBases.insert(cn);
      break;
    case M_PROTECTED: protectedBases.insert(cn);
      break;
    case M_PRIVATE: privateBases.insert(cn);
      break;
    default:cerr<<"Uknown inheritance type."<<endl;
      break;
    }
  }

  void insertFriendClass(classNode *cn){
    friendClasses.insert(cn);
  }

  void insertFriendProcedure(procedureNode *pn){
    friendProcedures.insert(pn);
  }

  CXXRecordDecl *getClassDecl(){
    return classDecl;
  }

  set<classNode*> *getPublicBases(){
    return &publicBases;
  }

  set<classNode*> *getProtectedBases(){
    return &protectedBases;
  }

  set<classNode*> *getPrivateBases(){
    return &privateBases;
  }

  set<methodNode*> *getPublicMethods(){
    return &publicMethods;
  }

  set<methodNode*> *getProtectedMethods(){
    return &protectedMethods;
  }

  set<methodNode*> *getPrivateMethods(){
    return &privateMethods;
  }

  set<classNode*> *getFriendClasses(){
    return &friendClasses;
  }

  set<procedureNode*> *getFriendProcedures(){
    return &friendProcedures;
  }

  bool isVirtualInheritance(classNode *cn){
    if(baseSpec.count(cn) > 0)
      return baseSpec.at(cn)->isVirtual();
    else{
      cerr<<"No such inheritance. Returning FALSE..."<<endl;
      return false;
    }
  }

  inheritanceInfo *getBaseSpec(){
    return &baseSpec;
  }

protected:
  CXXRecordDecl *classDecl;

  set<classNode*> publicBases;
  set<classNode*> protectedBases;
  set<classNode*> privateBases;
  inheritanceInfo baseSpec;

  set<methodNode*> publicMethods;
  set<methodNode*> protectedMethods;
  set<methodNode*> privateMethods;

  set<classNode*> friendClasses;
  set<procedureNode*> friendProcedures;
};

class graphManager{
public:
  graphManager(ASTContext *Context) : nodeCounter(0),  Context(Context) {}
  
  ///////////////////////////////////////////////////////////////////////
  //
  //EXPORTING GRAPH
  //
  ///////////////////////////////////////////////////////////////////////

  void exportGraph(){
    produceCHS();
    produceProcedureGraphs();
  }

  void produceCHS(){
    ofstream gf;
    gf.open ("CHS.dot");
    gf<<"digraph G {"<<endl;
    gf<<createNodes()<<endl;
    gf<<createClassHierarchy()<<endl;
    gf<<createMembershipEdges()<<endl;
    gf<<createFriendEdges()<<endl;
    gf<<createOverrideEdges()<<endl;
    gf<<"}"<<endl;
    gf.close();
    //system ("sfdp -x -Goverlap=scale -Tpng CHS.dot > CHS.png");
    system ("dot -Tpng CHS.dot > CHS.png");
  }

  void produceProcedureGraphs(){
    methodNode *mn;
    functionNode *fn;
    
    for (auto it = mHash.begin(); it != mHash.end(); ++it){
      mn = it->second;
      getPGraph(mn);
    }
    
    for (auto it = fHash.begin(); it != fHash.end(); ++it){
      fn = it->second;
      getPGraph(fn);
    }

  }

  void getPGraph(procedureNode *pn){
    if(pn->hasDefinition()){
      ofstream gf;
      string fname, gname, iname, command; 
      
      fname = pn->getAnalysis()->getFunctionDecl()->getQualifiedNameAsString();
      fname.append("-");
      fname.append(pn->getNodeID());
      gname = fname;
      gname.append(".dot");
      iname = fname;
      iname.append(".png");

      gf.open (gname);
      gf<<"digraph G {"<<endl;
      
      gf<<createProcedureGraph(pn,"lightblue", "");
      gf<<createThreadGraphs(pn);
      gf<<createThreadEdges(pn);

      gf<<"}"<<endl;
      gf.close();
      //command = "sfdp -x -Goverlap=scale -Tpng ";
      command = "dot -Tpng ";
      command.append(gname);
      command.append(" > ");
      command.append(iname);
      system ((command).c_str());
    }
  }

  string createProcedureGraph(procedureNode *pn, string color, string suffix){
    string out = "";

    out.append(createCFGNodes(pn,color,suffix));
    out.append(createFlowEdges(pn,suffix));
    out.append(createControlDependenceEdges(pn,suffix));

    return out;
  }
  
  string createCFGNodes(procedureNode *pn, string color, string suffix){
    CFG *cfg;
    CFGBlock *block;
    string out = "";
    out.append("subgraph cluster_");
    out.append(pn->getNodeID());
    out.append(suffix+"\n");
    out.append("{style=filled;\n");
    out.append("color=");
    out.append(color);
    out.append(";\n");
    out.append("label=\"");
    out.append(pn->getAnalysis()->getFunctionDecl()->getQualifiedNameAsString());
    out.append(" --- signature: ");
    out.append(pn->getSignature());
    out.append("\";\n");

    cfg = pn->getAnalysis()->getCFG();
    for(CFG::graph_iterator I = cfg->nodes_begin(), E = cfg->nodes_end(); I != E; ++I){
      block = I;
      out.append(bidHash.at(block)+suffix+" [shape=box, label=\""+constructCFGLabel(cfg ,block)+"\"];\n");
    }

    out.append("}");
    return out;
  }

  string createFlowEdges(procedureNode *pn,string suffix){
    CFG *cfg;
    CFGBlock *block;
    CFGBlock *currentSuccessor;
    string out = "";
    
    cfg = pn->getAnalysis()->getCFG();
    for(CFG::graph_iterator I = cfg->nodes_begin(), E = cfg->nodes_end(); I != E; ++I){
      block = I;
      for(CFGBlock::succ_iterator A = block->succ_begin(), T = block->succ_end(); A != T; ++A){
	  currentSuccessor = NULL;
	  currentSuccessor = A->getReachableBlock();
	  if(currentSuccessor != NULL){
	    out.append("edge [color=black, style=solid];\n");
	    out.append(bidHash.at(block)+suffix+"->"+bidHash.at(currentSuccessor)+suffix+";\n");
	  }
      }
    }
    return out;
  }

  string createControlDependenceEdges(procedureNode *pn, string suffix){
    string out = "";
    CFGBlock *block1, *block2;
    vector<CFGBlock*> vec;
    
    for (auto it = pn->getAnalysis()->getCDG()->getControlDependencies()->begin(); it != pn->getAnalysis()->getCDG()->getControlDependencies()->end(); ++it ){
      block1 = it->first;
      vec = it->second;
      for (auto it2 = vec.begin(); it2 != vec.end(); ++it2){
	block2 = *it2;
	out.append("edge [color=black, style=dotted];\n");
	out.append(bidHash.at(block1)+suffix+"->"+bidHash.at(block2)+suffix+";\n");
      }
    }

    out.append("edge [color=black, style=solid];\n");
    return out;
  }
  
  string createThreadGraphs(procedureNode *pn){
    string out = "";

    for (vector<functionThreadLaunch*>::iterator i = pn->getAnalysis()->getFunctionThreads()->begin(); i != pn->getAnalysis()->getFunctionThreads()->end(); ++i)
      out.append(getThreadGraph(pn, (*i)));
    for (vector<methodThreadLaunch*>::iterator i = pn->getAnalysis()->getMethodThreads()->begin(); i != pn->getAnalysis()->getMethodThreads()->end(); ++i)
      out.append(getThreadGraph(pn, (*i)));
    for (vector<lambdaThreadLaunch*>::iterator i = pn->getAnalysis()->getLambdaThreads()->begin(); i != pn->getAnalysis()->getLambdaThreads()->end(); ++i)
      out.append(getThreadGraph(pn, (*i)));

    return out;
  }


  string getThreadGraph(procedureNode *pn, threadLaunch *tl){
    string out = "";
    bool flag = false;
    procedureNode *fpn;
    functionThreadLaunch *ftl;
    methodThreadLaunch *mtl;
    lambdaThreadLaunch *ltl;
    CFGBlock *launcher;
    CFGBlock *entry;
    CFGBlock *exit;
    
    if(tl->getKind() == FUNCTION_THREAD){
      ftl= (functionThreadLaunch *) tl;
      if(fHash.count(getSignature(ftl->getThreadFunction())) > 0){
	fpn = fHash.at(getSignature(ftl->getThreadFunction()));
	flag = true;
      }
    }

    if(tl->getKind() == METHOD_THREAD){
      mtl = (methodThreadLaunch *) tl;
      if(mHash.count(getSignature(mtl->getThreadFunction())) > 0){
	fpn = mHash.at(getSignature(mtl->getThreadFunction()));
	flag = true;
      }
    }

    if(tl->getKind() == LAMBDA_THREAD){
      ltl = (lambdaThreadLaunch *) tl;
      fpn = ltl->getFunctionNode();
      flag = true;
    }
    

    if(flag){
      out.append("subgraph cluster_");
      out.append(pn->getNodeID());
      out.append(tl->getThreadID()+"\n");
      out.append("{\n");      
      out.append(createProcedureGraph(fpn,"pink",tl->getThreadID()));
      out.append(createProcedureGraph(fpn,"gold",tl->getThreadID()+"d"));
      out.append("}\n");
	
	
      //Thread launch edges.
      launcher = tl->getThreadLauncher();
      entry = &fpn->getAnalysis()->getCFG()->getEntry();
      out.append("edge [dir=forward, label=\""+tl->getThreadVariable()->getQualifiedNameAsString()+"\", color=green, style=solid];\n");
      out.append(bidHash.at(launcher)+"->"+bidHash.at(entry)+tl->getThreadID()+";\n");
      //out.append(bidHash.at(launcher)+"->"+bidHash.at(entry)+(*i)->getThreadID()+"d"+";\n");
	
      //Thread join edges.
      exit = &fpn->getAnalysis()->getCFG()->getExit();
      for(vector<CFGBlock*>::iterator j = tl->getThreadJoins()->begin(); j != tl->getThreadJoins()->end(); ++j){
	out.append("edge [dir=forward, label=\""+tl->getThreadVariable()->getQualifiedNameAsString()+"\", color=green4, style=solid];\n");
	out.append(bidHash.at(exit)+tl->getThreadID()+"->"+bidHash.at((*j))+";\n");
	//out.append(bidHash.at(exit)+(*i)->getThreadID()+"d->"+bidHash.at((*j))+";\n");
      }
    }

    return out;
  }


  string createThreadEdges(procedureNode *pn){
    CFGBlock *currentBlock;
    CFGBlock *block;
    string label;
    string id;
    string out = "";
    
    for(CFG::graph_iterator I = pn->getAnalysis()->getCFG()->nodes_begin(), E = pn->getAnalysis()->getCFG()->nodes_end(); I != E; ++I){
      currentBlock = I;
      
      if(sMutexLockCrash.count(currentBlock) > 0){
	for (unsigned i = 0; i < sMutexLockCrash.at(currentBlock).size(); i++){
	  block = sMutexLockCrash.at(currentBlock)[i];
	  label = sMutexLockCrashLabel.at(currentBlock)[i];
	  id = sMutexLockCrashId.at(currentBlock)[i];
	  out.append("edge [dir=back,color=red, label=\""+label+"\" ,style=solid];\n");
	  out.append(bidHash.at(currentBlock)+"->"+bidHash.at(block)+id+";\n");
	}
      }

      if(sMutexUnlockCrash.count(currentBlock) > 0){
	for (unsigned i = 0; i < sMutexUnlockCrash.at(currentBlock).size(); i++){
	  block = sMutexUnlockCrash.at(currentBlock)[i];
	  label = sMutexUnlockCrashLabel.at(currentBlock)[i];
	  id = sMutexUnlockCrashId.at(currentBlock)[i];
	  out.append("edge [dir=forward,color=red, label=\""+label+"\" ,style=solid];\n");
	  out.append(bidHash.at(currentBlock)+"->"+bidHash.at(block)+id+";\n");
	}
      }

      if(sAtomicCrash.count(currentBlock) > 0){
	for (unsigned i = 0; i < sAtomicCrash.at(currentBlock).size(); i++){
	  block = sAtomicCrash.at(currentBlock)[i];
	  label = sAtomicCrashLabel.at(currentBlock)[i];
	  id = sAtomicCrashId.at(currentBlock)[i];
	  out.append("edge [dir=both,color=orange, label=\""+label+"\" ,style=solid];\n");
	  out.append(bidHash.at(currentBlock)+"->"+bidHash.at(block)+id+";\n");
	}
      }

      if(sVarDefDefCrash.count(currentBlock) > 0){
	for (unsigned i = 0; i < sVarDefDefCrash.at(currentBlock).size(); i++){
	  block = sVarDefDefCrash.at(currentBlock)[i];
	  label = sVarDefDefCrashLabel.at(currentBlock)[i];
	  id = sVarDefDefCrashId.at(currentBlock)[i];
	  out.append("edge [dir=both,color=purple, label=\""+label+"\" ,style=solid];\n");
	  out.append(bidHash.at(currentBlock)+"->"+bidHash.at(block)+id+";\n");
	}
      }

      if(sVarUseDefCrash.count(currentBlock) > 0){
	for (unsigned i = 0; i < sVarUseDefCrash.at(currentBlock).size(); i++){
	  block = sVarUseDefCrash.at(currentBlock)[i];
	  label = sVarUseDefCrashLabel.at(currentBlock)[i];
	  id = sVarUseDefCrashId.at(currentBlock)[i];
	  out.append("edge [dir=both,color=turquoise, label=\""+label+"\" ,style=solid];\n");
	  out.append(bidHash.at(currentBlock)+"->"+bidHash.at(block)+id+";\n");
	}
      }

      if(sVarDefUseCrash.count(currentBlock) > 0){
	for (unsigned i = 0; i < sVarDefUseCrash.at(currentBlock).size(); i++){
	  block = sVarDefUseCrash.at(currentBlock)[i];
	  label = sVarDefUseCrashLabel.at(currentBlock)[i];
	  id = sVarDefUseCrashId.at(currentBlock)[i];
	  out.append("edge [dir=both,color=chocolate, label=\""+label+"\" ,style=solid];\n");
	  out.append(bidHash.at(currentBlock)+"->"+bidHash.at(block)+id+";\n");
	}
      }

      if(nsMutexLockCrash.count(currentBlock) > 0){
	for (unsigned i = 0; i < nsMutexLockCrash.at(currentBlock).size(); i++){
	  block = nsMutexLockCrash.at(currentBlock)[i];
	  label = nsMutexLockCrashLabel.at(currentBlock)[i];
	  id = nsMutexLockCrashId.at(currentBlock)[i];
	  out.append("edge [dir=back,color=red, label=\""+label+"\" ,style=solid];\n");
	  out.append(bidHash.at(currentBlock)+"->"+bidHash.at(block)+id+";\n");
	}
      }

      if(nsMutexUnlockCrash.count(currentBlock) > 0){
	for (unsigned i = 0; i < nsMutexUnlockCrash.at(currentBlock).size(); i++){
	  block = nsMutexUnlockCrash.at(currentBlock)[i];
	  label = nsMutexUnlockCrashLabel.at(currentBlock)[i];
	  id = nsMutexUnlockCrashId.at(currentBlock)[i];
	  out.append("edge [dir=forward,color=red, label=\""+label+"\" ,style=solid];\n");
	  out.append(bidHash.at(currentBlock)+"->"+bidHash.at(block)+id+";\n");
	}
      }

      if(nsAtomicCrash.count(currentBlock) > 0){
	for (unsigned i = 0; i < nsAtomicCrash.at(currentBlock).size(); i++){
	  block = nsAtomicCrash.at(currentBlock)[i];
	  label = nsAtomicCrashLabel.at(currentBlock)[i];
	  id = nsAtomicCrashId.at(currentBlock)[i];
	  out.append("edge [dir=both,color=orange, label=\""+label+"\" ,style=solid];\n");
	  out.append(bidHash.at(currentBlock)+"->"+bidHash.at(block)+id+";\n");
	}
      }

      if(nsVarDefDefCrash.count(currentBlock) > 0){
	for (unsigned i = 0; i < nsVarDefDefCrash.at(currentBlock).size(); i++){
	  block = nsVarDefDefCrash.at(currentBlock)[i];
	  label = nsVarDefDefCrashLabel.at(currentBlock)[i];
	  id = nsVarDefDefCrashId.at(currentBlock)[i];
	  out.append("edge [dir=both,color=purple, label=\""+label+"\" ,style=solid];\n");
	  out.append(bidHash.at(currentBlock)+"->"+bidHash.at(block)+id+";\n");
	}
      }

      if(nsVarUseDefCrash.count(currentBlock) > 0){
	for (unsigned i = 0; i < nsVarUseDefCrash.at(currentBlock).size(); i++){
	  block = nsVarUseDefCrash.at(currentBlock)[i];
	  label = nsVarUseDefCrashLabel.at(currentBlock)[i];
	  id = nsVarUseDefCrashId.at(currentBlock)[i];
	  out.append("edge [dir=both,color=turquoise, label=\""+label+"\" ,style=solid];\n");
	  out.append(bidHash.at(currentBlock)+"->"+bidHash.at(block)+id+";\n");
	}
      }

      if(nsVarDefUseCrash.count(currentBlock) > 0){
	for (unsigned i = 0; i < nsVarDefUseCrash.at(currentBlock).size(); i++){
	  block = nsVarDefUseCrash.at(currentBlock)[i];
	  label = nsVarDefUseCrashLabel.at(currentBlock)[i];
	  id = nsVarDefUseCrashId.at(currentBlock)[i];
	  out.append("edge [dir=both,color=chocolate, label=\""+label+"\" ,style=solid];\n");
	  out.append(bidHash.at(currentBlock)+"->"+bidHash.at(block)+id+";\n");
	}
      }
      


    }
    return out;
  }


  string constructCFGLabel(CFG *cfg ,CFGBlock *block){
    string OutStr;
    std::ostringstream stream;
    raw_os_ostream *rs = new raw_os_ostream(stream);
    LangOptions *lo = new LangOptions();
    block->print (*rs,  cfg, *lo, false);
    rs->flush();
    OutStr = stream.str();

    if (OutStr[0] == '\n') OutStr.erase(OutStr.begin());
    for (unsigned i = 0; i != OutStr.length(); ++i){
      if (OutStr[i] == '\n') {           
        OutStr[i] = '\\';
	OutStr.insert(OutStr.begin()+i+1, 'l');
      }

      if(OutStr[i] == '\"'){//for graphviz...
      	OutStr[i] = '\'';
      }

    }

    return OutStr;
    
    /*string str;
    llvm::DOTGraphTraits< const CFG * > labelGen;
    str = labelGen.getNodeLabel(block, cfg);
    return str;*/
  }

  

  string createNodes(){
    classNode *cn;
    methodNode *mn;
    functionNode *fn;
    string out = "";
    
    out.append("{");
    for (auto it = cHash.begin(); it != cHash.end(); ++it){
      cn = it->second;
      out.append(cn->getNodeID()+" [shape=ellipse, label=\""+cn->getClassDecl()->getQualifiedNameAsString()+"\"];\n");
    }

    for (auto it = mHash.begin(); it != mHash.end(); ++it){
      mn = it->second;
      if(mn->isVirtual())
	out.append(mn->getNodeID()+" [shape=octagon, label=\""+mn->getMethodDecl()->getQualifiedNameAsString()+"\"];\n");
      else
	out.append(mn->getNodeID()+" [shape=box, label=\""+mn->getMethodDecl()->getQualifiedNameAsString()+"\"];\n");
    }

    for (auto it = fHash.begin(); it != fHash.end(); ++it){
      fn = it->second;
      out.append(fn->getNodeID()+"[shape=box, label=\""+fn->getFunctionDecl()->getQualifiedNameAsString()+"\"];\n");
      }
    out.append("}");

    return out;
  }
  
  string createClassHierarchy(){
    classNode *cn;
    classNode *bcn;
    string out = "";
 
    for (auto it = cHash.begin(); it != cHash.end(); ++it){
      cn = it->second;
       
      for (std::set<classNode*>::iterator it2 = cn->getPublicBases()->begin(); it2!=cn->getPublicBases()->end(); ++it2){
	bcn = *it2;
	
	if(cn->isVirtualInheritance(bcn))
	  out.append("edge [dir=back, color=green4, style=dotted];\n");
	else
	  out.append("edge [dir=back, color=green4, style=solid];\n");
	
	out.append(bcn->getNodeID()+"->"+cn->getNodeID()+";\n");
      }

      for (std::set<classNode*>::iterator it3 = cn->getPrivateBases()->begin(); it3!=cn->getPrivateBases()->end(); ++it3){
	bcn = *it3;
	
	if(cn->isVirtualInheritance(bcn))
	  out.append("edge [dir=back, color=red4, style=dotted];\n");
	else
	  out.append("edge [dir=back, color=red4, style=solid];\n");
	
	out.append(bcn->getNodeID()+"->"+cn->getNodeID()+";\n");
      }

      for (std::set<classNode*>::iterator it4 = cn->getProtectedBases()->begin(); it4!=cn->getProtectedBases()->end(); ++it4){
	bcn = *it4;
	
	if(cn->isVirtualInheritance(bcn))
	  out.append("edge [dir=back, color=orange4, style=dotted];\n");
	else
	  out.append("edge [dir=back, color=orange4, style=solid];\n");

	out.append(bcn->getNodeID()+"->"+cn->getNodeID()+";\n");
      }
    }
    out.append("edge [dir=forward, style=solid];\n");
    return out;
  }

  string createMembershipEdges(){
    classNode *cn;
    methodNode *mn;
    string out = "";

    for (auto it = cHash.begin(); it != cHash.end(); ++it){
      cn = it->second;
      
      for (std::set<methodNode*>::iterator it2 = cn->getPublicMethods()->begin(); it2!=cn->getPublicMethods()->end(); ++it2){
	mn = *it2;
	out.append("edge [color=green];\n");
	out.append(cn->getNodeID()+"->"+mn->getNodeID()+";\n");
      }


      for (std::set<methodNode*>::iterator it2 = cn->getProtectedMethods()->begin(); it2!=cn->getProtectedMethods()->end(); ++it2){
	mn = *it2;
	out.append("edge [color=orange];\n");
	out.append(cn->getNodeID()+"->"+mn->getNodeID()+";\n");
      }

      
      for (std::set<methodNode*>::iterator it2 = cn->getPrivateMethods()->begin(); it2!=cn->getPrivateMethods()->end(); ++it2){
	mn = *it2;
	out.append("edge [color=red];\n");
	out.append(cn->getNodeID()+"->"+mn->getNodeID()+";\n");
      }
    }
    return out;
  }

  string createFriendEdges(){
    classNode *cn;
    classNode *fcn;
    procedureNode *fpn;
    string out = "";

    for (auto it = cHash.begin(); it != cHash.end(); ++it){
      cn = it->second;
      
      for (std::set<classNode*>::iterator it2 = cn->getFriendClasses()->begin(); it2!=cn->getFriendClasses()->end(); ++it2){
	fcn = *it2;
	out.append("edge [color=blue];\n");
	out.append(cn->getNodeID()+"->"+fcn->getNodeID()+";\n");
      }

      for (std::set<procedureNode*>::iterator it2 = cn->getFriendProcedures()->begin(); it2!=cn->getFriendProcedures()->end(); ++it2){
	fpn = *it2;
	out.append("edge [color=blue];\n");
	out.append(cn->getNodeID()+"->"+fpn->getNodeID()+";\n");
      }
    }
    return out;
  }

  string createOverrideEdges(){
    methodNode *mn;
    methodNode *omn;
    string out = "";

    for (auto it = mHash.begin(); it != mHash.end(); ++it){
      mn = it->second;
      for (std::set<methodNode*>::iterator it2 = mn->getOverriders()->begin(); it2!=mn->getOverriders()->end(); ++it2){
	omn = *it2;
	out.append("edge [color=purple];\n");
	out.append(omn->getNodeID()+"->"+mn->getNodeID()+";\n");
      }
    }
    return out;
  }

  ///////////////////////////////////////////////////////////////////////
  //
  //EXPANDING GRAPH
  //
  ///////////////////////////////////////////////////////////////////////

  void expandGraph(){
    produceCFGNodes();
    calculateImmediateBases();
    calculateFriendships();
    calculateOverriders();
    produceConcurrentDependencies();
  }

  void produceCFGNodes(){
    methodNode *mn;
    functionNode *fn;
    
    for (auto it = mHash.begin(); it != mHash.end(); ++it){
      mn = it->second;
      setCFGBlockID(mn);
    }
    
    for (auto it = fHash.begin(); it != fHash.end(); ++it){
      fn = it->second;
      setCFGBlockID(fn);
    }

  }

  void setCFGBlockID(procedureNode *pn){
    CFG *cfg;
    CFGBlock *block;
    if(pn->hasDefinition()){
      cfg = pn->getAnalysis()->getCFG();
      for(CFG::graph_iterator I = cfg->nodes_begin(), E = cfg->nodes_end(); I != E; ++I){
	block = I;
	pair <CFGBlock *, string> hash (block, draftNodeID());
	bidHash.insert(hash);
      }
    }
  }

  void calculateImmediateBases(){
    classNode *cn;
    CXXRecordDecl *cd;
    CXXBaseSpecifier *B;
    const CXXRecordDecl *base;
    classNode *bcn;
    for (auto it = cHash.begin(); it != cHash.end(); ++it){
      cn = it->second;
      cd = cn->getClassDecl();
      
      for(CXXRecordDecl::base_class_iterator A = cd->bases_begin(), T = cd->bases_end(); A != T; ++A){
	B = A;
	base =  B->getType().getTypePtrOrNull()->getAsCXXRecordDecl();

	if(base != NULL){
	  if(cHash.count(base->getQualifiedNameAsString()) > 0){
	    bcn =  cHash.at(base->getQualifiedNameAsString());
	    cn->insertBaseClass(bcn, B->getAccessSpecifier());
	    
	    pair<classNode*, CXXBaseSpecifier*> hash (bcn, B);
	    cn->getBaseSpec()->insert(hash);
	  }
	}
      }
    }
  }

  void calculateFriendships(){
    classNode *cn;
    classNode *fcn;
    CXXRecordDecl *cd;
    FriendDecl *fd;
    NamedDecl *nd;
    FunctionDecl *fnd;
    const CXXRecordDecl *base;
    string sign;
    
    for (auto it = cHash.begin(); it != cHash.end(); ++it){
      cn = it->second;
      cd = cn->getClassDecl();

      for(CXXRecordDecl::friend_iterator A = cd->friend_begin(), T = cd->friend_end(); A != T; ++A){
	fd = *A;
	nd = fd->getFriendDecl();
	
	if(nd == NULL){//Friend is a class.
	  base = fd->getFriendType()->getType().getTypePtrOrNull()->getAsCXXRecordDecl();
	  if(base != NULL){
	    if(cHash.count(base->getQualifiedNameAsString()) > 0){
	      fcn = cHash.at(base->getQualifiedNameAsString());
	      cn->insertFriendClass(fcn);
	    }
	  }
	}
	else{//Friend is a method.
	  if(isa<FunctionDecl>(nd)){
	    fnd = (FunctionDecl *) nd;
	    sign = getSignature(fnd);
	    if(mHash.count(sign) > 0)
	      cn->insertFriendProcedure(mHash.at(sign));
	    if(fHash.count(sign) > 0)
	      cn->insertFriendProcedure(fHash.at(sign));
	  }
	}
      }
    }
  }

  void calculateOverriders(){
    classNode *cn;
    methodNode *mn;
    CXXMethodDecl *md;
    const CXXMethodDecl *omdc;
    methodNode *omd;
    string sign;

    for (auto it = cHash.begin(); it != cHash.end(); ++it){
      cn = it->second;
      
      for (std::set<methodNode*>::iterator it2 = cn->getPublicMethods()->begin(); it2!=cn->getPublicMethods()->end(); ++it2){
	mn = *it2;
	md = mn->getMethodDecl();

	for(CXXMethodDecl::method_iterator A = md->begin_overridden_methods (), T = md->end_overridden_methods (); A != T; ++A){
	  omdc = *A;
	  sign = getSignature(omdc);
	  if(mHash.count(sign) > 0){
	    omd  = mHash.at(sign);
	    omd->insertOverrider(mn);
	  }
	}
      }

      for (std::set<methodNode*>::iterator it2 = cn->getProtectedMethods()->begin(); it2!=cn->getProtectedMethods()->end(); ++it2){
	mn = *it2;
	md = mn->getMethodDecl();

	for(CXXMethodDecl::method_iterator A = md->begin_overridden_methods (), T = md->end_overridden_methods (); A != T; ++A){
	  omdc = *A;
	  sign = getSignature(omdc);
	  if(mHash.count(sign) > 0){
	    omd  = mHash.at(sign);
	    omd->insertOverrider(mn);
	  }
	}
      }

      for (std::set<methodNode*>::iterator it2 = cn->getPrivateMethods()->begin(); it2!=cn->getPrivateMethods()->end(); ++it2){
	mn = *it2;
	md = mn->getMethodDecl();

	for(CXXMethodDecl::method_iterator A = md->begin_overridden_methods (), T = md->end_overridden_methods (); A != T; ++A){
	  omdc = *A;
	  sign = getSignature(omdc);
	  if(mHash.count(sign) > 0){
	    omd  = mHash.at(sign);
	    omd->insertOverrider(mn);
	  }
	}
      }
    }
  }

  void produceConcurrentDependencies(){
    methodNode *mn;
    functionNode *fn;
    
    for (auto it = mHash.begin(); it != mHash.end(); ++it){
      mn = it->second;
      if(mn->hasDefinition())
	getCDependencies(mn);
    }
    
    for (auto it = fHash.begin(); it != fHash.end(); ++it){
      fn = it->second;
      if(fn->hasDefinition())
	getCDependencies(fn);
    }

  }

  
  void getCDependencies(procedureNode *pn){
    for (vector<functionThreadLaunch*>::iterator i = pn->getAnalysis()->getFunctionThreads()->begin(); i != pn->getAnalysis()->getFunctionThreads()->end(); ++i)
      findThreadDependencies(pn, (*i));

    for (vector<methodThreadLaunch*>::iterator i = pn->getAnalysis()->getMethodThreads()->begin(); i != pn->getAnalysis()->getMethodThreads()->end(); ++i)
      findThreadDependencies(pn, (*i));

    for (vector<lambdaThreadLaunch*>::iterator i = pn->getAnalysis()->getLambdaThreads()->begin(); i != pn->getAnalysis()->getLambdaThreads()->end(); ++i)
      findThreadDependencies(pn, (*i));
  }
  
  
  void findThreadDependencies(procedureNode *pn, threadLaunch *tl){
    bool flag = false;
    string tid;
    VarDecl *vd;
    VarDecl *vd2;
    functionThreadLaunch *ftl;
    methodThreadLaunch *mtl;
    lambdaThreadLaunch *ltl;
    FunctionDecl *fd;
    procedureNode *tpn;
    functionNode *fn;
    CFGReverseBlockReachabilityAnalysis *cfgra;

    if(tl->getKind() == 1){
      ftl= (functionThreadLaunch *) tl;
      fd = ftl->getThreadFunction();
      if(fHash.count(getSignature(fd)) > 0){
	tpn = fHash.at(getSignature(fd));
	tid = ftl->getThreadID();
	flag = true;
      }
    }

    if(tl->getKind() == 2){
      mtl = (methodThreadLaunch *) tl;
      fd = mtl->getThreadFunction();
      if(mHash.count(getSignature(fd)) > 0){
	tpn = mHash.at(getSignature(fd));
	tid = mtl->getThreadID();
	flag = true;
      }
    }

    if(tl->getKind() == 3){
      ltl = (lambdaThreadLaunch *) tl;
      fd = ltl->getLambdaExpr()->getCallOperator();
      fn = new functionNode(fd);
      fn->setNodeID(draftNodeID());
      setCFGBlockID(fn);
      ltl->setFunctionNode(fn);
      tpn = ltl->getFunctionNode();
      tid = ltl->getThreadID();
      flag = true;
    }
    
    if(flag){
      cfgra = new CFGReverseBlockReachabilityAnalysis((*pn->getAnalysis()->getCFG()));
     
      //static mutex lock/unlock
      for (vector<aloneVar*>::iterator i = pn->getAnalysis()->getStaticMutexLocks()->begin(); i != pn->getAnalysis()->getStaticMutexLocks()->end(); ++i){
	for (vector<aloneVar*>::iterator j = tpn->getAnalysis()->getStaticMutexUnlocks()->begin(); j != tpn->getAnalysis()->getStaticMutexUnlocks()->end(); ++j){
	  if((cfgra->isReachable(tl->getThreadLauncher(), (*i)->getCFGBlock())) ||  (tl->getThreadLauncher() == (*i)->getCFGBlock()))
	    if(((*i)->getVarDecl() == (*j)->getVarDecl()) || 
	       ((tl->isAloneArgument((*i)->getVarDecl()) == tpn->getAnalysis()->isFunctionArgument((*j)->getVarDecl())) && (tl->isAloneArgument((*i)->getVarDecl()) != -1))){
	      toolBox::expandVectorMap((*i)->getCFGBlock(), (*j)->getCFGBlock(), &sMutexLockCrash);
	      toolBox::expandVectorMap((*i)->getCFGBlock(), (*i)->getVarDecl()->getQualifiedNameAsString(), &sMutexLockCrashLabel);
	      toolBox::expandVectorMap((*i)->getCFGBlock(), tid, &sMutexLockCrashId);
	    }
	}
      }

      //static mutex unlock/lock
      for (vector<aloneVar*>::iterator i = pn->getAnalysis()->getStaticMutexUnlocks()->begin(); i != pn->getAnalysis()->getStaticMutexUnlocks()->end(); ++i){
	for (vector<aloneVar*>::iterator j = tpn->getAnalysis()->getStaticMutexLocks()->begin(); j != tpn->getAnalysis()->getStaticMutexLocks()->end(); ++j){
	  if((cfgra->isReachable(tl->getThreadLauncher(), (*i)->getCFGBlock())) ||  (tl->getThreadLauncher() == (*i)->getCFGBlock()))
	    if(((*i)->getVarDecl() == (*j)->getVarDecl()) || 
	       ((tl->isAloneArgument((*i)->getVarDecl()) == tpn->getAnalysis()->isFunctionArgument((*j)->getVarDecl())) && (tl->isAloneArgument((*i)->getVarDecl()) != -1))){
	      toolBox::expandVectorMap((*i)->getCFGBlock(), (*j)->getCFGBlock(), &sMutexUnlockCrash);
	      toolBox::expandVectorMap((*i)->getCFGBlock(), (*i)->getVarDecl()->getQualifiedNameAsString(), &sMutexUnlockCrashLabel);
	      toolBox::expandVectorMap((*i)->getCFGBlock(), tid, &sMutexUnlockCrashId);
	    }
	}
      }

      //static atomic op
      for (vector<aloneVar*>::iterator i = pn->getAnalysis()->getStaticAtomics()->begin(); i != pn->getAnalysis()->getStaticAtomics()->end(); ++i){
	for (vector<aloneVar*>::iterator j = tpn->getAnalysis()->getStaticAtomics()->begin(); j != tpn->getAnalysis()->getStaticAtomics()->end(); ++j){
	  if((cfgra->isReachable(tl->getThreadLauncher(), (*i)->getCFGBlock())) ||  (tl->getThreadLauncher() == (*i)->getCFGBlock()))
	    if(((*i)->getVarDecl() == (*j)->getVarDecl()) || 
	       ((tl->isAloneArgument((*i)->getVarDecl()) == tpn->getAnalysis()->isFunctionArgument((*j)->getVarDecl())) && (tl->isAloneArgument((*i)->getVarDecl()) != -1))){
	      toolBox::expandVectorMap((*i)->getCFGBlock(), (*j)->getCFGBlock(), &sAtomicCrash);
	      toolBox::expandVectorMap((*i)->getCFGBlock(), (*i)->getVarDecl()->getQualifiedNameAsString(), &sAtomicCrashLabel);
	      toolBox::expandVectorMap((*i)->getCFGBlock(), tid, &sAtomicCrashId);
	    }
	}
      }


      //static varusedef
      for (vector<aloneVar*>::iterator i = pn->getAnalysis()->getStaticVarUses()->begin(); i != pn->getAnalysis()->getStaticVarUses()->end(); ++i){
	for (vector<aloneVar*>::iterator j = tpn->getAnalysis()->getStaticVarDefs()->begin(); j != tpn->getAnalysis()->getStaticVarDefs()->end(); ++j){
	  if((cfgra->isReachable(tl->getThreadLauncher(), (*i)->getCFGBlock())) ||  (tl->getThreadLauncher() == (*i)->getCFGBlock()))
	    if(((*i)->getVarDecl() == (*j)->getVarDecl()) || 
	       ((tl->isAloneArgument((*i)->getVarDecl()) == tpn->getAnalysis()->isFunctionArgument((*j)->getVarDecl())) && (tl->isAloneArgument((*i)->getVarDecl()) != -1))){
	      toolBox::expandVectorMap((*i)->getCFGBlock(), (*j)->getCFGBlock(), &sVarUseDefCrash);
	      toolBox::expandVectorMap((*i)->getCFGBlock(), (*i)->getVarDecl()->getQualifiedNameAsString(), &sVarUseDefCrashLabel);
	      toolBox::expandVectorMap((*i)->getCFGBlock(), tid, &sVarUseDefCrashId);
	    }
	}
      }

      //static vardefuse
      for (vector<aloneVar*>::iterator i = pn->getAnalysis()->getStaticVarDefs()->begin(); i != pn->getAnalysis()->getStaticVarDefs()->end(); ++i){
	for (vector<aloneVar*>::iterator j = tpn->getAnalysis()->getStaticVarUses()->begin(); j != tpn->getAnalysis()->getStaticVarUses()->end(); ++j){
	  if((cfgra->isReachable(tl->getThreadLauncher(), (*i)->getCFGBlock())) ||  (tl->getThreadLauncher() == (*i)->getCFGBlock()))
	    if(((*i)->getVarDecl() == (*j)->getVarDecl()) || 
	       ((tl->isAloneArgument((*i)->getVarDecl()) == tpn->getAnalysis()->isFunctionArgument((*j)->getVarDecl())) && (tl->isAloneArgument((*i)->getVarDecl()) != -1))){
	      toolBox::expandVectorMap((*i)->getCFGBlock(), (*j)->getCFGBlock(), &sVarDefUseCrash);
	      toolBox::expandVectorMap((*i)->getCFGBlock(), (*i)->getVarDecl()->getQualifiedNameAsString(), &sVarDefUseCrashLabel);
	      toolBox::expandVectorMap((*i)->getCFGBlock(), tid, &sVarDefUseCrashId);
	    }
	}
      }

      //static vardefdef
      for (vector<aloneVar*>::iterator i = pn->getAnalysis()->getStaticVarDefs()->begin(); i != pn->getAnalysis()->getStaticVarDefs()->end(); ++i){
	for (vector<aloneVar*>::iterator j = tpn->getAnalysis()->getStaticVarDefs()->begin(); j != tpn->getAnalysis()->getStaticVarDefs()->end(); ++j){
	  if((cfgra->isReachable(tl->getThreadLauncher(), (*i)->getCFGBlock())) ||  (tl->getThreadLauncher() == (*i)->getCFGBlock()))
	    if(((*i)->getVarDecl() == (*j)->getVarDecl()) || 
	       ((tl->isAloneArgument((*i)->getVarDecl()) == tpn->getAnalysis()->isFunctionArgument((*j)->getVarDecl())) && (tl->isAloneArgument((*i)->getVarDecl()) != -1))){
	      toolBox::expandVectorMap((*i)->getCFGBlock(), (*j)->getCFGBlock(), &sVarDefDefCrash);
	      toolBox::expandVectorMap((*i)->getCFGBlock(), (*i)->getVarDecl()->getQualifiedNameAsString(), &sVarDefDefCrashLabel);
	      toolBox::expandVectorMap((*i)->getCFGBlock(), tid, &sVarDefDefCrashId);
	    }
	}
      }


      for(vector<objectVar*>::iterator i = pn->getAnalysis()->getNonStaticMutexLocks()->begin(); i != pn->getAnalysis()->getNonStaticMutexLocks()->end(); ++i){
	for(vector<objectVar*>::iterator j = tpn->getAnalysis()->getNonStaticMutexUnlocks()->begin(); j != tpn->getAnalysis()->getNonStaticMutexUnlocks()->end(); ++j){
	  if((cfgra->isReachable(tl->getThreadLauncher(), (*i)->getCFGBlock())) ||  (tl->getThreadLauncher() == (*i)->getCFGBlock()))
	    if((*i)->getFieldDecl() == (*j)->getFieldDecl()){
	      if(isa<VarDecl>((*i)->getObjectDecl()->getDecl())){
		vd = (VarDecl *) (*i)->getObjectDecl()->getDecl();
		if(isa<VarDecl>((*j)->getObjectDecl()->getDecl())){
		  vd2 = (VarDecl *) (*j)->getObjectDecl()->getDecl();
		  if((tl->isAloneArgument(vd) == tpn->getAnalysis()->isFunctionArgument(vd2)) || 
		     (tl->isObjectArgument((*i)->getObjectDecl()) == tpn->getAnalysis()->isFunctionArgument(vd2))){
		  
		    toolBox::expandVectorMap((*i)->getCFGBlock(), (*j)->getCFGBlock(), &nsAtomicCrash);
		    toolBox::expandVectorMap((*i)->getCFGBlock(), vd->getQualifiedNameAsString(), &nsAtomicCrashLabel);
		    toolBox::expandVectorMap((*i)->getCFGBlock(), tid, &nsAtomicCrashId);
		  }
		}
	      }
	    }
	}
      }



      for(vector<objectVar*>::iterator i = pn->getAnalysis()->getNonStaticMutexUnlocks()->begin(); i != pn->getAnalysis()->getNonStaticMutexUnlocks()->end(); ++i){
	for(vector<objectVar*>::iterator j = tpn->getAnalysis()->getNonStaticMutexLocks()->begin(); j != tpn->getAnalysis()->getNonStaticMutexLocks()->end(); ++j){
	  if((cfgra->isReachable(tl->getThreadLauncher(), (*i)->getCFGBlock())) ||  (tl->getThreadLauncher() == (*i)->getCFGBlock()))
	    if((*i)->getFieldDecl() == (*j)->getFieldDecl()){
	      if(isa<VarDecl>((*i)->getObjectDecl()->getDecl())){
		vd = (VarDecl *) (*i)->getObjectDecl()->getDecl();
		if(isa<VarDecl>((*j)->getObjectDecl()->getDecl())){
		  vd2 = (VarDecl *) (*j)->getObjectDecl()->getDecl();
		  if((tl->isAloneArgument(vd) == tpn->getAnalysis()->isFunctionArgument(vd2)) || 
		     (tl->isObjectArgument((*i)->getObjectDecl()) == tpn->getAnalysis()->isFunctionArgument(vd2))){
		  
		    toolBox::expandVectorMap((*i)->getCFGBlock(), (*j)->getCFGBlock(), &nsAtomicCrash);
		    toolBox::expandVectorMap((*i)->getCFGBlock(), vd->getQualifiedNameAsString(), &nsAtomicCrashLabel);
		    toolBox::expandVectorMap((*i)->getCFGBlock(), tid, &nsAtomicCrashId);
		  }
		}
	      }
	    }
	}
      }


      for(vector<objectVar*>::iterator i = pn->getAnalysis()->getNonStaticAtomics()->begin(); i != pn->getAnalysis()->getNonStaticAtomics()->end(); ++i){
	for(vector<objectVar*>::iterator j = tpn->getAnalysis()->getNonStaticAtomics()->begin(); j != tpn->getAnalysis()->getNonStaticAtomics()->end(); ++j){
	  if((cfgra->isReachable(tl->getThreadLauncher(), (*i)->getCFGBlock())) ||  (tl->getThreadLauncher() == (*i)->getCFGBlock()))
	    if((*i)->getFieldDecl() == (*j)->getFieldDecl()){
	      if(isa<VarDecl>((*i)->getObjectDecl()->getDecl())){
		vd = (VarDecl *) (*i)->getObjectDecl()->getDecl();
		if(isa<VarDecl>((*j)->getObjectDecl()->getDecl())){
		  vd2 = (VarDecl *) (*j)->getObjectDecl()->getDecl();
		  if((tl->isAloneArgument(vd) == tpn->getAnalysis()->isFunctionArgument(vd2)) || 
		     (tl->isObjectArgument((*i)->getObjectDecl()) == tpn->getAnalysis()->isFunctionArgument(vd2))){
		  
		    toolBox::expandVectorMap((*i)->getCFGBlock(), (*j)->getCFGBlock(), &nsAtomicCrash);
		    toolBox::expandVectorMap((*i)->getCFGBlock(), vd->getQualifiedNameAsString(), &nsAtomicCrashLabel);
		    toolBox::expandVectorMap((*i)->getCFGBlock(), tid, &nsAtomicCrashId);
		  }
		}
	      }
	    }
	}
      }

      for(vector<objectVar*>::iterator i = pn->getAnalysis()->getNonStaticVarDefs()->begin(); i != pn->getAnalysis()->getNonStaticVarDefs()->end(); ++i){
	for(vector<objectVar*>::iterator j = tpn->getAnalysis()->getNonStaticVarUses()->begin(); j != tpn->getAnalysis()->getNonStaticVarUses()->end(); ++j){
	  if((cfgra->isReachable(tl->getThreadLauncher(), (*i)->getCFGBlock())) ||  (tl->getThreadLauncher() == (*i)->getCFGBlock()))
	    if((*i)->getFieldDecl() == (*j)->getFieldDecl()){
	      if(isa<VarDecl>((*i)->getObjectDecl()->getDecl())){
		vd = (VarDecl *) (*i)->getObjectDecl()->getDecl();
		if(isa<VarDecl>((*j)->getObjectDecl()->getDecl())){
		  vd2 = (VarDecl *) (*j)->getObjectDecl()->getDecl();
		  if((tl->isAloneArgument(vd) == tpn->getAnalysis()->isFunctionArgument(vd2)) || 
		     (tl->isObjectArgument((*i)->getObjectDecl()) == tpn->getAnalysis()->isFunctionArgument(vd2))){
		  
		    toolBox::expandVectorMap((*i)->getCFGBlock(), (*j)->getCFGBlock(), &nsVarDefUseCrash);
		    toolBox::expandVectorMap((*i)->getCFGBlock(), vd->getQualifiedNameAsString(), &nsVarDefUseCrashLabel);
		    toolBox::expandVectorMap((*i)->getCFGBlock(), tid, &nsVarDefUseCrashId);
		  }
		}
	      }
	    }
	}
      }
      

      for(vector<objectVar*>::iterator i = pn->getAnalysis()->getNonStaticVarUses()->begin(); i != pn->getAnalysis()->getNonStaticVarUses()->end(); ++i){
	for(vector<objectVar*>::iterator j = tpn->getAnalysis()->getNonStaticVarDefs()->begin(); j != tpn->getAnalysis()->getNonStaticVarDefs()->end(); ++j){
	  if((cfgra->isReachable(tl->getThreadLauncher(), (*i)->getCFGBlock())) ||  (tl->getThreadLauncher() == (*i)->getCFGBlock()))
	    if((*i)->getFieldDecl() == (*j)->getFieldDecl()){
	      if(isa<VarDecl>((*i)->getObjectDecl()->getDecl())){
		vd = (VarDecl *) (*i)->getObjectDecl()->getDecl();
		if(isa<VarDecl>((*j)->getObjectDecl()->getDecl())){
		  vd2 = (VarDecl *) (*j)->getObjectDecl()->getDecl();
		  if((tl->isAloneArgument(vd) == tpn->getAnalysis()->isFunctionArgument(vd2)) || 
		     (tl->isObjectArgument((*i)->getObjectDecl()) == tpn->getAnalysis()->isFunctionArgument(vd2))){
		  
		    toolBox::expandVectorMap((*i)->getCFGBlock(), (*j)->getCFGBlock(), &nsVarUseDefCrash);
		    toolBox::expandVectorMap((*i)->getCFGBlock(), vd->getQualifiedNameAsString(), &nsVarUseDefCrashLabel);
		    toolBox::expandVectorMap((*i)->getCFGBlock(), tid, &nsVarUseDefCrashId);
		  }
		}
	      }
	    }
	}
      }

      for(vector<objectVar*>::iterator i = pn->getAnalysis()->getNonStaticVarDefs()->begin(); i != pn->getAnalysis()->getNonStaticVarDefs()->end(); ++i){
	for(vector<objectVar*>::iterator j = tpn->getAnalysis()->getNonStaticVarDefs()->begin(); j != tpn->getAnalysis()->getNonStaticVarDefs()->end(); ++j){
	  if((cfgra->isReachable(tl->getThreadLauncher(), (*i)->getCFGBlock())) ||  (tl->getThreadLauncher() == (*i)->getCFGBlock()))
	    if((*i)->getFieldDecl() == (*j)->getFieldDecl()){
	      if(isa<VarDecl>((*i)->getObjectDecl()->getDecl())){
		vd = (VarDecl *) (*i)->getObjectDecl()->getDecl();
		if(isa<VarDecl>((*j)->getObjectDecl()->getDecl())){
		  vd2 = (VarDecl *) (*j)->getObjectDecl()->getDecl();
		  if((tl->isAloneArgument(vd) == tpn->getAnalysis()->isFunctionArgument(vd2)) || 
		     (tl->isObjectArgument((*i)->getObjectDecl()) == tpn->getAnalysis()->isFunctionArgument(vd2))){
		  
		    toolBox::expandVectorMap((*i)->getCFGBlock(), (*j)->getCFGBlock(), &nsVarDefDefCrash);
		    toolBox::expandVectorMap((*i)->getCFGBlock(), vd->getQualifiedNameAsString(), &nsVarDefDefCrashLabel);
		    toolBox::expandVectorMap((*i)->getCFGBlock(), tid, &nsVarDefDefCrashId);
		  }
		}
	      }
	    }
	}
      }



    }
  }

 
  ///////////////////////////////////////////////////////////////////////
  //
  //IMPORTING DATA
  //
  ///////////////////////////////////////////////////////////////////////

  string getSignature(const FunctionDecl *FD){
    string sign = "";
    const ParmVarDecl *var;
   
    sign.append(FD->getQualifiedNameAsString());
    
    for(FunctionDecl::param_const_iterator I = FD->param_begin(), E = FD->param_end(); I != E; ++I){
      var = *I;
      sign.append(QualType::getAsString(var->getType().split()));
    }
    return sign;
  }

  void insertClassNode(classNode *cn){
    string signature = cn->getClassDecl()->getQualifiedNameAsString();
    
    definedClasses.insert(signature);
    cn->setSignature(signature);
    cn->setNodeID(draftNodeID());
    pair<string, classNode*> hash (signature,cn);
    cHash.insert(hash);
  }

  void insertMethodNode(methodNode *mn){
    string signature = getSignature(mn->getMethodDecl());
   
    mn->setSignature(signature);
    mn->setNodeID(draftNodeID());
    pair<string, methodNode*> hash (signature, mn);
    mHash.insert(hash);
  }

  void insertFunctionNode(functionNode *fn){
    string signature = getSignature(fn->getFunctionDecl());
   
    definedFunctions.insert(signature);
    fn->setSignature(signature);
    fn->setNodeID(draftNodeID());
    pair<string, functionNode*> hash (signature, fn);
    fHash.insert(hash);
  }


  //Need to be controled by mutex if threads are introduced.
  string draftNodeID(){
    string id = "node";
    nodeCounter ++;
    id.append(to_string(nodeCounter));
    return id;
  }
 
  bool isClassDefined(CXXRecordDecl *CD){
    strIt = definedClasses.find(CD->getQualifiedNameAsString());
    if(strIt != definedClasses.end())
      return true;
    else
      return false;
  }

  bool isFunctionDefined(FunctionDecl *FD){
    strIt = definedFunctions.find(getSignature(FD));
    if(strIt != definedFunctions.end())
      return true;
    else
      return false;
  }

protected:
  int nodeCounter;
  ASTContext *Context;
  CFGBlockID bidHash;
  classHash cHash;
  methodHash mHash;
  functionHash fHash;
  mutexHash mxHash;
  set<string> definedClasses;
  set<string> definedFunctions;
  set<string>::iterator strIt;


  CFGBlockVector sMutexLockCrash;
  CFGBlockVectorLabel sMutexLockCrashLabel;
  CFGBlockVectorLabel sMutexLockCrashId;
  
  CFGBlockVector sMutexUnlockCrash;
  CFGBlockVectorLabel sMutexUnlockCrashLabel;
  CFGBlockVectorLabel sMutexUnlockCrashId;
  
  CFGBlockVector sAtomicCrash;
  CFGBlockVectorLabel sAtomicCrashLabel;
  CFGBlockVectorLabel sAtomicCrashId;

  CFGBlockVector sVarDefDefCrash;
  CFGBlockVectorLabel sVarDefDefCrashLabel;
  CFGBlockVectorLabel sVarDefDefCrashId;

  CFGBlockVector sVarUseDefCrash;
  CFGBlockVectorLabel sVarUseDefCrashLabel;
  CFGBlockVectorLabel sVarUseDefCrashId;

  CFGBlockVector sVarDefUseCrash;
  CFGBlockVectorLabel sVarDefUseCrashLabel;
  CFGBlockVectorLabel sVarDefUseCrashId;

  CFGBlockVector nsMutexLockCrash;
  CFGBlockVectorLabel nsMutexLockCrashLabel;
  CFGBlockVectorLabel nsMutexLockCrashId;
  
  CFGBlockVector nsMutexUnlockCrash;
  CFGBlockVectorLabel nsMutexUnlockCrashLabel;
  CFGBlockVectorLabel nsMutexUnlockCrashId;
  
  CFGBlockVector nsAtomicCrash;
  CFGBlockVectorLabel nsAtomicCrashLabel;
  CFGBlockVectorLabel nsAtomicCrashId;

  CFGBlockVector nsVarDefDefCrash;
  CFGBlockVectorLabel nsVarDefDefCrashLabel;
  CFGBlockVectorLabel nsVarDefDefCrashId;

  CFGBlockVector nsVarUseDefCrash;
  CFGBlockVectorLabel nsVarUseDefCrashLabel;
  CFGBlockVectorLabel nsVarUseDefCrashId;

  CFGBlockVector nsVarDefUseCrash;
  CFGBlockVectorLabel nsVarDefUseCrashLabel;
  CFGBlockVectorLabel nsVarDefUseCrashId;
};


class FunctionVisitor : public RecursiveASTVisitor<FunctionVisitor> {
public:
  explicit FunctionVisitor(ASTContext *Context, graphManager *gManager) : Context(Context), gManager(gManager) {}

  bool VisitFunctionDecl (FunctionDecl *FD) {
    if(!isa<CXXMethodDecl>(FD) && !Context->getFullLoc(FD->getSourceRange().getBegin()).isInSystemHeader()){
      if(!gManager->isFunctionDefined(FD) && FD->getNameAsString().size() > 0){

	functionNode *fn = new functionNode(FD); 
	gManager->insertFunctionNode(fn);
      }
    }
    return true;
  }

private:
  ASTContext *Context;
  graphManager *gManager;
};

class ClassVisitor : public RecursiveASTVisitor<ClassVisitor> {
public:
  explicit ClassVisitor(ASTContext *Context, graphManager *gManager) : Context(Context),gManager(gManager) {}

  bool VisitCXXRecordDecl(CXXRecordDecl *CD) {
    CXXMethodDecl *M;
    
    if(!Context->getFullLoc(CD->getSourceRange().getBegin()).isInSystemHeader()){

      if(!gManager->isClassDefined(CD) && CD->getNameAsString().size() > 0){
	
	if(CD->hasDefinition())
	  CD = CD->getDefinition();
	
	classNode *cn = new classNode(CD);
	gManager->insertClassNode(cn); 
	
	for(CXXRecordDecl::method_iterator A = CD->method_begin(), T = CD->method_end(); A != T; ++A){
	  M = *A;
	  if(M->getNameAsString().size() > 0){
	    methodNode *mn = new methodNode(M);
	    gManager->insertMethodNode(mn);
	    cn->insertMemberMethod(mn, mn->getMembershipType());   
	  }
	}
      }
    }
    return true;
  }

private:
  ASTContext *Context;
  graphManager *gManager;
};

class CGConsumerAST : public clang::ASTConsumer{
public:
  explicit CGConsumerAST(ASTContext *Context){
    gManager = new graphManager(Context);
    CVisitor = new ClassVisitor(Context, gManager);
    FVisitor = new FunctionVisitor(Context, gManager);
  }
  //This method is called when the input AST has been parsed.
  virtual void HandleTranslationUnit(clang::ASTContext &Context){
   
    CVisitor->TraverseDecl(Context.getTranslationUnitDecl());

    FVisitor->TraverseDecl(Context.getTranslationUnitDecl());

    gManager->expandGraph();

    gManager->exportGraph();
  }

private:
  ClassVisitor *CVisitor;
  FunctionVisitor *FVisitor;
  graphManager *gManager;
};

//Creates an AST consumer with the AST Context as an argument.
//May accept a sub-tree instead of the whole AST.
class CGAction : public clang::ASTFrontendAction {
public:
  clang::ASTConsumer *CreateASTConsumer(clang::CompilerInstance &Compiler, llvm::StringRef InFile) {
    return new CGConsumerAST(&Compiler.getASTContext());
  }
};

static cl::OptionCategory MyToolCategory("my-tool options");

int main(int argc, const char **argv) {
  //Compilation Options
  CommonOptionsParser op(argc, argv, MyToolCategory);
  
  ClangTool Tool(op.getCompilations(), op.getSourcePathList());
  //Execute action
  int result = Tool.run(newFrontendActionFactory<CGAction>().get());
  
  return result;
}
